package ihl.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import ihl.InductionHob;
import ihl.PowerChannel;
import ihl.SourceElement;
import ihl.TargetElement;
import ihl.diagram.edit.policies.IhlBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class PowerChannelReorientCommand extends EditElementCommand {

	/**
	* @generated
	*/
	private final int reorientDirection;

	/**
	* @generated
	*/
	private final EObject oldEnd;

	/**
	* @generated
	*/
	private final EObject newEnd;

	/**
	* @generated
	*/
	public PowerChannelReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	* @generated
	*/
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof PowerChannel) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	* @generated
	*/
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof SourceElement && newEnd instanceof SourceElement)) {
			return false;
		}
		TargetElement target = getLink().getTo();
		if (!(getLink().eContainer() instanceof InductionHob)) {
			return false;
		}
		InductionHob container = (InductionHob) getLink().eContainer();
		return IhlBaseItemSemanticEditPolicy.getLinkConstraints().canExistPowerChannel_4001(container, getLink(),
				getNewSource(), target);
	}

	/**
	* @generated
	*/
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof TargetElement && newEnd instanceof TargetElement)) {
			return false;
		}
		SourceElement source = getLink().getFrom();
		if (!(getLink().eContainer() instanceof InductionHob)) {
			return false;
		}
		InductionHob container = (InductionHob) getLink().eContainer();
		return IhlBaseItemSemanticEditPolicy.getLinkConstraints().canExistPowerChannel_4001(container, getLink(),
				source, getNewTarget());
	}

	/**
	* @generated
	*/
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException("Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	* @generated
	*/
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setFrom(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	* @generated
	*/
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTo(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	* @generated
	*/
	protected PowerChannel getLink() {
		return (PowerChannel) getElementToEdit();
	}

	/**
	* @generated
	*/
	protected SourceElement getOldSource() {
		return (SourceElement) oldEnd;
	}

	/**
	* @generated
	*/
	protected SourceElement getNewSource() {
		return (SourceElement) newEnd;
	}

	/**
	* @generated
	*/
	protected TargetElement getOldTarget() {
		return (TargetElement) oldEnd;
	}

	/**
	* @generated
	*/
	protected TargetElement getNewTarget() {
		return (TargetElement) newEnd;
	}
}
