package ihl.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;

import ihl.diagram.part.IhlVisualIDRegistry;

/**
 * @generated
 */
public class IhlEditPartFactory implements EditPartFactory {

	/**
	* @generated
	*/
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (IhlVisualIDRegistry.getVisualID(view)) {

			case InductionHobEditPart.VISUAL_ID:
				return new InductionHobEditPart(view);

			case PowerManagerEditPart.VISUAL_ID:
				return new PowerManagerEditPart(view);

			case InductorEditPart.VISUAL_ID:
				return new InductorEditPart(view);

			case InverterEditPart.VISUAL_ID:
				return new InverterEditPart(view);

			case PowerChannelEditPart.VISUAL_ID:
				return new PowerChannelEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	* @generated
	*/
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	* @generated
	*/
	public static CellEditorLocator getTextCellEditorLocator(ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE.getTextCellEditorLocator(source);
	}

}
