package ihl.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import ihl.diagram.edit.commands.InductorCreateCommand;
import ihl.diagram.edit.commands.InverterCreateCommand;
import ihl.diagram.edit.commands.PowerManagerCreateCommand;
import ihl.diagram.providers.IhlElementTypes;

/**
 * @generated
 */
public class InductionHobItemSemanticEditPolicy extends IhlBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public InductionHobItemSemanticEditPolicy() {
		super(IhlElementTypes.InductionHob_1000);
	}

	/**
	* @generated
	*/
	protected Command getCreateCommand(CreateElementRequest req) {
		if (IhlElementTypes.PowerManager_2001 == req.getElementType()) {
			return getGEFWrapper(new PowerManagerCreateCommand(req));
		}
		if (IhlElementTypes.Inductor_2002 == req.getElementType()) {
			return getGEFWrapper(new InductorCreateCommand(req));
		}
		if (IhlElementTypes.Inverter_2003 == req.getElementType()) {
			return getGEFWrapper(new InverterCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	* @generated
	*/
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	* @generated
	*/
	private static class DuplicateAnythingCommand extends DuplicateEObjectsCommand {

		/**
		* @generated
		*/
		public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain, DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req.getElementsToBeDuplicated(), req.getAllDuplicatedElementsMap());
		}

	}

}
