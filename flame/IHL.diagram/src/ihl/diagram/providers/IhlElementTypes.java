package ihl.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import ihl.IhlPackage;
import ihl.diagram.edit.parts.InductionHobEditPart;
import ihl.diagram.edit.parts.InductorEditPart;
import ihl.diagram.edit.parts.InverterEditPart;
import ihl.diagram.edit.parts.PowerChannelEditPart;
import ihl.diagram.edit.parts.PowerManagerEditPart;
import ihl.diagram.part.IhlDiagramEditorPlugin;

/**
 * @generated
 */
public class IhlElementTypes {

	/**
	* @generated
	*/
	private IhlElementTypes() {
	}

	/**
	* @generated
	*/
	private static Map<IElementType, ENamedElement> elements;

	/**
	* @generated
	*/
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			IhlDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());

	/**
	* @generated
	*/
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	* @generated
	*/
	public static final IElementType InductionHob_1000 = getElementType("IHL.diagram.InductionHob_1000"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType PowerManager_2001 = getElementType("IHL.diagram.PowerManager_2001"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Inductor_2002 = getElementType("IHL.diagram.Inductor_2002"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Inverter_2003 = getElementType("IHL.diagram.Inverter_2003"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType PowerChannel_4001 = getElementType("IHL.diagram.PowerChannel_4001"); //$NON-NLS-1$

	/**
	* @generated
	*/
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	* @generated
	*/
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	* @generated
	*/
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	* @generated
	*/
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	* Returns 'type' of the ecore object associated with the hint.
	* 
	* @generated
	*/
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(InductionHob_1000, IhlPackage.eINSTANCE.getInductionHob());

			elements.put(PowerManager_2001, IhlPackage.eINSTANCE.getPowerManager());

			elements.put(Inductor_2002, IhlPackage.eINSTANCE.getInductor());

			elements.put(Inverter_2003, IhlPackage.eINSTANCE.getInverter());

			elements.put(PowerChannel_4001, IhlPackage.eINSTANCE.getPowerChannel());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	* @generated
	*/
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	* @generated
	*/
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(InductionHob_1000);
			KNOWN_ELEMENT_TYPES.add(PowerManager_2001);
			KNOWN_ELEMENT_TYPES.add(Inductor_2002);
			KNOWN_ELEMENT_TYPES.add(Inverter_2003);
			KNOWN_ELEMENT_TYPES.add(PowerChannel_4001);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	* @generated
	*/
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case InductionHobEditPart.VISUAL_ID:
			return InductionHob_1000;
		case PowerManagerEditPart.VISUAL_ID:
			return PowerManager_2001;
		case InductorEditPart.VISUAL_ID:
			return Inductor_2002;
		case InverterEditPart.VISUAL_ID:
			return Inverter_2003;
		case PowerChannelEditPart.VISUAL_ID:
			return PowerChannel_4001;
		}
		return null;
	}

	/**
	* @generated
	*/
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(elementTypeImages) {

		/**
		* @generated
		*/
		@Override

		public boolean isKnownElementType(IElementType elementType) {
			return ihl.diagram.providers.IhlElementTypes.isKnownElementType(elementType);
		}

		/**
		* @generated
		*/
		@Override

		public IElementType getElementTypeForVisualId(int visualID) {
			return ihl.diagram.providers.IhlElementTypes.getElementType(visualID);
		}

		/**
		* @generated
		*/
		@Override

		public ENamedElement getDefiningNamedElement(IAdaptable elementTypeAdapter) {
			return ihl.diagram.providers.IhlElementTypes.getElement(elementTypeAdapter);
		}
	};

}
