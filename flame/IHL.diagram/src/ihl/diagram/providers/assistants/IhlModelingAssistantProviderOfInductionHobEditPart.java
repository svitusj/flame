package ihl.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import ihl.diagram.providers.IhlElementTypes;
import ihl.diagram.providers.IhlModelingAssistantProvider;

/**
 * @generated
 */
public class IhlModelingAssistantProviderOfInductionHobEditPart extends IhlModelingAssistantProvider {

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(3);
		types.add(IhlElementTypes.PowerManager_2001);
		types.add(IhlElementTypes.Inductor_2002);
		types.add(IhlElementTypes.Inverter_2003);
		return types;
	}

}
