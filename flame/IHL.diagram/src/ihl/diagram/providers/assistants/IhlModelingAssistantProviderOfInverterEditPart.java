package ihl.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import ihl.diagram.edit.parts.InductorEditPart;
import ihl.diagram.edit.parts.InverterEditPart;
import ihl.diagram.edit.parts.PowerManagerEditPart;
import ihl.diagram.providers.IhlElementTypes;
import ihl.diagram.providers.IhlModelingAssistantProvider;

/**
 * @generated
 */
public class IhlModelingAssistantProviderOfInverterEditPart extends IhlModelingAssistantProvider {

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSource((InverterEditPart) sourceEditPart);
	}

	/**
	* @generated
	*/
	public List<IElementType> doGetRelTypesOnSource(InverterEditPart source) {
		List<IElementType> types = new ArrayList<IElementType>(1);
		types.add(IhlElementTypes.PowerChannel_4001);
		return types;
	}

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getRelTypesOnSourceAndTarget(IAdaptable source, IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSourceAndTarget((InverterEditPart) sourceEditPart, targetEditPart);
	}

	/**
	* @generated
	*/
	public List<IElementType> doGetRelTypesOnSourceAndTarget(InverterEditPart source,
			IGraphicalEditPart targetEditPart) {
		List<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof PowerManagerEditPart) {
			types.add(IhlElementTypes.PowerChannel_4001);
		}
		if (targetEditPart instanceof InductorEditPart) {
			types.add(IhlElementTypes.PowerChannel_4001);
		}
		return types;
	}

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getTypesForTarget(IAdaptable source, IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForTarget((InverterEditPart) sourceEditPart, relationshipType);
	}

	/**
	* @generated
	*/
	public List<IElementType> doGetTypesForTarget(InverterEditPart source, IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == IhlElementTypes.PowerChannel_4001) {
			types.add(IhlElementTypes.PowerManager_2001);
			types.add(IhlElementTypes.Inductor_2002);
		}
		return types;
	}

}
