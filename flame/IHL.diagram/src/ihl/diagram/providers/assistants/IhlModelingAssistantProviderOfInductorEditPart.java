package ihl.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import ihl.diagram.edit.parts.InductorEditPart;
import ihl.diagram.providers.IhlElementTypes;
import ihl.diagram.providers.IhlModelingAssistantProvider;

/**
 * @generated
 */
public class IhlModelingAssistantProviderOfInductorEditPart extends IhlModelingAssistantProvider {

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((InductorEditPart) targetEditPart);
	}

	/**
	* @generated
	*/
	public List<IElementType> doGetRelTypesOnTarget(InductorEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(1);
		types.add(IhlElementTypes.PowerChannel_4001);
		return types;
	}

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getTypesForSource(IAdaptable target, IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((InductorEditPart) targetEditPart, relationshipType);
	}

	/**
	* @generated
	*/
	public List<IElementType> doGetTypesForSource(InductorEditPart target, IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == IhlElementTypes.PowerChannel_4001) {
			types.add(IhlElementTypes.PowerManager_2001);
			types.add(IhlElementTypes.Inverter_2003);
		}
		return types;
	}

}
