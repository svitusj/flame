package ihl.diagram.providers;

import org.eclipse.gmf.tooling.runtime.providers.DefaultEditPartProvider;

import ihl.diagram.edit.parts.IhlEditPartFactory;
import ihl.diagram.edit.parts.InductionHobEditPart;
import ihl.diagram.part.IhlVisualIDRegistry;

/**
 * @generated
 */
public class IhlEditPartProvider extends DefaultEditPartProvider {

	/**
	* @generated
	*/
	public IhlEditPartProvider() {
		super(new IhlEditPartFactory(), IhlVisualIDRegistry.TYPED_INSTANCE, InductionHobEditPart.MODEL_ID);
	}

}
