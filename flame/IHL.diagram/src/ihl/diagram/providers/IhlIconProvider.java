package ihl.diagram.providers;

import org.eclipse.gmf.runtime.common.ui.services.icon.IIconProvider;
import org.eclipse.gmf.tooling.runtime.providers.DefaultElementTypeIconProvider;

/**
 * @generated
 */
public class IhlIconProvider extends DefaultElementTypeIconProvider implements IIconProvider {

	/**
	* @generated
	*/
	public IhlIconProvider() {
		super(IhlElementTypes.TYPED_INSTANCE);
	}

}
