package ihl.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

import ihl.IhlPackage;
import ihl.InductionHob;
import ihl.diagram.edit.parts.InductionHobEditPart;
import ihl.diagram.edit.parts.InductorEditPart;
import ihl.diagram.edit.parts.InverterEditPart;
import ihl.diagram.edit.parts.PowerChannelEditPart;
import ihl.diagram.edit.parts.PowerManagerEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class IhlVisualIDRegistry {

	/**
	* @generated
	*/
	private static final String DEBUG_KEY = "IHL.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	* @generated
	*/
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (InductionHobEditPart.MODEL_ID.equals(view.getType())) {
				return InductionHobEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return ihl.diagram.part.IhlVisualIDRegistry.getVisualID(view.getType());
	}

	/**
	* @generated
	*/
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	* @generated
	*/
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(Platform.getDebugOption(DEBUG_KEY))) {
				IhlDiagramEditorPlugin.getInstance()
						.logError("Unable to parse view type as a visualID number: " + type);
			}
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	* @generated
	*/
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (IhlPackage.eINSTANCE.getInductionHob().isSuperTypeOf(domainElement.eClass())
				&& isDiagram((InductionHob) domainElement)) {
			return InductionHobEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = ihl.diagram.part.IhlVisualIDRegistry.getModelID(containerView);
		if (!InductionHobEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (InductionHobEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = ihl.diagram.part.IhlVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = InductionHobEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case InductionHobEditPart.VISUAL_ID:
			if (IhlPackage.eINSTANCE.getPowerManager().isSuperTypeOf(domainElement.eClass())) {
				return PowerManagerEditPart.VISUAL_ID;
			}
			if (IhlPackage.eINSTANCE.getInductor().isSuperTypeOf(domainElement.eClass())) {
				return InductorEditPart.VISUAL_ID;
			}
			if (IhlPackage.eINSTANCE.getInverter().isSuperTypeOf(domainElement.eClass())) {
				return InverterEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = ihl.diagram.part.IhlVisualIDRegistry.getModelID(containerView);
		if (!InductionHobEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (InductionHobEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = ihl.diagram.part.IhlVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = InductionHobEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case InductionHobEditPart.VISUAL_ID:
			if (PowerManagerEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InductorEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InverterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	* @generated
	*/
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (IhlPackage.eINSTANCE.getPowerChannel().isSuperTypeOf(domainElement.eClass())) {
			return PowerChannelEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	* User can change implementation of this method to handle some specific
	* situations not covered by default logic.
	* 
	* @generated
	*/
	private static boolean isDiagram(InductionHob element) {
		return true;
	}

	/**
	* @generated
	*/
	public static boolean checkNodeVisualID(View containerView, EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	* @generated
	*/
	public static boolean isCompartmentVisualID(int visualID) {
		return false;
	}

	/**
	* @generated
	*/
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case InductionHobEditPart.VISUAL_ID:
			return false;
		case PowerManagerEditPart.VISUAL_ID:
		case InductorEditPart.VISUAL_ID:
		case InverterEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	* @generated
	*/
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		* @generated
		*/
		@Override

		public int getVisualID(View view) {
			return ihl.diagram.part.IhlVisualIDRegistry.getVisualID(view);
		}

		/**
		* @generated
		*/
		@Override

		public String getModelID(View view) {
			return ihl.diagram.part.IhlVisualIDRegistry.getModelID(view);
		}

		/**
		* @generated
		*/
		@Override

		public int getNodeVisualID(View containerView, EObject domainElement) {
			return ihl.diagram.part.IhlVisualIDRegistry.getNodeVisualID(containerView, domainElement);
		}

		/**
		* @generated
		*/
		@Override

		public boolean checkNodeVisualID(View containerView, EObject domainElement, int candidate) {
			return ihl.diagram.part.IhlVisualIDRegistry.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		* @generated
		*/
		@Override

		public boolean isCompartmentVisualID(int visualID) {
			return ihl.diagram.part.IhlVisualIDRegistry.isCompartmentVisualID(visualID);
		}

		/**
		* @generated
		*/
		@Override

		public boolean isSemanticLeafVisualID(int visualID) {
			return ihl.diagram.part.IhlVisualIDRegistry.isSemanticLeafVisualID(visualID);
		}
	};

}
