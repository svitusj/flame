package ihl.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
 * @generated
 */
public class IhlNodeDescriptor extends UpdaterNodeDescriptor {
	/**
	* @generated
	*/
	public IhlNodeDescriptor(EObject modelElement, int visualID) {
		super(modelElement, visualID);
	}

}
