package ihl.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

import ihl.IhlPackage;
import ihl.InductionHob;
import ihl.Inductor;
import ihl.Inverter;
import ihl.PowerChannel;
import ihl.PowerManager;
import ihl.SourceElement;
import ihl.TargetElement;
import ihl.diagram.edit.parts.InductionHobEditPart;
import ihl.diagram.edit.parts.InductorEditPart;
import ihl.diagram.edit.parts.InverterEditPart;
import ihl.diagram.edit.parts.PowerChannelEditPart;
import ihl.diagram.edit.parts.PowerManagerEditPart;
import ihl.diagram.providers.IhlElementTypes;

/**
 * @generated
 */
public class IhlDiagramUpdater {

	/**
	* @generated
	*/
	public static List<IhlNodeDescriptor> getSemanticChildren(View view) {
		switch (IhlVisualIDRegistry.getVisualID(view)) {
		case InductionHobEditPart.VISUAL_ID:
			return getInductionHob_1000SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<IhlNodeDescriptor> getInductionHob_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		InductionHob modelElement = (InductionHob) view.getElement();
		LinkedList<IhlNodeDescriptor> result = new LinkedList<IhlNodeDescriptor>();
		for (Iterator<?> it = modelElement.getPowerManagers().iterator(); it.hasNext();) {
			PowerManager childElement = (PowerManager) it.next();
			int visualID = IhlVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == PowerManagerEditPart.VISUAL_ID) {
				result.add(new IhlNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getInductors().iterator(); it.hasNext();) {
			Inductor childElement = (Inductor) it.next();
			int visualID = IhlVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == InductorEditPart.VISUAL_ID) {
				result.add(new IhlNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getInverters().iterator(); it.hasNext();) {
			Inverter childElement = (Inverter) it.next();
			int visualID = IhlVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == InverterEditPart.VISUAL_ID) {
				result.add(new IhlNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<IhlLinkDescriptor> getContainedLinks(View view) {
		switch (IhlVisualIDRegistry.getVisualID(view)) {
		case InductionHobEditPart.VISUAL_ID:
			return getInductionHob_1000ContainedLinks(view);
		case PowerManagerEditPart.VISUAL_ID:
			return getPowerManager_2001ContainedLinks(view);
		case InductorEditPart.VISUAL_ID:
			return getInductor_2002ContainedLinks(view);
		case InverterEditPart.VISUAL_ID:
			return getInverter_2003ContainedLinks(view);
		case PowerChannelEditPart.VISUAL_ID:
			return getPowerChannel_4001ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<IhlLinkDescriptor> getIncomingLinks(View view) {
		switch (IhlVisualIDRegistry.getVisualID(view)) {
		case PowerManagerEditPart.VISUAL_ID:
			return getPowerManager_2001IncomingLinks(view);
		case InductorEditPart.VISUAL_ID:
			return getInductor_2002IncomingLinks(view);
		case InverterEditPart.VISUAL_ID:
			return getInverter_2003IncomingLinks(view);
		case PowerChannelEditPart.VISUAL_ID:
			return getPowerChannel_4001IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<IhlLinkDescriptor> getOutgoingLinks(View view) {
		switch (IhlVisualIDRegistry.getVisualID(view)) {
		case PowerManagerEditPart.VISUAL_ID:
			return getPowerManager_2001OutgoingLinks(view);
		case InductorEditPart.VISUAL_ID:
			return getInductor_2002OutgoingLinks(view);
		case InverterEditPart.VISUAL_ID:
			return getInverter_2003OutgoingLinks(view);
		case PowerChannelEditPart.VISUAL_ID:
			return getPowerChannel_4001OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getInductionHob_1000ContainedLinks(View view) {
		InductionHob modelElement = (InductionHob) view.getElement();
		LinkedList<IhlLinkDescriptor> result = new LinkedList<IhlLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_PowerChannel_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getPowerManager_2001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getInductor_2002ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getInverter_2003ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getPowerChannel_4001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getPowerManager_2001IncomingLinks(View view) {
		PowerManager modelElement = (PowerManager) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<IhlLinkDescriptor> result = new LinkedList<IhlLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_PowerChannel_4001(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getInductor_2002IncomingLinks(View view) {
		Inductor modelElement = (Inductor) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<IhlLinkDescriptor> result = new LinkedList<IhlLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_PowerChannel_4001(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getInverter_2003IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getPowerChannel_4001IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getPowerManager_2001OutgoingLinks(View view) {
		PowerManager modelElement = (PowerManager) view.getElement();
		LinkedList<IhlLinkDescriptor> result = new LinkedList<IhlLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_PowerChannel_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getInductor_2002OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getInverter_2003OutgoingLinks(View view) {
		Inverter modelElement = (Inverter) view.getElement();
		LinkedList<IhlLinkDescriptor> result = new LinkedList<IhlLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_PowerChannel_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IhlLinkDescriptor> getPowerChannel_4001OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	private static Collection<IhlLinkDescriptor> getContainedTypeModelFacetLinks_PowerChannel_4001(
			InductionHob container) {
		LinkedList<IhlLinkDescriptor> result = new LinkedList<IhlLinkDescriptor>();
		for (Iterator<?> links = container.getPowerChannels().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof PowerChannel) {
				continue;
			}
			PowerChannel link = (PowerChannel) linkObject;
			if (PowerChannelEditPart.VISUAL_ID != IhlVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			TargetElement dst = link.getTo();
			SourceElement src = link.getFrom();
			result.add(new IhlLinkDescriptor(src, dst, link, IhlElementTypes.PowerChannel_4001,
					PowerChannelEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<IhlLinkDescriptor> getIncomingTypeModelFacetLinks_PowerChannel_4001(TargetElement target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<IhlLinkDescriptor> result = new LinkedList<IhlLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != IhlPackage.eINSTANCE.getPowerChannel_To()
					|| false == setting.getEObject() instanceof PowerChannel) {
				continue;
			}
			PowerChannel link = (PowerChannel) setting.getEObject();
			if (PowerChannelEditPart.VISUAL_ID != IhlVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			SourceElement src = link.getFrom();
			result.add(new IhlLinkDescriptor(src, target, link, IhlElementTypes.PowerChannel_4001,
					PowerChannelEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	private static Collection<IhlLinkDescriptor> getOutgoingTypeModelFacetLinks_PowerChannel_4001(
			SourceElement source) {
		InductionHob container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element.eContainer()) {
			if (element instanceof InductionHob) {
				container = (InductionHob) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<IhlLinkDescriptor> result = new LinkedList<IhlLinkDescriptor>();
		for (Iterator<?> links = container.getPowerChannels().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof PowerChannel) {
				continue;
			}
			PowerChannel link = (PowerChannel) linkObject;
			if (PowerChannelEditPart.VISUAL_ID != IhlVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			TargetElement dst = link.getTo();
			SourceElement src = link.getFrom();
			if (src != source) {
				continue;
			}
			result.add(new IhlLinkDescriptor(src, dst, link, IhlElementTypes.PowerChannel_4001,
					PowerChannelEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		* @generated
		*/
		@Override

		public List<IhlNodeDescriptor> getSemanticChildren(View view) {
			return IhlDiagramUpdater.getSemanticChildren(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<IhlLinkDescriptor> getContainedLinks(View view) {
			return IhlDiagramUpdater.getContainedLinks(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<IhlLinkDescriptor> getIncomingLinks(View view) {
			return IhlDiagramUpdater.getIncomingLinks(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<IhlLinkDescriptor> getOutgoingLinks(View view) {
			return IhlDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
