
package ihl.diagram.part;

import java.util.Collections;

import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.tooling.runtime.part.DefaultNodeToolEntry;

import ihl.diagram.providers.IhlElementTypes;

/**
 * @generated
 */
public class IhlPaletteFactory {

	/**
	* @generated
	*/
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createIhl1Group());
	}

	/**
	* Creates "ihl" palette tool group
	* @generated
	*/
	private PaletteContainer createIhl1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(Messages.Ihl1Group_title);
		paletteContainer.setId("createIhl1Group"); //$NON-NLS-1$
		paletteContainer.add(createInverter1CreationTool());
		paletteContainer.add(createInductor2CreationTool());
		paletteContainer.add(createPowerManager3CreationTool());
		return paletteContainer;
	}

	/**
	* @generated
	*/
	private ToolEntry createInverter1CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Inverter1CreationTool_title,
				Messages.Inverter1CreationTool_desc, Collections.singletonList(IhlElementTypes.Inverter_2003));
		entry.setId("createInverter1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IhlElementTypes.getImageDescriptor(IhlElementTypes.Inverter_2003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createInductor2CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.Inductor2CreationTool_title,
				Messages.Inductor2CreationTool_desc, Collections.singletonList(IhlElementTypes.Inductor_2002));
		entry.setId("createInductor2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IhlElementTypes.getImageDescriptor(IhlElementTypes.Inductor_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createPowerManager3CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.PowerManager3CreationTool_title,
				Messages.PowerManager3CreationTool_desc, Collections.singletonList(IhlElementTypes.PowerManager_2001));
		entry.setId("createPowerManager3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IhlElementTypes.getImageDescriptor(IhlElementTypes.PowerManager_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

}
