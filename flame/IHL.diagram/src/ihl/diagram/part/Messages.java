package ihl.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	* @generated
	*/
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private Messages() {
	}

	/**
	* @generated
	*/
	public static String IhlCreationWizardTitle;

	/**
	* @generated
	*/
	public static String IhlCreationWizard_DiagramModelFilePageTitle;

	/**
	* @generated
	*/
	public static String IhlCreationWizard_DiagramModelFilePageDescription;

	/**
	* @generated
	*/
	public static String IhlCreationWizard_DomainModelFilePageTitle;

	/**
	* @generated
	*/
	public static String IhlCreationWizard_DomainModelFilePageDescription;

	/**
	* @generated
	*/
	public static String IhlCreationWizardOpenEditorError;

	/**
	* @generated
	*/
	public static String IhlCreationWizardCreationError;

	/**
	* @generated
	*/
	public static String IhlCreationWizardPageExtensionError;

	/**
	* @generated
	*/
	public static String IhlDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	* @generated
	*/
	public static String IhlDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	* @generated
	*/
	public static String IhlDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	* @generated
	*/
	public static String IhlDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_isModifiable;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_handleElementContentChanged;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_IncorrectInputError;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_NoDiagramInResourceError;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_DiagramLoadingError;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_UnsynchronizedFileSaveError;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_SaveDiagramTask;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_SaveNextResourceTask;

	/**
	* @generated
	*/
	public static String IhlDocumentProvider_SaveAsOperation;

	/**
	* @generated
	*/
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	* @generated
	*/
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	* @generated
	*/
	public static String InitDiagramFile_WizardTitle;

	/**
	* @generated
	*/
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_CreationPageName;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_CreationPageTitle;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_CreationPageDescription;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_RootSelectionPageName;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_InitDiagramCommand;

	/**
	* @generated
	*/
	public static String IhlNewDiagramFileWizard_IncorrectRootError;

	/**
	* @generated
	*/
	public static String IhlDiagramEditor_SavingDeletedFile;

	/**
	* @generated
	*/
	public static String IhlDiagramEditor_SaveAsErrorTitle;

	/**
	* @generated
	*/
	public static String IhlDiagramEditor_SaveAsErrorMessage;

	/**
	* @generated
	*/
	public static String IhlDiagramEditor_SaveErrorTitle;

	/**
	* @generated
	*/
	public static String IhlDiagramEditor_SaveErrorMessage;

	/**
	* @generated
	*/
	public static String IhlElementChooserDialog_SelectModelElementTitle;

	/**
	* @generated
	*/
	public static String ModelElementSelectionPageMessage;

	/**
	* @generated
	*/
	public static String ValidateActionMessage;

	/**
	* @generated
	*/
	public static String Ihl1Group_title;

	/**
	* @generated
	*/
	public static String Inverter1CreationTool_title;

	/**
	* @generated
	*/
	public static String Inverter1CreationTool_desc;

	/**
	* @generated
	*/
	public static String Inductor2CreationTool_title;

	/**
	* @generated
	*/
	public static String Inductor2CreationTool_desc;

	/**
	* @generated
	*/
	public static String PowerManager3CreationTool_title;

	/**
	* @generated
	*/
	public static String PowerManager3CreationTool_desc;

	/**
	* @generated
	*/
	public static String CommandName_OpenDiagram;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_InductionHob_1000_links;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_PowerManager_2001_incominglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_PowerManager_2001_outgoinglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Inductor_2002_incominglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Inverter_2003_outgoinglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_PowerChannel_4001_target;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_PowerChannel_4001_source;

	/**
	* @generated
	*/
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	* @generated
	*/
	public static String IhlModelingAssistantProviderTitle;

	/**
	* @generated
	*/
	public static String IhlModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
