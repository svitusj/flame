package ihl.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import ihl.diagram.part.IhlVisualIDRegistry;

/**
 * @generated
 */
public class IhlNavigatorSorter extends ViewerSorter {

	/**
	* @generated
	*/
	private static final int GROUP_CATEGORY = 4003;

	/**
	* @generated
	*/
	public int category(Object element) {
		if (element instanceof IhlNavigatorItem) {
			IhlNavigatorItem item = (IhlNavigatorItem) element;
			return IhlVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
