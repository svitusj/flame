package ihl.diagram.navigator;

import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import ihl.InductionHob;
import ihl.Inductor;
import ihl.Inverter;
import ihl.PowerChannel;
import ihl.PowerManager;
import ihl.diagram.edit.parts.InductionHobEditPart;
import ihl.diagram.edit.parts.InductorEditPart;
import ihl.diagram.edit.parts.InverterEditPart;
import ihl.diagram.edit.parts.PowerChannelEditPart;
import ihl.diagram.edit.parts.PowerManagerEditPart;
import ihl.diagram.part.IhlDiagramEditorPlugin;
import ihl.diagram.part.IhlVisualIDRegistry;
import ihl.diagram.providers.IhlElementTypes;

/**
 * @generated
 */
public class IhlNavigatorLabelProvider extends LabelProvider implements ICommonLabelProvider, ITreePathLabelProvider {

	/**
	* @generated
	*/
	static {
		IhlDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?UnknownElement", //$NON-NLS-1$
				ImageDescriptor.getMissingImageDescriptor());
		IhlDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?ImageNotFound", //$NON-NLS-1$
				ImageDescriptor.getMissingImageDescriptor());
	}

	/**
	* @generated
	*/
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof IhlNavigatorItem && !isOwnView(((IhlNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	* @generated
	*/
	public Image getImage(Object element) {
		if (element instanceof IhlNavigatorGroup) {
			IhlNavigatorGroup group = (IhlNavigatorGroup) element;
			return IhlDiagramEditorPlugin.getInstance().getBundledImage(group.getIcon());
		}

		if (element instanceof IhlNavigatorItem) {
			IhlNavigatorItem navigatorItem = (IhlNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	* @generated
	*/
	public Image getImage(View view) {
		switch (IhlVisualIDRegistry.getVisualID(view)) {
		case InductionHobEditPart.VISUAL_ID:
			return getImage("Navigator?Diagram?http://org/eclipse/ihl?InductionHob", IhlElementTypes.InductionHob_1000); //$NON-NLS-1$
		case PowerManagerEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://org/eclipse/ihl?PowerManager", //$NON-NLS-1$
					IhlElementTypes.PowerManager_2001);
		case InductorEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://org/eclipse/ihl?Inductor", IhlElementTypes.Inductor_2002); //$NON-NLS-1$
		case InverterEditPart.VISUAL_ID:
			return getImage("Navigator?TopLevelNode?http://org/eclipse/ihl?Inverter", IhlElementTypes.Inverter_2003); //$NON-NLS-1$
		case PowerChannelEditPart.VISUAL_ID:
			return getImage("Navigator?Link?http://org/eclipse/ihl?PowerChannel", IhlElementTypes.PowerChannel_4001); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = IhlDiagramEditorPlugin.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null && IhlElementTypes.isKnownElementType(elementType)) {
			image = IhlElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	* @generated
	*/
	public String getText(Object element) {
		if (element instanceof IhlNavigatorGroup) {
			IhlNavigatorGroup group = (IhlNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof IhlNavigatorItem) {
			IhlNavigatorItem navigatorItem = (IhlNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	* @generated
	*/
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (IhlVisualIDRegistry.getVisualID(view)) {
		case InductionHobEditPart.VISUAL_ID:
			return getInductionHob_1000Text(view);
		case PowerManagerEditPart.VISUAL_ID:
			return getPowerManager_2001Text(view);
		case InductorEditPart.VISUAL_ID:
			return getInductor_2002Text(view);
		case InverterEditPart.VISUAL_ID:
			return getInverter_2003Text(view);
		case PowerChannelEditPart.VISUAL_ID:
			return getPowerChannel_4001Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	* @generated
	*/
	private String getInductionHob_1000Text(View view) {
		InductionHob domainModelElement = (InductionHob) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getID();
		} else {
			IhlDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getPowerManager_2001Text(View view) {
		PowerManager domainModelElement = (PowerManager) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getID();
		} else {
			IhlDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 2001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getInductor_2002Text(View view) {
		Inductor domainModelElement = (Inductor) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getID();
		} else {
			IhlDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 2002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getInverter_2003Text(View view) {
		Inverter domainModelElement = (Inverter) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getID();
		} else {
			IhlDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 2003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getPowerChannel_4001Text(View view) {
		PowerChannel domainModelElement = (PowerChannel) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getID();
		} else {
			IhlDiagramEditorPlugin.getInstance().logError("No domain element for view with visualID = " + 4001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	* @generated
	*/
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	* @generated
	*/
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	* @generated
	*/
	public void restoreState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public void saveState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	* @generated
	*/
	private boolean isOwnView(View view) {
		return InductionHobEditPart.MODEL_ID.equals(IhlVisualIDRegistry.getModelID(view));
	}

}
