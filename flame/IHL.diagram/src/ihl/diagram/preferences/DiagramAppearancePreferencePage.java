package ihl.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.AppearancePreferencePage;

import ihl.diagram.part.IhlDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramAppearancePreferencePage extends AppearancePreferencePage {

	/**
	* @generated
	*/
	public DiagramAppearancePreferencePage() {
		setPreferenceStore(IhlDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
