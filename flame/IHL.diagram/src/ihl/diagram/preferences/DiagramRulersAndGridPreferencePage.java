package ihl.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

import ihl.diagram.part.IhlDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	* @generated
	*/
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(IhlDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
