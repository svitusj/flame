/**
 */
package ihl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ihl.IhlPackage
 * @generated
 */
public interface IhlFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IhlFactory eINSTANCE = ihl.impl.IhlFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Inverter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inverter</em>'.
	 * @generated
	 */
	Inverter createInverter();

	/**
	 * Returns a new object of class '<em>Induction Hob</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Induction Hob</em>'.
	 * @generated
	 */
	InductionHob createInductionHob();

	/**
	 * Returns a new object of class '<em>Inductor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inductor</em>'.
	 * @generated
	 */
	Inductor createInductor();

	/**
	 * Returns a new object of class '<em>Power Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Power Manager</em>'.
	 * @generated
	 */
	PowerManager createPowerManager();

	/**
	 * Returns a new object of class '<em>Power Channel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Power Channel</em>'.
	 * @generated
	 */
	PowerChannel createPowerChannel();

	/**
	 * Returns a new object of class '<em>Source Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Source Element</em>'.
	 * @generated
	 */
	SourceElement createSourceElement();

	/**
	 * Returns a new object of class '<em>Target Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Target Element</em>'.
	 * @generated
	 */
	TargetElement createTargetElement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	IhlPackage getIhlPackage();

} //IhlFactory
