/**
 */
package ihl.impl;

import ihl.IhlPackage;
import ihl.InductionHob;
import ihl.Inductor;
import ihl.Inverter;
import ihl.PowerChannel;
import ihl.PowerManager;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Induction Hob</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ihl.impl.InductionHobImpl#getID <em>ID</em>}</li>
 *   <li>{@link ihl.impl.InductionHobImpl#getPowerChannels <em>Power Channels</em>}</li>
 *   <li>{@link ihl.impl.InductionHobImpl#getPowerManagers <em>Power Managers</em>}</li>
 *   <li>{@link ihl.impl.InductionHobImpl#getInductors <em>Inductors</em>}</li>
 *   <li>{@link ihl.impl.InductionHobImpl#getInverters <em>Inverters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InductionHobImpl extends MinimalEObjectImpl.Container implements InductionHob {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPowerChannels() <em>Power Channels</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPowerChannels()
	 * @generated
	 * @ordered
	 */
	protected EList<PowerChannel> powerChannels;

	/**
	 * The cached value of the '{@link #getPowerManagers() <em>Power Managers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPowerManagers()
	 * @generated
	 * @ordered
	 */
	protected EList<PowerManager> powerManagers;

	/**
	 * The cached value of the '{@link #getInductors() <em>Inductors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInductors()
	 * @generated
	 * @ordered
	 */
	protected EList<Inductor> inductors;

	/**
	 * The cached value of the '{@link #getInverters() <em>Inverters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInverters()
	 * @generated
	 * @ordered
	 */
	protected EList<Inverter> inverters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InductionHobImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IhlPackage.Literals.INDUCTION_HOB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		String oldID = id;
		id = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IhlPackage.INDUCTION_HOB__ID, oldID, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PowerChannel> getPowerChannels() {
		if (powerChannels == null) {
			powerChannels = new EObjectContainmentEList<PowerChannel>(PowerChannel.class, this, IhlPackage.INDUCTION_HOB__POWER_CHANNELS);
		}
		return powerChannels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PowerManager> getPowerManagers() {
		if (powerManagers == null) {
			powerManagers = new EObjectContainmentEList<PowerManager>(PowerManager.class, this, IhlPackage.INDUCTION_HOB__POWER_MANAGERS);
		}
		return powerManagers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Inductor> getInductors() {
		if (inductors == null) {
			inductors = new EObjectContainmentEList<Inductor>(Inductor.class, this, IhlPackage.INDUCTION_HOB__INDUCTORS);
		}
		return inductors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Inverter> getInverters() {
		if (inverters == null) {
			inverters = new EObjectContainmentEList<Inverter>(Inverter.class, this, IhlPackage.INDUCTION_HOB__INVERTERS);
		}
		return inverters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case IhlPackage.INDUCTION_HOB__POWER_CHANNELS:
				return ((InternalEList<?>)getPowerChannels()).basicRemove(otherEnd, msgs);
			case IhlPackage.INDUCTION_HOB__POWER_MANAGERS:
				return ((InternalEList<?>)getPowerManagers()).basicRemove(otherEnd, msgs);
			case IhlPackage.INDUCTION_HOB__INDUCTORS:
				return ((InternalEList<?>)getInductors()).basicRemove(otherEnd, msgs);
			case IhlPackage.INDUCTION_HOB__INVERTERS:
				return ((InternalEList<?>)getInverters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IhlPackage.INDUCTION_HOB__ID:
				return getID();
			case IhlPackage.INDUCTION_HOB__POWER_CHANNELS:
				return getPowerChannels();
			case IhlPackage.INDUCTION_HOB__POWER_MANAGERS:
				return getPowerManagers();
			case IhlPackage.INDUCTION_HOB__INDUCTORS:
				return getInductors();
			case IhlPackage.INDUCTION_HOB__INVERTERS:
				return getInverters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IhlPackage.INDUCTION_HOB__ID:
				setID((String)newValue);
				return;
			case IhlPackage.INDUCTION_HOB__POWER_CHANNELS:
				getPowerChannels().clear();
				getPowerChannels().addAll((Collection<? extends PowerChannel>)newValue);
				return;
			case IhlPackage.INDUCTION_HOB__POWER_MANAGERS:
				getPowerManagers().clear();
				getPowerManagers().addAll((Collection<? extends PowerManager>)newValue);
				return;
			case IhlPackage.INDUCTION_HOB__INDUCTORS:
				getInductors().clear();
				getInductors().addAll((Collection<? extends Inductor>)newValue);
				return;
			case IhlPackage.INDUCTION_HOB__INVERTERS:
				getInverters().clear();
				getInverters().addAll((Collection<? extends Inverter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IhlPackage.INDUCTION_HOB__ID:
				setID(ID_EDEFAULT);
				return;
			case IhlPackage.INDUCTION_HOB__POWER_CHANNELS:
				getPowerChannels().clear();
				return;
			case IhlPackage.INDUCTION_HOB__POWER_MANAGERS:
				getPowerManagers().clear();
				return;
			case IhlPackage.INDUCTION_HOB__INDUCTORS:
				getInductors().clear();
				return;
			case IhlPackage.INDUCTION_HOB__INVERTERS:
				getInverters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IhlPackage.INDUCTION_HOB__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case IhlPackage.INDUCTION_HOB__POWER_CHANNELS:
				return powerChannels != null && !powerChannels.isEmpty();
			case IhlPackage.INDUCTION_HOB__POWER_MANAGERS:
				return powerManagers != null && !powerManagers.isEmpty();
			case IhlPackage.INDUCTION_HOB__INDUCTORS:
				return inductors != null && !inductors.isEmpty();
			case IhlPackage.INDUCTION_HOB__INVERTERS:
				return inverters != null && !inverters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ID: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //InductionHobImpl
