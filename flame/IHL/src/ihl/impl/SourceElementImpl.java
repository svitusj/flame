/**
 */
package ihl.impl;

import ihl.IhlPackage;
import ihl.SourceElement;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Source Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SourceElementImpl extends MinimalEObjectImpl.Container implements SourceElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SourceElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IhlPackage.Literals.SOURCE_ELEMENT;
	}

} //SourceElementImpl
