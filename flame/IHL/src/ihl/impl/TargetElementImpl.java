/**
 */
package ihl.impl;

import ihl.IhlPackage;
import ihl.TargetElement;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Target Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TargetElementImpl extends MinimalEObjectImpl.Container implements TargetElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TargetElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IhlPackage.Literals.TARGET_ELEMENT;
	}

} //TargetElementImpl
