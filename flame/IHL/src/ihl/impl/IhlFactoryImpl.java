/**
 */
package ihl.impl;

import ihl.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IhlFactoryImpl extends EFactoryImpl implements IhlFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static IhlFactory init() {
		try {
			IhlFactory theIhlFactory = (IhlFactory)EPackage.Registry.INSTANCE.getEFactory(IhlPackage.eNS_URI);
			if (theIhlFactory != null) {
				return theIhlFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new IhlFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IhlFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case IhlPackage.INVERTER: return createInverter();
			case IhlPackage.INDUCTION_HOB: return createInductionHob();
			case IhlPackage.INDUCTOR: return createInductor();
			case IhlPackage.POWER_MANAGER: return createPowerManager();
			case IhlPackage.POWER_CHANNEL: return createPowerChannel();
			case IhlPackage.SOURCE_ELEMENT: return createSourceElement();
			case IhlPackage.TARGET_ELEMENT: return createTargetElement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Inverter createInverter() {
		InverterImpl inverter = new InverterImpl();
		return inverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InductionHob createInductionHob() {
		InductionHobImpl inductionHob = new InductionHobImpl();
		return inductionHob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Inductor createInductor() {
		InductorImpl inductor = new InductorImpl();
		return inductor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PowerManager createPowerManager() {
		PowerManagerImpl powerManager = new PowerManagerImpl();
		return powerManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PowerChannel createPowerChannel() {
		PowerChannelImpl powerChannel = new PowerChannelImpl();
		return powerChannel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceElement createSourceElement() {
		SourceElementImpl sourceElement = new SourceElementImpl();
		return sourceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TargetElement createTargetElement() {
		TargetElementImpl targetElement = new TargetElementImpl();
		return targetElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IhlPackage getIhlPackage() {
		return (IhlPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static IhlPackage getPackage() {
		return IhlPackage.eINSTANCE;
	}

} //IhlFactoryImpl
