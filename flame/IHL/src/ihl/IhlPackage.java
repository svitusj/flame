/**
 */
package ihl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ihl.IhlFactory
 * @model kind="package"
 * @generated
 */
public interface IhlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ihl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org/eclipse/ihl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.eclipse.ihl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IhlPackage eINSTANCE = ihl.impl.IhlPackageImpl.init();

	/**
	 * The meta object id for the '{@link ihl.impl.SourceElementImpl <em>Source Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ihl.impl.SourceElementImpl
	 * @see ihl.impl.IhlPackageImpl#getSourceElement()
	 * @generated
	 */
	int SOURCE_ELEMENT = 5;

	/**
	 * The number of structural features of the '<em>Source Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Source Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ihl.impl.InverterImpl <em>Inverter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ihl.impl.InverterImpl
	 * @see ihl.impl.IhlPackageImpl#getInverter()
	 * @generated
	 */
	int INVERTER = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__ID = SOURCE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Inverter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_FEATURE_COUNT = SOURCE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Inverter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_OPERATION_COUNT = SOURCE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ihl.impl.InductionHobImpl <em>Induction Hob</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ihl.impl.InductionHobImpl
	 * @see ihl.impl.IhlPackageImpl#getInductionHob()
	 * @generated
	 */
	int INDUCTION_HOB = 1;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTION_HOB__ID = 0;

	/**
	 * The feature id for the '<em><b>Power Channels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTION_HOB__POWER_CHANNELS = 1;

	/**
	 * The feature id for the '<em><b>Power Managers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTION_HOB__POWER_MANAGERS = 2;

	/**
	 * The feature id for the '<em><b>Inductors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTION_HOB__INDUCTORS = 3;

	/**
	 * The feature id for the '<em><b>Inverters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTION_HOB__INVERTERS = 4;

	/**
	 * The number of structural features of the '<em>Induction Hob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTION_HOB_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Induction Hob</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTION_HOB_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ihl.impl.TargetElementImpl <em>Target Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ihl.impl.TargetElementImpl
	 * @see ihl.impl.IhlPackageImpl#getTargetElement()
	 * @generated
	 */
	int TARGET_ELEMENT = 6;

	/**
	 * The number of structural features of the '<em>Target Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Target Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ihl.impl.InductorImpl <em>Inductor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ihl.impl.InductorImpl
	 * @see ihl.impl.IhlPackageImpl#getInductor()
	 * @generated
	 */
	int INDUCTOR = 2;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTOR__ID = TARGET_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Inductor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTOR_FEATURE_COUNT = TARGET_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Inductor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDUCTOR_OPERATION_COUNT = TARGET_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ihl.impl.PowerManagerImpl <em>Power Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ihl.impl.PowerManagerImpl
	 * @see ihl.impl.IhlPackageImpl#getPowerManager()
	 * @generated
	 */
	int POWER_MANAGER = 3;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_MANAGER__ID = SOURCE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Power Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_MANAGER_FEATURE_COUNT = SOURCE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Power Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_MANAGER_OPERATION_COUNT = SOURCE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ihl.impl.PowerChannelImpl <em>Power Channel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ihl.impl.PowerChannelImpl
	 * @see ihl.impl.IhlPackageImpl#getPowerChannel()
	 * @generated
	 */
	int POWER_CHANNEL = 4;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_CHANNEL__ID = 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_CHANNEL__FROM = 1;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_CHANNEL__TO = 2;

	/**
	 * The number of structural features of the '<em>Power Channel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_CHANNEL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Power Channel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_CHANNEL_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link ihl.Inverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inverter</em>'.
	 * @see ihl.Inverter
	 * @generated
	 */
	EClass getInverter();

	/**
	 * Returns the meta object for the attribute '{@link ihl.Inverter#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see ihl.Inverter#getID()
	 * @see #getInverter()
	 * @generated
	 */
	EAttribute getInverter_ID();

	/**
	 * Returns the meta object for class '{@link ihl.InductionHob <em>Induction Hob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Induction Hob</em>'.
	 * @see ihl.InductionHob
	 * @generated
	 */
	EClass getInductionHob();

	/**
	 * Returns the meta object for the attribute '{@link ihl.InductionHob#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see ihl.InductionHob#getID()
	 * @see #getInductionHob()
	 * @generated
	 */
	EAttribute getInductionHob_ID();

	/**
	 * Returns the meta object for the containment reference list '{@link ihl.InductionHob#getPowerChannels <em>Power Channels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Power Channels</em>'.
	 * @see ihl.InductionHob#getPowerChannels()
	 * @see #getInductionHob()
	 * @generated
	 */
	EReference getInductionHob_PowerChannels();

	/**
	 * Returns the meta object for the containment reference list '{@link ihl.InductionHob#getPowerManagers <em>Power Managers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Power Managers</em>'.
	 * @see ihl.InductionHob#getPowerManagers()
	 * @see #getInductionHob()
	 * @generated
	 */
	EReference getInductionHob_PowerManagers();

	/**
	 * Returns the meta object for the containment reference list '{@link ihl.InductionHob#getInductors <em>Inductors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inductors</em>'.
	 * @see ihl.InductionHob#getInductors()
	 * @see #getInductionHob()
	 * @generated
	 */
	EReference getInductionHob_Inductors();

	/**
	 * Returns the meta object for the containment reference list '{@link ihl.InductionHob#getInverters <em>Inverters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverters</em>'.
	 * @see ihl.InductionHob#getInverters()
	 * @see #getInductionHob()
	 * @generated
	 */
	EReference getInductionHob_Inverters();

	/**
	 * Returns the meta object for class '{@link ihl.Inductor <em>Inductor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inductor</em>'.
	 * @see ihl.Inductor
	 * @generated
	 */
	EClass getInductor();

	/**
	 * Returns the meta object for the attribute '{@link ihl.Inductor#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see ihl.Inductor#getID()
	 * @see #getInductor()
	 * @generated
	 */
	EAttribute getInductor_ID();

	/**
	 * Returns the meta object for class '{@link ihl.PowerManager <em>Power Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Power Manager</em>'.
	 * @see ihl.PowerManager
	 * @generated
	 */
	EClass getPowerManager();

	/**
	 * Returns the meta object for the attribute '{@link ihl.PowerManager#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see ihl.PowerManager#getID()
	 * @see #getPowerManager()
	 * @generated
	 */
	EAttribute getPowerManager_ID();

	/**
	 * Returns the meta object for class '{@link ihl.PowerChannel <em>Power Channel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Power Channel</em>'.
	 * @see ihl.PowerChannel
	 * @generated
	 */
	EClass getPowerChannel();

	/**
	 * Returns the meta object for the attribute '{@link ihl.PowerChannel#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see ihl.PowerChannel#getID()
	 * @see #getPowerChannel()
	 * @generated
	 */
	EAttribute getPowerChannel_ID();

	/**
	 * Returns the meta object for the reference '{@link ihl.PowerChannel#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see ihl.PowerChannel#getFrom()
	 * @see #getPowerChannel()
	 * @generated
	 */
	EReference getPowerChannel_From();

	/**
	 * Returns the meta object for the reference '{@link ihl.PowerChannel#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see ihl.PowerChannel#getTo()
	 * @see #getPowerChannel()
	 * @generated
	 */
	EReference getPowerChannel_To();

	/**
	 * Returns the meta object for class '{@link ihl.SourceElement <em>Source Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Element</em>'.
	 * @see ihl.SourceElement
	 * @generated
	 */
	EClass getSourceElement();

	/**
	 * Returns the meta object for class '{@link ihl.TargetElement <em>Target Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Element</em>'.
	 * @see ihl.TargetElement
	 * @generated
	 */
	EClass getTargetElement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IhlFactory getIhlFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ihl.impl.InverterImpl <em>Inverter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ihl.impl.InverterImpl
		 * @see ihl.impl.IhlPackageImpl#getInverter()
		 * @generated
		 */
		EClass INVERTER = eINSTANCE.getInverter();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INVERTER__ID = eINSTANCE.getInverter_ID();

		/**
		 * The meta object literal for the '{@link ihl.impl.InductionHobImpl <em>Induction Hob</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ihl.impl.InductionHobImpl
		 * @see ihl.impl.IhlPackageImpl#getInductionHob()
		 * @generated
		 */
		EClass INDUCTION_HOB = eINSTANCE.getInductionHob();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INDUCTION_HOB__ID = eINSTANCE.getInductionHob_ID();

		/**
		 * The meta object literal for the '<em><b>Power Channels</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INDUCTION_HOB__POWER_CHANNELS = eINSTANCE.getInductionHob_PowerChannels();

		/**
		 * The meta object literal for the '<em><b>Power Managers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INDUCTION_HOB__POWER_MANAGERS = eINSTANCE.getInductionHob_PowerManagers();

		/**
		 * The meta object literal for the '<em><b>Inductors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INDUCTION_HOB__INDUCTORS = eINSTANCE.getInductionHob_Inductors();

		/**
		 * The meta object literal for the '<em><b>Inverters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INDUCTION_HOB__INVERTERS = eINSTANCE.getInductionHob_Inverters();

		/**
		 * The meta object literal for the '{@link ihl.impl.InductorImpl <em>Inductor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ihl.impl.InductorImpl
		 * @see ihl.impl.IhlPackageImpl#getInductor()
		 * @generated
		 */
		EClass INDUCTOR = eINSTANCE.getInductor();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INDUCTOR__ID = eINSTANCE.getInductor_ID();

		/**
		 * The meta object literal for the '{@link ihl.impl.PowerManagerImpl <em>Power Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ihl.impl.PowerManagerImpl
		 * @see ihl.impl.IhlPackageImpl#getPowerManager()
		 * @generated
		 */
		EClass POWER_MANAGER = eINSTANCE.getPowerManager();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POWER_MANAGER__ID = eINSTANCE.getPowerManager_ID();

		/**
		 * The meta object literal for the '{@link ihl.impl.PowerChannelImpl <em>Power Channel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ihl.impl.PowerChannelImpl
		 * @see ihl.impl.IhlPackageImpl#getPowerChannel()
		 * @generated
		 */
		EClass POWER_CHANNEL = eINSTANCE.getPowerChannel();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POWER_CHANNEL__ID = eINSTANCE.getPowerChannel_ID();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POWER_CHANNEL__FROM = eINSTANCE.getPowerChannel_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POWER_CHANNEL__TO = eINSTANCE.getPowerChannel_To();

		/**
		 * The meta object literal for the '{@link ihl.impl.SourceElementImpl <em>Source Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ihl.impl.SourceElementImpl
		 * @see ihl.impl.IhlPackageImpl#getSourceElement()
		 * @generated
		 */
		EClass SOURCE_ELEMENT = eINSTANCE.getSourceElement();

		/**
		 * The meta object literal for the '{@link ihl.impl.TargetElementImpl <em>Target Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ihl.impl.TargetElementImpl
		 * @see ihl.impl.IhlPackageImpl#getTargetElement()
		 * @generated
		 */
		EClass TARGET_ELEMENT = eINSTANCE.getTargetElement();

	}

} //IhlPackage
