/**
 */
package ihl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Power Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ihl.PowerManager#getID <em>ID</em>}</li>
 * </ul>
 *
 * @see ihl.IhlPackage#getPowerManager()
 * @model
 * @generated
 */
public interface PowerManager extends SourceElement, TargetElement {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see ihl.IhlPackage#getPowerManager_ID()
	 * @model
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link ihl.PowerManager#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

} // PowerManager
