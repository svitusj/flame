/**
 */
package ihl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ihl.IhlPackage#getTargetElement()
 * @model
 * @generated
 */
public interface TargetElement extends EObject {
} // TargetElement
