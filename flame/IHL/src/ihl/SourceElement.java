/**
 */
package ihl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ihl.IhlPackage#getSourceElement()
 * @model
 * @generated
 */
public interface SourceElement extends EObject {
} // SourceElement
