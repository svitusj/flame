/**
 */
package ihl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Induction Hob</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ihl.InductionHob#getID <em>ID</em>}</li>
 *   <li>{@link ihl.InductionHob#getPowerChannels <em>Power Channels</em>}</li>
 *   <li>{@link ihl.InductionHob#getPowerManagers <em>Power Managers</em>}</li>
 *   <li>{@link ihl.InductionHob#getInductors <em>Inductors</em>}</li>
 *   <li>{@link ihl.InductionHob#getInverters <em>Inverters</em>}</li>
 * </ul>
 *
 * @see ihl.IhlPackage#getInductionHob()
 * @model
 * @generated
 */
public interface InductionHob extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see ihl.IhlPackage#getInductionHob_ID()
	 * @model
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link ihl.InductionHob#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Power Channels</b></em>' containment reference list.
	 * The list contents are of type {@link ihl.PowerChannel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power Channels</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power Channels</em>' containment reference list.
	 * @see ihl.IhlPackage#getInductionHob_PowerChannels()
	 * @model containment="true"
	 * @generated
	 */
	EList<PowerChannel> getPowerChannels();

	/**
	 * Returns the value of the '<em><b>Power Managers</b></em>' containment reference list.
	 * The list contents are of type {@link ihl.PowerManager}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power Managers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power Managers</em>' containment reference list.
	 * @see ihl.IhlPackage#getInductionHob_PowerManagers()
	 * @model containment="true"
	 * @generated
	 */
	EList<PowerManager> getPowerManagers();

	/**
	 * Returns the value of the '<em><b>Inductors</b></em>' containment reference list.
	 * The list contents are of type {@link ihl.Inductor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inductors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inductors</em>' containment reference list.
	 * @see ihl.IhlPackage#getInductionHob_Inductors()
	 * @model containment="true"
	 * @generated
	 */
	EList<Inductor> getInductors();

	/**
	 * Returns the value of the '<em><b>Inverters</b></em>' containment reference list.
	 * The list contents are of type {@link ihl.Inverter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inverters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inverters</em>' containment reference list.
	 * @see ihl.IhlPackage#getInductionHob_Inverters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Inverter> getInverters();

} // InductionHob
