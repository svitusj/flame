/**
 */
package ihl.tests;

import ihl.IhlFactory;
import ihl.InductionHob;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Induction Hob</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class InductionHobTest extends TestCase {

	/**
	 * The fixture for this Induction Hob test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InductionHob fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InductionHobTest.class);
	}

	/**
	 * Constructs a new Induction Hob test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InductionHobTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Induction Hob test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(InductionHob fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Induction Hob test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InductionHob getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(IhlFactory.eINSTANCE.createInductionHob());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //InductionHobTest
