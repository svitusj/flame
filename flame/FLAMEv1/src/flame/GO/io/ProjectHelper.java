package flame.GO.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import flame.GO.core.BitSet;
import flame.GO.utils.Constants;

public class ProjectHelper {

	private IProject project;
	private static ProjectHelper instance;

	public static ProjectHelper getInstance() {
		if (instance == null)
			instance = new ProjectHelper();

		return instance;
	}

	private ProjectHelper() {
		checkProject();
	}

	private void checkProject() {

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		project = root.getProject(Constants.PROJECT_NAME);
		try {
			if (!project.exists()) {
				// project.delete(true, new NullProgressMonitor());
				project.create(null);
			}
			project.open(null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public BufferedWriter createFile(String name) {
		return createFile(name, "");
	}

	public BufferedWriter createFile(String name, String content) {
		IFile f = project.getFile(name);
		try {
			project.refreshLocal(IFile.DEPTH_INFINITE, null);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		BufferedWriter bufferedWriter = null;
		if (!f.exists()) {

			try {
				f.create(new ByteArrayInputStream(content.getBytes()), true, null);
				File file = f.getRawLocation().makeAbsolute().toFile();

				FileWriter writer = new FileWriter(file);
				bufferedWriter = new BufferedWriter(writer, 4 * (int) Math.pow(1024, 2));
			} catch (CoreException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return bufferedWriter;
	}

	public EObject loadModel(String name) {

		IFile f = project.getFile(name);
		if (f.exists()) {

			String filename = f.getFullPath().toString();
			ResourceSet resSet = new ResourceSetImpl();
			URI uri = URI.createFileURI(filename);
			Resource resource = resSet.getResource(uri, true);
			EObject myModel = (EObject) resource.getContents().get(0);
			return myModel;

		}

		return null;

	}

	public EObject loadModel(IFile f) {

		if (f.exists()) {

			String filename = f.getFullPath().toString();
			ResourceSet resSet = new ResourceSetImpl();
			URI uri = URI.createFileURI(filename);
			Resource resource = resSet.getResource(uri, true);
			EObject myModel = (EObject) resource.getContents().get(0);
			return myModel;

		}

		return null;

	}

	public void createFile(String name, EObject content) {
		IFile f = project.getFile(name);
		if (!f.exists()) {
			ResourceSet resSet = new ResourceSetImpl();
			Resource resource = resSet.createResource(URI.createPlatformResourceURI(f.getFullPath().toString(), true));
			resource.getContents().add(content);
			try {
				resource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public ArrayList<EObject> loadModels() {

		try {
			ArrayList<EObject> objects = new ArrayList<EObject>();

			IFolder modelsfolder = project.getFolder("models/");

			for (IResource modelFolder : modelsfolder.members()) {

				if (modelFolder instanceof IFolder) {
					IFolder folder = (IFolder) modelFolder;

					for (IResource res : folder.members()) {

						if (res instanceof IFile) {
							IFile file = (IFile) res;

							if (file.getFileExtension().equals("ih62")) {
								objects.add(loadModel(file));
							}
						}

					}

				}

			}
			return objects;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public BitSet readOracle(String name) {

		BufferedReader reader;
		BitSet result=null;
		try {

			IFile f = project.getFile(name);
			File file = f.getRawLocation().makeAbsolute().toFile();

			if (f.exists()) {

				project.refreshLocal(IFile.DEPTH_INFINITE, null);
				reader = new BufferedReader(new FileReader(file));
				int size = Integer.parseInt(reader.readLine());
				result = new BitSet(size);
				String nextLine;
				int i=0;
				while((nextLine = reader.readLine()) != null) {
					
					if(Integer.parseInt(nextLine)==1)
						result.set(i);
					i++;
				}
				reader.close();
				
			}
			
		} catch (IOException | CoreException e) {

			e.printStackTrace();
		}
		return result;
	}

	public void writeOracle(String name, BitSet oracle) {

		try {
			IFile f = project.getFile(name);
			project.refreshLocal(IFile.DEPTH_INFINITE, null);
			if (!f.exists()) {

				
				f.create(new ByteArrayInputStream("".getBytes()), true, null);
				File file = f.getRawLocation().makeAbsolute().toFile();

				FileWriter writer = new FileWriter(file);
				writer.write(oracle.size()+"\r\n");
				for(int i=0;i<oracle.size();i++) {
					if(oracle.elements[i])
						writer.write("1\r\n");
					else
						writer.write("0\r\n");
				}
				writer.flush();
				writer.close();
			}
		} catch (CoreException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String readFile(String name) {
		IFile f = project.getFile(name);
		try {
			project.refreshLocal(IFile.DEPTH_INFINITE, null);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		BufferedReader bufferedReader = null;
		if (f.exists()) {

			try {
				bufferedReader = new BufferedReader(new InputStreamReader(f.getContents()),
						4 * (int) Math.pow(1024, 2));

				// File file = f.getRawLocation().makeAbsolute().toFile();
				String inputLine;
				String text = "";

				while ((inputLine = bufferedReader.readLine()) != null) {
					System.out.println(inputLine);
					text += inputLine;
				}

				return text;

			} catch (CoreException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return "";
	}
}
