package flame.GO.selection;

import java.util.ArrayList;
import java.util.Arrays;

import flame.GO.core.Individual;
import flame.GO.core.Population;
import flame.GO.utils.GOUtils;

public class SelectionProvider {

	public static ArrayList<Individual> rouletteWheelSelection(Population population,int size) {
		
		
		 double[] cumulativeFitnesses = new double[population.size()];
	     cumulativeFitnesses[0] = population.get(0).getFitness();
		
	    for (int i = 1; i < population.size(); i++)
        {
            double fitness = population.get(i).getFitness();
            cumulativeFitnesses[i] = cumulativeFitnesses[i - 1] + fitness;
        }
	     
	    ArrayList<Individual> selection = new ArrayList<Individual>(size);
        for (int i = 0; i < size; i++)
        {
            double randomFitness = GOUtils.getRandomDouble() * cumulativeFitnesses[cumulativeFitnesses.length - 1];
            int index = Arrays.binarySearch(cumulativeFitnesses, randomFitness);
            if (index < 0)
            {
                index = Math.abs(index + 1);
            }
            selection.add(population.get(index));
        }
        return selection;
	}
	
}
