package flame.GO.codec;

import java.util.ArrayList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import flame.GO.core.BitSet;
import flame.GO.core.Individual;

public class CODECUtils {

	
	public static Individual encode(EObject root) {
		
		CODECMapping codec = new CODECMapping(root);
		return new Individual(new BitSet(codec.getSize()),codec);
	}

	
	public static EObject decode(Individual individual) {
		
		Individual auxIndividual = new Individual(individual);
		
		EObject root = auxIndividual.getCodec().getRoot();
		TreeIterator<EObject> elements = EcoreUtil.getAllContents(root,false);
		
		ArrayList<EObject> toDelete = new ArrayList<EObject>();
		
		while (elements.hasNext()) {
			EObject object = elements.next();
			
			int index = individual.getCodec().getMapEObjectToID().get(object);
			if(!individual.getGenes().get(index)) {
				toDelete.add(object);
			}
		}
		
		for(EObject obj : toDelete) {
			EcoreUtil.delete(obj);
		}
		
		return auxIndividual.getCodec().getRoot();
	}
	
}
