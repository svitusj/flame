package flame.GO.core;

import flame.GO.codec.CODECMapping;

public class Individual {

	private BitSet genes;
	private float fitness;
	private CODECMapping codec;
	
	public BitSet getGenes() {
		return genes;
	}

	public void setGenes(BitSet genes) {
		this.genes = genes;
	}

	public float getFitness() {
		return fitness;
	}

	public void setFitness(float fitness) {
		this.fitness = fitness;
	}
	
	public Individual(BitSet bs,CODECMapping codec) {
		this.genes = bs;
		this.codec = codec;
	}
	
	public Individual(Individual individual) {
		this(individual.genes,individual.codec);
		this.fitness = individual.fitness;
		
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return genes.toString();
	}

	public CODECMapping getCodec() {
		// TODO Auto-generated method stub
		return codec;
	}
}
