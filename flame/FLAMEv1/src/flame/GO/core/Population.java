package flame.GO.core;

import java.util.ArrayList;

public class Population {

	ArrayList<Individual> individuals;
	
	public Population() {
		individuals = new ArrayList<Individual>();
	}
	
	public Individual get(int index) {
		return individuals.get(index);
	}

	public int size() {
		// TODO Auto-generated method stub
		return individuals.size();
	}
	
	public void set(Individual individual)
	{
		individuals.add(individual);
	}
}
