package flame.GO.core;

import java.io.Serializable;
import java.util.Arrays;

import flame.GO.codec.CODECMapping;

public class BitSet implements Serializable {
	
	private static final long serialVersionUID = -7831632285165159957L;
	public boolean[] elements;
	
	public BitSet(int size) {
		elements = new boolean[size];
		for (int i = 0; i < elements.length; i++) {
			elements[i] = true;
		}
	}

	public BitSet(BitSet bitSet) {
		
		elements = new boolean[bitSet.size()];
		for (int i = 0; i < elements.length; i++) {
			elements[i] = bitSet.elements[i];
		}
	}

	public void flip(int i) {
		elements[i] = !elements[i];

	}

	public int size() {
		return elements.length;
	}
	
	public int sizeInelements() {
		int size=0;
		for(int i=0;i<elements.length;i++) {
			if(elements[i])
				size++;
		}
		
		return size;
	}

	public BitSet clone() {
		return new BitSet(this);
	}

	public void negate() {

		for (int i = 0; i < elements.length; i++) {
			elements[i] = !elements[i];
		}

	}

	public boolean get(int i) {
		// TODO Auto-generated method stub
		return elements[i];
	}

	public void and(BitSet mask) {

		for (int i = 0; i < elements.length; i++) {
			elements[i] = elements[i] && mask.get(i);
		}

	}

	public void or(BitSet mask) {

		for (int i = 0; i < elements.length; i++) {
			elements[i] = elements[i] || mask.get(i);
		}

	}

	public void set(int i) {
		elements[i] = true;

	}

	public String toString() {
		String s = "";
		for (int i = 0; i < elements.length; i++) {
			if (elements[i])
				s = 1 + s;
			else
				s = 0 + s;
		}
		return s;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return Arrays.equals(elements, ((BitSet)obj).elements);
	}
	
}
