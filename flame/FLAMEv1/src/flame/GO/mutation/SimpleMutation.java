package flame.GO.mutation;

import java.util.Random;

import flame.GO.core.Individual;
import flame.GO.utils.Constants;

public class SimpleMutation {

	
	/**
	 * same probability for each model element of being added/removed
	 * 
	 * @param Individual
	 * @return Individual
	 */
	public static Individual mutate(Individual individual) {

		if(Math.random() > Constants.MUTATION_PROBABILITY)
			return individual;
		
		Random ran = new Random();
		int index = ran.nextInt(individual.getGenes().size());

		individual.getGenes().flip(index);

		return individual;
	}
	
}
