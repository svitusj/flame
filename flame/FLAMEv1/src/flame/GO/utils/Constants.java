package flame.GO.utils;

public class Constants {

	public static final float MUTATION_PROBABILITY = 1f;
	public static final double CROSSOVER_PROBABILITY = 1f;
	public static final String PROJECT_NAME = "FLAME";
	public static final String FILES_EXTENSION = ".ihl";
}
