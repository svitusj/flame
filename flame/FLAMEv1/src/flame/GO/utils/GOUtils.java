package flame.GO.utils;

import java.util.Random;

import flame.GO.core.BitSet;

public class GOUtils {

	static Random rng = new Random();
	
	public static BitSet generateRandomMask(int size) {
		
		BitSet mask = new BitSet(size);
		
		for (int i = 0; i < mask.size(); i++) {
			if (Math.random() < 0.5d) {
				mask.flip(i);
			}
		}
		
		return mask;
	}
	
	public static double getRandomDouble() {
		return rng.nextDouble();
		
	}
}
