package flame.GO.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.emf.ecore.EObject;
import org.modelversioning.ecoremutator.EcoreMutator;
import org.modelversioning.ecoremutator.IModelProvider;
import org.modelversioning.ecoremutator.mutations.ModelProvider;
import org.modelversioning.ecoremutator.mutations.impl.AddObjectMutation;
import org.modelversioning.ecoremutator.tracker.impl.CSVMutationTracker;

import ihl.IhlFactory;
import ihl.InductionHob;
import ihl.Inductor;
import ihl.Inverter;
import ihl.PowerManager;
import ihl.SourceElement;
import ihl.TargetElement;
import ihl.PowerChannel;
import flame.GO.io.ProjectHelper;

public class IHUtils {
	
	private static IhlFactory factory = IhlFactory.eINSTANCE;
	
	public static InductionHob createP1() {
		
		InductionHob p1 = factory.createInductionHob();
		p1.setID("IH1");
		
		Inverter inv = factory.createInverter();
		inv.setID("1");
		PowerManager pm = factory.createPowerManager();
		pm.setID("1");
		Inductor inductor = factory.createInductor();
		inductor.setID("1");
		PowerChannel channel_provider = factory.createPowerChannel();
		PowerChannel channel_consumer = factory.createPowerChannel();
		
		channel_provider.setFrom(inv);
		channel_provider.setTo(pm);
		
		channel_consumer.setFrom(pm);
		channel_consumer.setTo(inductor);
		
		p1.getInverters().add(inv);
		p1.getPowerManagers().add(pm);
		p1.getInductors().add(inductor);
		p1.getPowerChannels().add(channel_provider);
		p1.getPowerChannels().add(channel_consumer);
		
		inv = factory.createInverter();
		inv.setID("2");
		pm = factory.createPowerManager();
		pm.setID("2");
		inductor = factory.createInductor();
		inductor.setID("2");
		channel_provider = factory.createPowerChannel();
		channel_consumer = factory.createPowerChannel();
		
		channel_provider.setFrom(inv);
		channel_provider.setTo(pm);
		
		channel_consumer.setFrom(pm);
		channel_consumer.setTo(inductor);
		
		p1.getInverters().add(inv);
		p1.getPowerManagers().add(pm);
		p1.getInductors().add(inductor);
		p1.getPowerChannels().add(channel_provider);
		p1.getPowerChannels().add(channel_consumer);
		
		print(p1);
		fullPrint(p1);
		
		return p1;
	}


	public static InductionHob createP2() {
		InductionHob p2 = factory.createInductionHob();
		p2.setID("IH2");
		
		Inverter inv = factory.createInverter();
		inv.setID("1");
		PowerManager pm = factory.createPowerManager();
		pm.setID("1");
		Inductor inductor = factory.createInductor();
		inductor.setID("1");
		PowerChannel channel_provider = factory.createPowerChannel();
		PowerChannel channel_consumer1 = factory.createPowerChannel();
		
		PowerChannel channel_consumer2 = factory.createPowerChannel();
		Inductor inductor2 = factory.createInductor();
		inductor2.setID("2");
		
		channel_provider.setFrom(inv);
		channel_provider.setTo(pm);
		
		channel_consumer1.setFrom(pm);
		channel_consumer1.setTo(inductor);
		
		channel_consumer2.setFrom(pm);
		channel_consumer2.setTo(inductor2);
		
		p2.getInverters().add(inv);
		p2.getPowerManagers().add(pm);
		p2.getInductors().add(inductor);
		p2.getPowerChannels().add(channel_provider);
		p2.getPowerChannels().add(channel_consumer1);
		
		p2.getInductors().add(inductor2);
		p2.getPowerChannels().add(channel_consumer2);
		
		inv = factory.createInverter();
		inv.setID("2");
		pm = factory.createPowerManager();
		pm.setID("2");
		inductor = factory.createInductor();
		inductor.setID("3");
		channel_provider = factory.createPowerChannel();
		channel_consumer1 = factory.createPowerChannel();
		
		channel_consumer2 = factory.createPowerChannel();
		inductor2 = factory.createInductor();
		inductor2.setID("4");
		
		channel_provider.setFrom(inv);
		channel_provider.setTo(pm);
		
		channel_consumer1.setFrom(pm);
		channel_consumer1.setTo(inductor);
		
		channel_consumer2.setFrom(pm);
		channel_consumer2.setTo(inductor2);
		
		p2.getInverters().add(inv);
		p2.getPowerManagers().add(pm);
		p2.getInductors().add(inductor);
		p2.getPowerChannels().add(channel_provider);
		p2.getPowerChannels().add(channel_consumer1);
		
		p2.getInductors().add(inductor2);
		p2.getPowerChannels().add(channel_consumer2);
		
		print(p2);
		fullPrint(p2);
		
		return p2;
	}

	public static InductionHob createP3() {
		
		InductionHob p3 = factory.createInductionHob();
		p3.setID("IH3");
		
		Inverter inv = factory.createInverter();
		inv.setID("1");
		PowerManager pm = factory.createPowerManager();
		pm.setID("1");
		Inductor inductor = factory.createInductor();
		inductor.setID("1");
		PowerChannel channel_provider = factory.createPowerChannel();
		PowerChannel channel_consumer = factory.createPowerChannel();
		
		PowerChannel channel_consumer2 = factory.createPowerChannel();
		Inductor inductor2 = factory.createInductor();
		inductor2.setID("2");
		
		PowerChannel channel_consumer3 = factory.createPowerChannel();
		Inductor inductor3 = factory.createInductor();
		inductor3.setID("3");
		
		channel_provider.setFrom(inv);
		channel_provider.setTo(pm);
		
		channel_consumer.setFrom(pm);
		channel_consumer.setTo(inductor);
		
		channel_consumer2.setFrom(pm);
		channel_consumer2.setTo(inductor2);
		
		channel_consumer3.setFrom(pm);
		channel_consumer3.setTo(inductor3);
		
		p3.getInverters().add(inv);
		p3.getPowerManagers().add(pm);
		p3.getInductors().add(inductor);
		p3.getPowerChannels().add(channel_provider);
		p3.getPowerChannels().add(channel_consumer);
		
		p3.getInductors().add(inductor2);
		p3.getPowerChannels().add(channel_consumer2);
		
		p3.getInductors().add(inductor3);
		p3.getPowerChannels().add(channel_consumer3);
		
		inv = factory.createInverter();
		inv.setID("2");
		pm = factory.createPowerManager();
		pm.setID("2");
		inductor = factory.createInductor();
		inductor.setID("4");
		channel_provider = factory.createPowerChannel();
		channel_consumer = factory.createPowerChannel();
		
		channel_provider.setFrom(inv);
		channel_provider.setTo(pm);
		
		channel_consumer.setFrom(pm);
		channel_consumer.setTo(inductor);
		
		p3.getInverters().add(inv);
		p3.getPowerManagers().add(pm);
		p3.getInductors().add(inductor);
		p3.getPowerChannels().add(channel_provider);
		p3.getPowerChannels().add(channel_consumer);
		
		print(p3);
		fullPrint(p3);
		
		return p3;
	}
	
public static InductionHob createP4() {
		
		InductionHob p4 = factory.createInductionHob();
		p4.setID("IH4");
		
		Inverter inv = factory.createInverter();
		inv.setID("1");
		PowerManager pm = factory.createPowerManager();
		pm.setID("1");
		Inductor inductor = factory.createInductor();
		inductor.setID("1");
		PowerChannel channel_provider = factory.createPowerChannel();
		PowerChannel channel_consumer = factory.createPowerChannel();
		
		Inverter inv2 = factory.createInverter();
		inv2.setID("2");
		PowerChannel channel_provider2 = factory.createPowerChannel();
		PowerChannel channel_consumer2 = factory.createPowerChannel();
		Inductor inductor2 = factory.createInductor();
		inductor2.setID("2");
		
		channel_provider.setFrom(inv);
		channel_provider.setTo(pm);
		
		channel_consumer.setFrom(pm);
		channel_consumer.setTo(inductor);
		
		channel_provider2.setFrom(inv2);
		channel_provider2.setTo(pm);
		
		channel_consumer2.setFrom(pm);
		channel_consumer2.setTo(inductor2);
		
		p4.getInverters().add(inv);
		p4.getPowerManagers().add(pm);
		p4.getInductors().add(inductor);
		p4.getPowerChannels().add(channel_provider);
		p4.getPowerChannels().add(channel_consumer);
		
		p4.getInverters().add(inv2);
		p4.getInductors().add(inductor2);
		p4.getPowerChannels().add(channel_provider2);
		p4.getPowerChannels().add(channel_consumer2);
		
		
		pm = factory.createPowerManager();
		pm.setID("2");
		inductor = factory.createInductor();
		inductor.setID("3");
		channel_provider = factory.createPowerChannel();
		channel_consumer = factory.createPowerChannel();
		
		channel_provider.setFrom(inv2);
		channel_provider.setTo(pm);
		
		channel_consumer.setFrom(pm);
		channel_consumer.setTo(inductor);
				
		inv = factory.createInverter();
		inv.setID("3");
		inductor2 = factory.createInductor();
		inductor2.setID("4");
		channel_provider2 = factory.createPowerChannel();
		channel_consumer2 = factory.createPowerChannel();
		
		channel_provider2.setFrom(inv);
		channel_provider2.setTo(pm);
		
		channel_consumer2.setFrom(pm);
		channel_consumer2.setTo(inductor2);
		
		p4.getPowerManagers().add(pm);
		p4.getInductors().add(inductor);
		p4.getPowerChannels().add(channel_provider);
		p4.getPowerChannels().add(channel_consumer);
		
		p4.getInverters().add(inv);
		p4.getInductors().add(inductor2);
		p4.getPowerChannels().add(channel_provider2);
		p4.getPowerChannels().add(channel_consumer2);
		
		print(p4);
		fullPrint(p4);
		
		return p4;
	}

	
	public static EObject createORGetRandomModel(int elements) {
		
		String name = "RNG/Random_"+elements+Constants.FILES_EXTENSION;
		
		EObject result = ProjectHelper.getInstance().loadModel(name);
		
		if(result != null)
			return result;
		
		result = createModel(name,elements);
		
		ProjectHelper.getInstance().createFile(name, result);
		return ProjectHelper.getInstance().loadModel(name);
	}
	

	private static EObject createModel(String name,int elements) {
		
		InductionHob hob = factory.createInductionHob();
		
		ProjectHelper.getInstance().createFile(name, hob);
		
		EcoreMutator mut = new EcoreMutator();
		mut.setTracker(new CSVMutationTracker());
		ArrayList<EObject> forbiden = new ArrayList<EObject>();
		forbiden.add(factory.createInductor());
		mut.addMutation(new AddObjectMutation());
		
		IModelProvider modelProvider = new ModelProvider();
		
		
		modelProvider.setModelResource(hob.eResource());
		// mutate model
		mut.mutate(modelProvider, elements);
		
		try {
			hob.eResource().save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return hob;
	}
	
public static void print(InductionHob ih) {
		
		System.out.println(ih.getID()+" INV: "+ih.getInverters().size()+" PM: "+ih.getPowerManagers().size()+" IND: "+ih.getInductors().size()+" PC: "+ih.getPowerChannels().size());
}
public static void fullPrint(InductionHob ih) {
		for(Inverter inv : ih.getInverters()) {
			
			System.out.println("INV "+inv.getID());
			
			for(PowerChannel channel : findPowerChannels(inv, ih)) {
				
				TargetElement target_element = channel.getTo();
				if (target_element instanceof PowerManager) {
					
					PowerManager pm = ((PowerManager)target_element);
					System.out.println("\tPM "+ pm.getID());
					
					findPathToInductor(ih, pm);
					
				} else {
					
					System.out.println("\tIND "+ ((Inductor)target_element).getID());
				}
				
			}
			
			System.out.println();
		}
		
		
	}


private static void findPathToInductor(InductionHob ih, PowerManager pm) {
	
	for(PowerChannel channel_consumer : findPowerChannels(pm,ih)) {
		
		TargetElement target_element = channel_consumer.getTo();
		if (target_element instanceof PowerManager) {
			
			PowerManager pm_next = ((PowerManager)target_element);
			System.out.println("\tPM "+ pm_next.getID());
			findPathToInductor(ih, pm_next);
			
		}
		else
		{
			System.out.println("\t\tIND "+ ((Inductor)target_element).getID());
		}
		
	}
}

public static ArrayList<PowerChannel> findPowerChannels(SourceElement source_element,InductionHob ih) {
	
	ArrayList<PowerChannel> channels = new ArrayList<PowerChannel>();
	
	for(PowerChannel channel : ih.getPowerChannels()) {
		
		if(channel.getFrom() == source_element) {
			channels.add(channel);
		}
		
	}
	return channels;
}

}
