package flame.GO.crossover;

import flame.GO.core.BitSet;
import flame.GO.core.Individual;
import flame.GO.utils.Constants;
import flame.GO.utils.GOUtils;

public class SimpleCrossover {


	/**
	 * If no croossover happens, the first individual is returned
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static Individual crossoverRandomMask(Individual f1, Individual f2) {

		if(Math.random() > Constants.CROSSOVER_PROBABILITY)
			return f1;
		
		Individual offspring = new Individual(f1);
			
		BitSet mask = GOUtils.generateRandomMask(f1.getGenes().size());
		BitSet noMask = (BitSet) mask.clone();
		noMask.negate();
		BitSet aux1 = f1.getGenes().clone();
		BitSet aux2 = f2.getGenes().clone();
		aux1.and(mask);
		aux2.and(noMask);
		
		aux1.or(aux2);
		
		offspring.setGenes(aux1);

		return offspring;
	}

}

