package flame.views;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import ihl.InductionHob;
import flame.GO.codec.CODECUtils;
import flame.GO.core.Individual;
import flame.GO.crossover.SimpleCrossover;
import flame.GO.io.ProjectHelper;
import flame.GO.mutation.SimpleMutation;
import flame.GO.utils.Constants;
import flame.GO.utils.IHUtils;

	public class TestHelper {

		public static void encode() {
			
			EObject o1 = IHUtils.createP1();
			Individual i1 = CODECUtils.encode(o1);
			
			TreeIterator<EObject> elements = i1.getCodec().getRoot().eAllContents();
			
			while (elements.hasNext()) {
				EObject object = elements.next();
				int id = i1.getCodec().getMapEObjectToID().get(object);		
			}
			
			for(int i : i1.getCodec().getMapIDTOEOBject().keySet()){
				EObject obj = i1.getCodec().getMapIDTOEOBject().get(i);
				System.out.println(obj);
			}
		}
		
		
		
		public static void mutation() {
			EObject o1 = IHUtils.createP1();
			IHUtils.print((InductionHob)o1);
			
			Individual i1 = CODECUtils.encode(o1);
			System.out.println("Individual before mutation "+i1);
			SimpleMutation.mutate(i1);
			System.out.println("Individual after mutation: "+i1);
			EObject o2 = CODECUtils.decode(i1);
			IHUtils.print((InductionHob)o2);
		}
		
		
		private static void mutateIH(String regularName,String mutatedName) {
			
			EObject o1 = ProjectHelper.getInstance().loadModel(regularName);
			if(o1 == null) {
				o1 = IHUtils.createP1();
				ProjectHelper.getInstance().createFile(regularName, o1);
			}
			Individual i1 = CODECUtils.encode(o1);
			for(int i=0;i<10;i++) {
				SimpleMutation.mutate(i1);
			}
			EObject o2 = CODECUtils.decode(i1);
			ProjectHelper.getInstance().createFile(mutatedName, o2);
			IHUtils.print((InductionHob)o2);
		}
		
		private static void crossoverIH(String parent1,String crossoverName) {
			
			EObject o1 = ProjectHelper.getInstance().loadModel(parent1);
			if(o1 == null) {
				mutateIHs();
				o1 = ProjectHelper.getInstance().loadModel(parent1);
			}
			Individual i1 = CODECUtils.encode(o1);
			Individual i2 = CODECUtils.encode(o1);
			
			for(int i=0;i<10;i++) {
				SimpleMutation.mutate(i1);
				SimpleMutation.mutate(i2);
			}
			
			Individual i3 = SimpleCrossover.crossoverRandomMask(i1, i2);
			EObject o3 = CODECUtils.decode(i3);
			ProjectHelper.getInstance().createFile(crossoverName, o3);
		}
		
		public static void mutateIHs() {
			mutateIH("IH/ih1"+Constants.FILES_EXTENSION,"IH/mutated/ih1"+Constants.FILES_EXTENSION);
			mutateIH("IH/ih2"+Constants.FILES_EXTENSION,"IH/mutated/ih2"+Constants.FILES_EXTENSION);
			mutateIH("IH/ih3"+Constants.FILES_EXTENSION,"IH/mutated/ih3"+Constants.FILES_EXTENSION);
			
		}
		
		public static void crossoverIHs() {
			crossoverIH("IH/ih1"+Constants.FILES_EXTENSION,"IH/crossover/ih1"+Constants.FILES_EXTENSION);
			crossoverIH("IH/ih2"+Constants.FILES_EXTENSION,"IH/crossover/ih2"+Constants.FILES_EXTENSION);
			crossoverIH("IH/ih3"+Constants.FILES_EXTENSION,"IH/crossover/ih3"+Constants.FILES_EXTENSION);
			
		}
		
		public static void createRandomModel() {
			
			IHUtils.createORGetRandomModel(10);
			IHUtils.createORGetRandomModel(100);
			IHUtils.createORGetRandomModel(1000);
		}
		
		public static void createIHs() {
			
			EObject o1 = IHUtils.createP1();
			EObject o2 = IHUtils.createP2();
			EObject o3 = IHUtils.createP3();
			EObject o4 = IHUtils.createP4();
			
			ProjectHelper.getInstance().createFile("IH/ih1"+Constants.FILES_EXTENSION, o1);
			ProjectHelper.getInstance().createFile("IH/ih2"+Constants.FILES_EXTENSION, o2);
			ProjectHelper.getInstance().createFile("IH/ih3"+Constants.FILES_EXTENSION, o3);
			ProjectHelper.getInstance().createFile("IH/ih4"+Constants.FILES_EXTENSION, o4);
			
			System.out.println(o1);
			System.out.println(o2);
			System.out.println(o3);
			System.out.println(o4);
		}
}


