package flame.views;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;


public class TestView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "flamev1.views.FLAMEView";


	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		
		Composite panel = new Composite(parent, SWT.NONE);
		RowLayout layout = new RowLayout();
		panel.setLayout(layout);
		
		Button b = new Button(panel, SWT.NONE);
		b.setText("Create 3 IHs");
		b.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestHelper.createIHs();
			}
		});
		
		Button b2 = new Button(panel, SWT.NONE);
		b2.setText("Mutate 3 IHs");
		b2.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestHelper.mutateIHs();
			}
		});
		
		Button b3 = new Button(panel, SWT.NONE);
		b3.setText("Crossover 3 IHs");
		b3.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestHelper.crossoverIHs();
			}
		});
		
		Button b4 = new Button(panel, SWT.NONE);
		b4.setText("Create Random IHs");
		b4.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestHelper.createRandomModel();
			}
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		
	}
}
