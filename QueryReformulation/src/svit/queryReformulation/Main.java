package svit.queryReformulation;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		final int numTermsToExpand=5;
		
		//query initialization
		ArrayList<String> queryKeywords= new ArrayList<String>();
		queryKeywords.add("panto");
		queryKeywords.add("active");
		queryKeywords.add("breaker");
		
		//initialization of each document with the set of words and frequencies
		HashMap<String,Integer> doc1= new HashMap<String,Integer>();
		doc1.put("panto", 2);
		doc1.put("active", 3);
		doc1.put("cabin", 1);
		
		HashMap<String,Integer> doc2= new HashMap<String,Integer>();
		doc2.put("active", 2);
		doc2.put("panto", 2);	
		doc2.put("button", 1);
		
		HashMap<String,Integer> doc3= new HashMap<String,Integer>();
		doc3.put("doors", 3);
		doc3.put("time", 1);
		
		HashMap<String,Integer> doc4= new HashMap<String,Integer>();
		doc4.put("breaker", 2);
		doc4.put("status", 2);
		doc4.put("speed", 1);
		doc4.put("panto", 1);
		
		//initialization of each document in the corpus
		ArrayList<HashMap> corpusDocs= new ArrayList<HashMap>();
		corpusDocs.add(doc1);
		corpusDocs.add(doc2);
		corpusDocs.add(doc3);
		corpusDocs.add(doc4);
		
		//initialization of relevant and non-relevant docs
		ArrayList<HashMap> relevantDocs= new ArrayList<HashMap>();
		relevantDocs.add(doc1);
		relevantDocs.add(doc2);
		relevantDocs.add(doc3);
		
		ArrayList<HashMap> nonRelevantDocs= new ArrayList<HashMap>();
		nonRelevantDocs.add(doc4);
		
		//running
		System.out.println("Terms in query: ");
		printResults(queryKeywords);
		
		QueryReformulation qr= new QueryReformulation();
		
		System.out.println("\n* Reduced query: ");
		printResults(qr.queryReduction(queryKeywords, corpusDocs));
	
		
		System.out.println("\n* Rocchio query expansion: ");
		printResults(qr.queryExpasionRocchio(queryKeywords,relevantDocs, corpusDocs,numTermsToExpand));
		
		System.out.println("\n* RSV query expansion: ");
		printResults(qr.queryExpasionRSV(queryKeywords,relevantDocs, nonRelevantDocs,corpusDocs,numTermsToExpand));
		
		System.out.println("\n* Dice coeficient query expansion: ");
		printResults(qr.queryExpasionDiceCoeficient(queryKeywords,relevantDocs, corpusDocs,numTermsToExpand));

	}
	
	private static void printResults(ArrayList<String> results)
	{
		int i=1;
		int sizeR=results.size();
		for (String word: results)
		{
			
			System.out.print(word);
			if(i!=sizeR) System.out.print(", ");
			i++;
		}
		System.out.println();
	}

}
