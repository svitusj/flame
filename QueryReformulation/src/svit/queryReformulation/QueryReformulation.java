package svit.queryReformulation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class QueryReformulation {

	
	private ArrayList<String> queryKeywords;
	private ArrayList<HashMap> relevantDocs;
	private ArrayList<HashMap> nonRelevantDocs;
	private ArrayList<HashMap> corpusDocs;
	
	public ArrayList<String> queryReduction(ArrayList<String> queryKeywords,ArrayList<HashMap> corpusDocs)
	{
		this.queryKeywords=new ArrayList<String>(queryKeywords);
		this.corpusDocs=new ArrayList<HashMap>(corpusDocs);
		this.relevantDocs=new ArrayList<HashMap>();
		this.nonRelevantDocs=new ArrayList<HashMap>();
		
		int reductionValue=25;
		ArrayList<String> result=new ArrayList<String>();
		HashMap<String,Integer> wordFreqInCorpusDocuments= new HashMap<String,Integer>();
		for (String word: queryKeywords)
		{
			wordFreqInCorpusDocuments.put(word, 0);
			
		}
		
		int totalCorpusDocs=corpusDocs.size();
		
		for (String word: queryKeywords)
		{
			for (HashMap<String,Integer> doc: corpusDocs)
			{
			if(doc.containsKey(word)) 
				{				
				//wordFreqInCorpusDocuments.put(word,wordFreqInCorpusDocuments.get(word)+1);
				wordFreqInCorpusDocuments.replace(word, wordFreqInCorpusDocuments.get(word)+1);
				}
			}
		}


		//checks if the frequency of each word is less than 25%. If the frequency is greater, the word is removed
		for (HashMap.Entry<String, Integer> entry:wordFreqInCorpusDocuments.entrySet())
		{
			String word = entry.getKey();
		    int frequency = entry.getValue();
		    int appearance=(frequency*100)/totalCorpusDocs;
		    if(appearance<=reductionValue)
		    {
		    	//word stays
		    	result.add(word);
		    }
		    
		}
		return result;
	}
	
	public ArrayList<String> queryExpasionRocchio(ArrayList<String> queryKeywords,ArrayList<HashMap> relevantDocs, ArrayList<HashMap> corpusDocs, int termsToExpand)
	{
		this.queryKeywords=new ArrayList<String>(queryKeywords);
		this.corpusDocs=new ArrayList<HashMap>(corpusDocs);
		this.relevantDocs=new ArrayList<HashMap>(relevantDocs);
		this.nonRelevantDocs=new ArrayList<HashMap>();
		
		ArrayList<String> result=new ArrayList<String>(queryKeywords);
		
		ArrayList<HashMap> resultTfxIdfValuesInDocs=getTfxIdfValues(relevantDocs, corpusDocs);
		
		HashMap <String, Double> resultWeights=new HashMap<String, Double>();
		
		for (HashMap<String,Double> doc: resultTfxIdfValuesInDocs)
		{
						
			for (HashMap.Entry<String, Double> entry:doc.entrySet())
			{
				String word = entry.getKey();
			    double tfxIdfValue = entry.getValue();
			    
			    if(resultWeights.containsKey(word)) resultWeights.replace(word, resultWeights.get(word)+tfxIdfValue);
			    else resultWeights.put(word, (double) tfxIdfValue);
			}
		}
		
	
		Map<String, Double> sortedTermWeights=sortByComparator(resultWeights, false);
		
		result = expandQuery(termsToExpand, result, sortedTermWeights);
		
		return result;
	}
	
	public ArrayList<String> queryExpasionRSV(ArrayList<String> queryKeywords,ArrayList<HashMap> relevantDocs, ArrayList<HashMap> nonRelevantDocs, ArrayList<HashMap> corpusDocs, int termsToExpand)
	{
		this.queryKeywords=new ArrayList<String>(queryKeywords);
		this.corpusDocs=new ArrayList<HashMap>(corpusDocs);
		this.relevantDocs=new ArrayList<HashMap>(relevantDocs);
		this.nonRelevantDocs=new ArrayList<HashMap>(nonRelevantDocs);
		
		ArrayList<String> result=new ArrayList<String>(queryKeywords);
		
		ArrayList<HashMap> resultTfxIdfValuesInDocs=getTfxIdfValues(relevantDocs, corpusDocs);
		
		HashMap <String, Double> resultWeights=new HashMap<String, Double>();
		
		
		for (HashMap<String,Double> doc: resultTfxIdfValuesInDocs)
		{
						
			for (HashMap.Entry<String, Double> entry:doc.entrySet())
			{
				String word = entry.getKey();
			    double tfxIdfValue = entry.getValue();
			    int freqInRelevant=getNumOccurrencesInDocuments(relevantDocs, word);
			    int freqInNonRelevant=getNumOccurrencesInDocuments(nonRelevantDocs, word);
			    double dif=freqInRelevant-freqInNonRelevant;
			    double rsvValue=tfxIdfValue*dif;
			    
			    if(resultWeights.containsKey(word)) resultWeights.replace(word, resultWeights.get(word)+rsvValue);
			    else resultWeights.put(word, (double) rsvValue);
			}
		}
		
	
		Map<String, Double> sortedTermWeights=sortByComparator(resultWeights, false);

		result = expandQuery(termsToExpand, result, sortedTermWeights);
		
		return result;
	}
	
	public ArrayList<String> queryExpasionDiceCoeficient(ArrayList<String> queryKeywords,ArrayList<HashMap> relevantDocs, ArrayList<HashMap> corpusDocs, int termsToExpand)
	{
		this.queryKeywords=new ArrayList<String>(queryKeywords);
		this.corpusDocs=new ArrayList<HashMap>(corpusDocs);
		this.relevantDocs=new ArrayList<HashMap>(relevantDocs);
		this.nonRelevantDocs=new ArrayList<HashMap>();
		
		ArrayList<String> result=new ArrayList<String>(queryKeywords);
		
		
		HashMap <String, Double> resultWeights=new HashMap<String, Double>();
		
		
		for (String wordInQuery: queryKeywords)
		{
			for (HashMap<String,Double> doc: relevantDocs)
			{
			
				for (HashMap.Entry<String, Double> entry:doc.entrySet())
				{
					String wordInDoc = entry.getKey();
					double dfuv=getNumDocsHasTwoWords(corpusDocs, wordInQuery, wordInDoc);
					double dfu=getNumDocsHasAWord(corpusDocs, wordInQuery);
					double dfv=getNumDocsHasAWord(corpusDocs, wordInDoc);
					double weight=(2*dfuv)/(dfu+dfv);
					if(!wordInQuery.equals(wordInDoc))
					{if(resultWeights.containsKey(wordInDoc)) 
						{				
						if(resultWeights.get(wordInDoc)<weight) resultWeights.replace(wordInDoc, weight);
						}
					else resultWeights.put(wordInDoc, weight);
					}
				}
			}
		}
		
		
		
	
		Map<String, Double> sortedTermWeights=sortByComparator(resultWeights, false);
		
		
		result = expandQuery(termsToExpand, result, sortedTermWeights);
		
		return result;
	}

	private ArrayList<String> expandQuery(int termsToExpand, ArrayList<String> result,
			Map<String, Double> sortedTermWeights) {
		for (Entry<String, Double> entry : sortedTermWeights.entrySet())
		{
			termsToExpand--;
			String word=entry.getKey();
			if(termsToExpand<0) break;
			else if(!result.contains(word) && entry.getValue()>0) result.add(word);
			//value>0 to ensure that an expanded term has some weight
			
		}
		return result;
	}
	
	private int getNumOccurrencesInDocuments(ArrayList<HashMap> documents, String word)
	{
		int result=0;
		//number of times that a term appears in a document
		for (HashMap<String,Integer> doc: documents)
		{
			if(doc.containsKey(word)) result=result+doc.get(word);
		}
	
		return result;
	}
	
	private int getNumDocsHasAWord(ArrayList<HashMap> documents, String word)
	{
		int result=0;
		//number of times that a term appears in a document
		for (HashMap<String,Integer> doc: documents)
		{
			if(doc.containsKey(word)) result++;
		}
	
		return result;
	}
	
	private int getNumDocsHasTwoWords(ArrayList<HashMap> documents, String word1, String word2)
	{
		int result=0;
		//number of times that a term appears in a document
		for (HashMap<String,Integer> doc: documents)
		{
			if(doc.containsKey(word1) && doc.containsKey(word2)) result++;
		}
	
		return result;
	}
	
private ArrayList<HashMap> getTfxIdfValues(ArrayList<HashMap> relevantDocs, ArrayList<HashMap> corpusDocs)
{
	ArrayList<HashMap> resultTfxIdfValuesInDocs=new ArrayList<HashMap>();
	for (HashMap<String,Integer> doc: relevantDocs)
	{
		HashMap<String,Double> resultDoc= new HashMap<String,Double> ();
		
		for (HashMap.Entry<String, Integer> entry:doc.entrySet())
		{
			String word = entry.getKey();
		    int frequencyInDoc = entry.getValue();
		    int numCorpusDocsHasTheWord=getNumDocsHasAWord(corpusDocs, word);
		    double idf=1.0/numCorpusDocsHasTheWord;
		    double value=frequencyInDoc*idf;
		    
		    resultDoc.put(word, value);
		}
		resultTfxIdfValuesInDocs.add(resultDoc);
	
	}
	return resultTfxIdfValuesInDocs;
}
private static Map<String, Double> sortByComparator(Map<String, Double> unsortMap, final boolean order)
{
	//order attribute
	//	public static boolean ASC = true;
	//  public static boolean DESC = false;

    List<Entry<String, Double>> list = new LinkedList<Entry<String, Double>>(unsortMap.entrySet());

    // Sorting the list based on values
    Collections.sort(list, new Comparator<Entry<String, Double>>()
    {
        public int compare(Entry<String, Double> o1,
                Entry<String, Double> o2)
        {
            if (order)
            {
                return o1.getValue().compareTo(o2.getValue());
            }
            else
            {
                return o2.getValue().compareTo(o1.getValue());

            }
        }
    });

    // Maintaining insertion order with the help of LinkedList
    Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
    for (Entry<String, Double> entry : list)
    {
        sortedMap.put(entry.getKey(), entry.getValue());
    }

    return sortedMap;
}


}
