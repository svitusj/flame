package utils;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import classes.Token;
import main.Main;
import opennlp.tools.lemmatizer.SimpleLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.WhitespaceTokenizer;

/**
 * This class contains the functionality needed to extract keywords from a set of already processed documents and query.
 * @author Ra�l
 */
public class PreprocessingUtil 
{
	
	/* Variables used internally to represent the domain terms and stopwords */
	private final URL domainTermsURL = Main.class.getResource("/resources/domainTerms.csv");
	private final URL stopwordsURL = Main.class.getResource("/resources/stopwords.csv");
	private List<String> domainTerms;
	private List<String> stopwords;
	
	/* Variables for the linguistic models */
	private final String posModelRoute = "/resources/en-pos-maxent.bin";
	private final String dictRoute = "/resources/en-lemmatizer.dict";
	
	/**
	 * Class constructor
	 */
	public PreprocessingUtil()
	{
		domainTerms = DocumentsUtil.retrieveTermsFromCSV(domainTermsURL);
		stopwords = DocumentsUtil.retrieveTermsFromCSV(stopwordsURL);
	}
	
	/* Automatic Linguistic Preprocessing methods */
	
	/**
	 * This method tokenizes the introduced string and transforms the tokens into lowercase.
	 * @param s String that must be processed
	 * @return A list of tokens
	 */
	private List<Token> basicProcessing(String introducedString)
	{
		// Tokenize the string
        String[] wordsArray = WhitespaceTokenizer.INSTANCE.tokenize(introducedString);
        
        // Create the tokens list
        List<Token> tokens = new ArrayList<Token>();
        
        // Add them as tokens to the tokens list
        for(String word : wordsArray)
        {
        	// Lowercase the word first
        	word = word.toLowerCase();
        	tokens.add(new Token(word));
        }
        
        return tokens;
	}
	
	/**
	 * Method used to extract the POS Tags a list of tokens extracted with the basic processing method.
	 * The POS tags are stored into the posTag variable of each token.
	 * @param tokens The list of tokens from which POS Tags must be extracted
	 */
	private void posTag(List<Token> tokens)
    {
        try 
        {
        	/* START POS TAGGING */
            
        	// Create the POS Model from resource stream
            InputStream is = Main.class.getResourceAsStream(posModelRoute);
            final POSModel model = new POSModel(is);
          
            // Create the POS tagger from the model
            POSTaggerME tagger = new POSTaggerME(model);
            
            // Close the resource stream
            is.close();
            
            // Get the tokens as array of words
            String[] words = new String[tokens.size()];
            int index = 0;
            for(Token token : tokens)
            {
            	words[index] = token.word;
            	index++;
            }
            
            // Get the array of tags
            String[] tags = tagger.tag(words);
            
            // Store the tags in the proper token variable
            index = 0;
            for(Token token : tokens)
            {
            	token.posTag = tags[index];
            	index++;
            }
            
            /* END POS TAGGING */
            
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }	

	/**
	 * Method used to lemmatize the tokens extracted with the basic processing method, using their POS Tags.
	 * @param tokens The list of tokens from which lemmas must be extracted. NOTE: Tokens must be POS Tagged first
	 */
	private void lemmatize(List<Token> tokens) 
	{
		try
		{
			// Get the dictionary as stream
			InputStream is = Main.class.getResourceAsStream(dictRoute);
			// Create the lemmatizer
			SimpleLemmatizer lemmatizer = new SimpleLemmatizer(is);
			// Close the stream
			is.close();
			
			// For all the tokens
			for(Token token : tokens)
			{
				// Lemmatize word according to its postag
				token.lemma = lemmatizer.lemmatize(token.word, token.posTag);
			}		
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/* Human-In-The-Loop Methods */
	
	/** Method that detects domain terms in an introduced string and puts them into the final list of words
	* @param s String that must be processed
	* @param processedString The final list of words into which domain terms must be introduced
	*/
	private void extractDomainTerms(String introducedString, List<String> processedString)
	{
		// For every term in the list of domain terms
		for(String term : domainTerms)
			// If the string contains the term
			if (introducedString.contains(term))
				// The term is added to the list
				processedString.add(term);
	}
	
	/**
	 * Method that removes stopwords from the final list of words.
	 * @param processedString The final list of words, from which stopwords must be removed
	 */
	private void removeStopwords(List<String> processedString)
	{
		// Remove every stopword in the final list of words
		processedString.removeAll(stopwords);
	}
	
	/* User-friendly preprocessing util methods */
	
	/**
	 * Method that performs the baseline processing to an introduced string.
	 * @param introducedString The string that must be processed.
	 * @param humanIntervention Boolean to decide whether to include or exclude humans in the loop
	 * @return The processed string as a list of words.
	 */
	public List<String> baselineProcessing(String introducedString, boolean humanIntervention)
	{
		List<String> processedString = new ArrayList<String>();
		List<Token> tokens = new ArrayList<Token>();
		
		// If there is a human in the loop we extract the domain terms
		if(humanIntervention) 
			extractDomainTerms(introducedString, processedString);
		
		// Perform the basic processing
		tokens = basicProcessing(introducedString);
		
		// Form the processed string
		for (Token token : tokens)
			processedString.add(token.word);
		
		// If there is a human in the loop we filter stopwords
		if(humanIntervention)
			removeStopwords(processedString);
		
		// Sort the words alphabetically
		Collections.sort(processedString);
		
		// Return the final list of strings
		return processedString;
	}
	
	/**
	 * Method that performs the Tier 1 processing to an introduced string
	 * @param introducedString The string that must be processed
	 * @param humanIntervention Boolean to decide whether to include or exclude humans in the loop
	 * @return The processed string as a list of words
	 */
	public List<String> tierOneProcessing(String introducedString, boolean humanIntervention)
	{
		List<String> processedString = new ArrayList<String>();
		List<Token> tokens = new ArrayList<Token>();
		
		// If there is a human in the loop we extract the domain terms
		if(humanIntervention) 
			extractDomainTerms(introducedString, processedString);
		
		// Perform the basic processing
		tokens = basicProcessing(introducedString);
		
		// POS Tag the tokens
		posTag(tokens);
		
		// Create the processed string by filtering out the words through the POS Tags
		tierOneFiltering(tokens, processedString);
				
		// If there is a human in the loop we filter stopwords
		if(humanIntervention)
			removeStopwords(processedString);
		
		// Sort the words alphabetically
		Collections.sort(processedString);
		
		// Return the final list of strings
		return processedString;
	}
	
	/**
	 * Method that performs the Tier 2 processing to an introduced string
	 * @param introducedString The string that must be processed
	 * @param humanIntervention Boolean to decide whether to include or exclude humans in the loop
	 * @return The processed string as a list of words
	 */
	public List<String> tierTwoProcessing(String introducedString, boolean humanIntervention)
	{
		List<String> processedString = new ArrayList<String>();
		List<Token> tokens = new ArrayList<Token>();
		
		// If there is a human in the loop we extract the domain terms
		if(humanIntervention) 
			extractDomainTerms(introducedString, processedString);
		
		// Perform the basic processing
		tokens = basicProcessing(introducedString);
		
		// POS Tag the tokens
		posTag(tokens);
		
		// Lemmatize the tokens
		lemmatize(tokens);
		
		// Create the processed string by filtering out lemmas by their POS Tag
		tierTwoFiltering(tokens, processedString);
				
		// If there is a human in the loop we filter stopwords
		if(humanIntervention)
			removeStopwords(processedString);
		
		// Sort the words alphabetically
		Collections.sort(processedString);
		
		// Return the final list of strings
		return processedString;
	}
	
	/* Auxiliary methods */
	
	/**
	 * Method used to filter out words through their POS Tags
	 * Currently we maintain only nouns
	 * @param tokens The list of tokens that must be filtered
	 * @param processedString The list of string into which words must be introduced
	 */
	private void tierOneFiltering(List<Token> tokens, List<String> processedString)
	{
		// Form the processed string by adding only the nouns
		for (Token token : tokens)
			if (token.posTag.equalsIgnoreCase("NN"))
				processedString.add(token.word);
	}
	
	/**
	 * Method used to filter out lemmas through the POS Tags of their words
	 * Currently we maintain only nouns
	 * 	 * @param tokens The list of tokens that must be filtered
	 * @param processedString The list of string into which lemmas must be introduced
	 */
	private void tierTwoFiltering(List<Token> tokens, List<String> processedString)
	{
		// Form the processed string by adding only the nouns
		for (Token token : tokens)
			if (token.posTag.equalsIgnoreCase("NN"))
				processedString.add(token.lemma);
	}
}
