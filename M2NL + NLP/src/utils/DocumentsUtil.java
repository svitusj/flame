package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import classes.ModelFragment;
import classes.Requirement;

/** 
 * @author Ra�l
 * Auxiliary class that contains a collection of methods related to documents functionality
 */
public class DocumentsUtil 
{
	
	/**
	 * Reads a CSV, retrieving the elements inside as an ArrayList of String
	 * @param csvURL The URL of the CSV from which to extract the elements 
	 * @return An ArrayList of String containing the elements in the CSV
	 */
	public static List<String> retrieveTermsFromCSV(URL csvURL)
	{
		try 
		{
			// Generate the file, reader, line, and split character
			File csvFile = new File(csvURL.toURI());
			BufferedReader br = new BufferedReader(new FileReader(csvFile));
			String line = "";
			String cvsSplitCharacter = ",";
			List<String> words = new ArrayList<String>();

			// Iterate, just in case there are multiple lines in the file
			while ((line = br.readLine()) != null) 
			{
				// Use comma as separator
				words = Arrays.asList(line.split(cvsSplitCharacter));
			}

			// Close the reader and return the words list
			br.close();
			return words;

		} 
		catch (Exception e)
		{
			// If there is some error, print the trace and return null
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Method that extracts requirements from a requirements specification XLSX file
	 * @param xlsxURL URL of the file from which requirements must be extracted
	 * @return A List of Requirements
	 */
	public static List<Requirement> retrieveRequirementsFromExcelFile(URL xlsxURL)
	{
		try 
		{
			// Generate the file, reader
			File xlsxFile = new File(xlsxURL.toURI());
			InputStream input = new FileInputStream(xlsxFile);

			// We have to start from row number 0
			int rowControl = 0;

			// Create workbook, sheet, row 
			Workbook wb = WorkbookFactory.create(input);
			Sheet sheet = wb.getSheetAt(0);
			Row row;
			
			// Create List to return
			List<Requirement> list = new ArrayList<Requirement>();

			// Fill the List
			boolean isNull = false;
			do 
			{
				try 
				{
					// Get the row
					row = sheet.getRow(rowControl);
					
					// Get the requirement from the first cell 
					String id = row.getCell(0).toString();
					String body = row.getCell(1).toString();
					String fragmentId = row.getCell(2).toString();
					
					// Add the requirement to the list
					list.add(new Requirement(id, body, fragmentId));

					// Increment the list counter
					rowControl++;
					
				} 
				catch (Exception e) 
				{
					isNull = true;
				}			
			} while (isNull != true);

			// Close the input and return the list
			input.close();
			return list;

		} 
		catch (Exception e)
		{
			// If there is some error, print the trace and return null
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method that extracts model fragments from a model fragments XLSX file
	 * @param xlsxURL URL of the file from which model fragments must be extracted
	 * @return A List of Model Fragments
	 */
	public static List<ModelFragment> retrieveModelFragmentsFromExcelFile(URL xlsxURL)
	{
		try 
		{
			// Generate the file, reader
			File xlsxFile = new File(xlsxURL.toURI());
			InputStream input = new FileInputStream(xlsxFile);

			// We have to start from row number 0
			int rowControl = 0;

			// Create workbook, sheet, row 
			Workbook wb = WorkbookFactory.create(input);
			Sheet sheet = wb.getSheetAt(0);
			Row row;
			
			// Create List to return
			List<ModelFragment> list = new ArrayList<ModelFragment>();

			// Fill the List
			boolean isNull = false;
			do 
			{
				try 
				{
					// Get the row
					row = sheet.getRow(rowControl);
					
					// Get the model fragment from the first cell 
					String id = row.getCell(0).toString();
					String elementBasedBody = row.getCell(1).toString();
					String ruleBasedBody = row.getCell(2).toString();
					
					// Add the requirement to the list
					list.add(new ModelFragment(id, elementBasedBody, ruleBasedBody));

					// Increment the list counter
					rowControl++;
					
				} 
				catch (Exception e) 
				{
					isNull = true;
				}			
			} while (isNull != true);

			// Close the input and return the list
			input.close();
			return list;

		} 
		catch (Exception e)
		{
			// If there is some error, print the trace and return null
			e.printStackTrace();
			return null;
		}
	}
}