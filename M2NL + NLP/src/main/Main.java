package main;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.management.Query;

import classes.ModelFragment;
import classes.Requirement;
import utils.DocumentsUtil;
import utils.LSIUtil;
import utils.PreprocessingUtil;

/**
 * Main class. Runs the SimpleLSI for the attached examples.
 * @author Ra�l
 */
public class Main
{
	/* Requirement resources URLs */
	private static final URL reqAuckland = Main.class.getResource("/resources/reqAuckland.xlsx");
	private static final URL reqBucarest = Main.class.getResource("/resources/reqBucarest.xlsx");
	private static final URL reqCincinnati = Main.class.getResource("/resources/reqCincinnati.xlsx");
	private static final URL reqHouston = Main.class.getResource("/resources/reqHouston.xlsx");
	private static final URL reqKaohsiung = Main.class.getResource("/resources/reqKaohsiung.xlsx");
	
	/* Models resources URLs */
	private static final URL modAuckland = Main.class.getResource("/resources/modAuckland.xlsx");
	private static final URL modBucarest = Main.class.getResource("/resources/modBucarest.xlsx");
	private static final URL modCincinnati = Main.class.getResource("/resources/modCincinnati.xlsx");
	private static final URL modHouston = Main.class.getResource("/resources/modHouston.xlsx");
	private static final URL modKaohsiung = Main.class.getResource("/resources/modKaohsiung.xlsx");
	
	public static void main(String[] args) 
	{
		// Use the utils
		PreprocessingUtil pUtil = new PreprocessingUtil();
		
		// Retrieve the list of requirements
		List<Requirement> requirements = DocumentsUtil.retrieveRequirementsFromExcelFile(reqHouston);
		
		// Retrieve the fragments
		List<ModelFragment> auckFragments = DocumentsUtil.retrieveModelFragmentsFromExcelFile(modAuckland);
		List<ModelFragment> bucFragments = DocumentsUtil.retrieveModelFragmentsFromExcelFile(modBucarest);
		List<ModelFragment> cinFragments = DocumentsUtil.retrieveModelFragmentsFromExcelFile(modCincinnati);
		List<ModelFragment> houFragments = DocumentsUtil.retrieveModelFragmentsFromExcelFile(modHouston);
		List<ModelFragment> kaoFragments = DocumentsUtil.retrieveModelFragmentsFromExcelFile(modKaohsiung);
		
		// Insert the fragments into unified fragment list
		List<ModelFragment> fragments = new ArrayList<ModelFragment>();
		fragments.addAll(auckFragments);
		fragments.addAll(bucFragments);
		fragments.addAll(cinFragments);
		fragments.addAll(houFragments);
		fragments.addAll(kaoFragments);
		
		// Preprocessing of requirements
		for(Requirement requirement : requirements)
			requirement.processedBody = pUtil.baselineProcessing(requirement.body, true);
		
//		// Baseline preprocessing of fragments
//		for (ModelFragment fragment : fragments)
//		{
//			fragment.processedElementBasedBody = pUtil.baselineProcessing(fragment.elementBasedBody, false);
//			fragment.processedRuleBasedBody = pUtil.baselineProcessing(fragment.ruleBasedBody, false);
//		}
		
		// Complete preprocessing of fragments
		for (ModelFragment fragment : fragments)
		{
			fragment.processedElementBasedBody = pUtil.tierTwoProcessing(fragment.elementBasedBody, true);
			fragment.processedRuleBasedBody = pUtil.tierTwoProcessing(fragment.ruleBasedBody, true);
		}
		
		// For each requirement, use it as query to perform the LSI 
		for(Requirement query : requirements)
		{
			LSIUtil lsiUtil = new LSIUtil();
			lsiUtil.PerformLSI(fragments, query, 20);	
		}
	
	}

}
