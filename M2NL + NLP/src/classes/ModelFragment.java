package classes;

import java.util.List;

public class ModelFragment implements Comparable<ModelFragment>
{
	public String id;
	public String elementBasedBody;
	public String ruleBasedBody;
	public List<String> processedElementBasedBody;
	public List<String> processedRuleBasedBody;
	public double score;
	
	public ModelFragment(String id, String elementBasedBody, String ruleBasedBody)
	{
		this.id = id;
		this.elementBasedBody = elementBasedBody;
		this.ruleBasedBody = ruleBasedBody;
	}

	@Override
	public String toString() 
	{
		return "ModelFragment id=" + id + ", score=" + score;
	}

	@Override
	public int compareTo(ModelFragment fragment)
	{
		if (fragment.score > this.score) return 1;
		else if (fragment.score < this.score) return -1;
		else return 0;
	}
	
	
	
}