package classes;

public class Token 
{
	public String word;
	public String posTag;
	public String lemma;
	
	public Token(String word)
	{
		this.word = word;
	}

}
