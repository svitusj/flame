package classes;

import java.util.List;

public class Requirement 
{
	public String id;
	public String body;
	public List<String> processedBody;
	public String fragmentId;
	
	public Requirement(String id, String body, String fragmentId)
	{
		this.id = id;
		this.body = body;
		this.fragmentId = fragmentId;
	}

}
