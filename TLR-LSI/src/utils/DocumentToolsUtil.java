package utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

import classes.CustomModelElement;

import TrainControlDSL.Cabin;
import TrainControlDSL.Car;
import TrainControlDSL.Equipment;
import TrainControlDSL.Order;
import TrainControlDSL.OrderInhibitionRule;
import TrainControlDSL.OrderInvocationRule;
import TrainControlDSL.Property;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainModule;
import TrainControlDSL.TrainUnit;
import TrainControlDSL.impl.TrainControlDSLFactoryImpl;

public class DocumentToolsUtil 
{
	public DocumentToolsUtil(){}
	
	public static TrainUnit loadModel(String route)
	{
		XMIResourceImpl resource = new XMIResourceImpl();
		File source = new File(route);
		
		try
		{
			resource.load(new FileInputStream(source), new HashMap<Object, Object>());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return (TrainUnit) resource.getContents().get(0);
	}
	
	public static TrainUnit buildModel(String modelName, List<CustomModelElement> elements)
	{
		TrainUnit train = TrainControlDSLFactoryImpl.eINSTANCE.createTrainUnit();
		train.setName(modelName);
		
		for(CustomModelElement element : elements)
		{
			if(element.similitude >= 0.7)
			{
				if (element.elementType.equals("equipment"))
				{
					Equipment e = TrainControlDSLFactoryImpl.eINSTANCE.createEquipment();
					e.getHasProperties().addAll(element.equipment.getHasProperties());
					e.getHasOrders().addAll(element.equipment.getHasOrders());
					e.setName(element.equipment.getName());
			        train.getHasEquipments().add(e);
				}
				else if (element.elementType.equals("rule"))
				{
					if(element.rule instanceof OrderInhibitionRule)
					{
						Rule r = TrainControlDSLFactoryImpl.eINSTANCE.createOrderInhibitionRule();
						r.setID(element.rule.getID());
						train.getHasRules().add(r);
					}
					else if(element.rule instanceof OrderInvocationRule)
					{
						Rule r = TrainControlDSLFactoryImpl.eINSTANCE.createOrderInvocationRule();
						r.setID(element.rule.getID());
						train.getHasRules().add(r);
					}
				}
				else if (element.elementType.equals("trainModule"))
				{
					if(element.trainModule instanceof Car)
					{
						TrainModule tm = TrainControlDSLFactoryImpl.eINSTANCE.createCar();
						tm.setID(element.trainModule.getID());
						train.getHasTrainModules().add(tm);
					}
					else if(element.trainModule instanceof Cabin)
					{
						TrainModule tm = TrainControlDSLFactoryImpl.eINSTANCE.createCabin();
						tm.setID(element.trainModule.getID());
						train.getHasTrainModules().add(tm);
					}
				}
			}
		}	
		
		return train;
	}
	
	public static void saveModel(String route, TrainUnit train)
	{
		XMIResourceImpl resource = new XMIResourceImpl(URI.createFileURI(route));
        resource.getContents().add(train);
        try {
            resource.save(Collections.EMPTY_MAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public static String readQuery(String route)
	{
		try 
		{
			Scanner in = new Scanner(new File(route));
			String query = in.nextLine();
			in.close();
			query = processString(query);
			return query;			
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	private static String processString(String string)
	{
		if (string != null)
		{
			String s = string.replaceAll("\\d","");
			s = s.replace("(", "");
			s = s.replace(")", "");
			s = s.replace(",", "");
			s = s.replace(".", "");
			s = s.replace("_", " ");
			return s;
		}
		else return "";
	}
	
	public static String equipmentToString(Equipment equipment)
	{
		// Start with the name
		String s = equipment.getName();
		
		// Get properties and orders
		EList<Property> properties = equipment.getHasProperties();
		EList<Order> orders = equipment.getHasOrders();
		
		// Get names of the properties
		for(Property p : properties)
		{
			s = s.concat(" " + p.getName());
		}
		
		// Get names of the orders
		for(Order o : orders)
		{
			s = s.concat(" " + o.getName());
		}
		
		// Process the whole thing & return the processed string
		s = processString(s);
		return s;
	}

	public static String ruleToString(Rule rule)
	{
		// Start with the name
		String s = rule.getID();
		// Process the whole thing & return the processed string
		s = processString(s);
		return s;
	}
	
	public static String trainModuleToString(TrainModule trainModule)
	{
		// Start with the name
		String s = trainModule.getID();
		// Process the whole thing & return the processed string
		s = processString(s);
		return s;
	}

}
