package main;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import classes.CustomModelElement;

import utils.DocumentToolsUtil;
import utils.LSIUtil;
import TrainControlDSL.Equipment;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainControlDSLPackage;
import TrainControlDSL.TrainModule;
import TrainControlDSL.TrainUnit;

public class Main 
{
	
	/* Variables for routing the existing models and the generated fragments */
	private static final String modelRoot = "C:/Users/Ra�l/Desktop/Workspace/LSI DKE/src/resources/";
	private static final String fragmentRoot = "C:/Users/Ra�l/Desktop/Workspace/LSI DKE/src/results/";
	
	private static final String modelFileName = "/model.traincontroldsl";
	private static final String fragmentFileName = ".traincontroldsl";
	private static final String requirementFileName = "/requirement.txt";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		@SuppressWarnings("unused")
		TrainControlDSLPackage packageInstance = TrainControlDSLPackage.eINSTANCE;
		
		for(int i = 0; i < 140; i++)
		{
			// Get the model
			TrainUnit train = DocumentToolsUtil.loadModel(modelRoot + (i+1) + modelFileName);
			
			// Get the query
			String query = DocumentToolsUtil.readQuery(modelRoot + (i+1) + requirementFileName);
			
			// Extract the equipments, rules, etc. from the model
			EList<Equipment> equipments = train.getHasEquipments();
			EList<Rule> rules = train.getHasRules();
			EList<TrainModule> trainModules = train.getHasTrainModules();
					
			// Create custom element list with the equipments & fill it with equipments, rules, etc.
			List<CustomModelElement> elements = new ArrayList<CustomModelElement>();
			for(Equipment equipment : equipments) elements.add(new CustomModelElement(equipment));
			for(Rule rule : rules) elements.add(new CustomModelElement(rule));
			for(TrainModule trainModule : trainModules) elements.add(new CustomModelElement(trainModule));
			
			// Perform the LSI to extract the similitude values
			LSIUtil lsi = new LSIUtil();
			lsi.PerformLSI(elements, query, 3);
			
			// Build the fragment (taking in account the similitude values) & save it
			TrainUnit fragment = DocumentToolsUtil.buildModel(train.getName() + "fragment", elements);
			DocumentToolsUtil.saveModel(fragmentRoot + (i+1) + fragmentFileName, fragment);
		}		
	}
}
