package classes;

import utils.DocumentToolsUtil;
import TrainControlDSL.Equipment;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainModule;

public class CustomModelElement implements Comparable<CustomModelElement>
{
	public Equipment equipment;
	public Rule rule;
	public TrainModule trainModule;
	
	public String elementType;
	public String elementString;
	public double similitude;
	
	public CustomModelElement(Equipment equipment)
	{
		this.equipment = equipment;
		this.elementString = DocumentToolsUtil.equipmentToString(equipment);
		this.elementType = "equipment";
	}
	
	public CustomModelElement(Rule rule)
	{
		this.rule = rule;
		this.elementString = DocumentToolsUtil.ruleToString(rule);
		this.elementType = "rule";
	}
	
	public CustomModelElement(TrainModule trainModule)
	{
		this.trainModule = trainModule;
		this.elementString = DocumentToolsUtil.trainModuleToString(trainModule);
		this.elementType = "trainModule";
	}
		
	@Override
	public int compareTo(CustomModelElement element)
	{
		if (element.similitude > this.similitude) return 1;
		else if (element.similitude < this.similitude) return -1;
		else return 0;
	}
}
