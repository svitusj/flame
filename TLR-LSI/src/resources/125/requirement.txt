COSMOS will inhibit the order to close the circuit breaker if there is inhibition from ACR, the outdoor outlet is present or there is not permission from CCU.
