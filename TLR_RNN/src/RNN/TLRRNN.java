package RNN;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.datavec.api.conf.Configuration;
import org.datavec.api.records.reader.impl.misc.SVMLightRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.eval.EvaluationAveraging;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.layers.LSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.eclipse.core.resources.IFile;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.Constants;
import TLR.GO.utils.FeatureVectorGenerator;
import data.KnowledgeBase;
import data.TestCase;

public class TLRRNN {

	private ProjectHelper project = null;
	
	
	public TLRRNN(ProjectHelper project) {
		super();
		this.project = project;
	}

	public File prepareTraining(String filepath_kb, String filespath_fragments_kb, String filespath_descriptions_kb) 
	{
				
		//Load the Knowledge Base
		KnowledgeBase knowledge_base = project.loadKnowledgeBase(filepath_kb, filespath_fragments_kb, filespath_descriptions_kb);
		
		//Encode Feature Vectors
		String feature_vectors = knowledge_base.encodeToFeatureVectors();
		
		// Save Feature Vectors
		String filename_training = Constants.FILEPATH_TRAINING;
		IFile ifile = project.createFile(feature_vectors, filename_training);
		
		return ifile.getRawLocation().makeAbsolute().toFile();		
	}
	
	public HashMap<String, File> prepareTesting(String filespath)
	{
		HashMap<String, File> testing_files = new HashMap<String, File>();
		ProjectHelper project = ProjectHelper.getInstance();
		FeatureVectorGenerator feature_vector_generator = new FeatureVectorGenerator();

		//Load Test Cases from Dataset
		HashMap<String, TestCase> test_cases = project.loadTestCases(filespath);
		
		//Launch Test Cases
		for (Entry<String, TestCase> entry : test_cases.entrySet())
		{
			TestCase test_case =((TestCase)entry.getValue());

			String feature_vectors = feature_vector_generator.getFeatureVector(test_case.getId(), test_case.getDescription(), test_case.getModel());
			
			//Save Feature Vectors
			String filename_testing = Constants.FILEPATH_TESTING+test_case.getId();
			IFile ifile = project.createFile(feature_vectors, filename_testing);
			testing_files.put(test_case.getId(), ifile.getRawLocation().makeAbsolute().toFile());
		}
		return testing_files;
	}
	
	public void launchRNN(File file_training, HashMap<String, File> files_testing, String filepathReports) {
		
		
		int numOfFeatures = 94;     // For MNIST data set, each row is a 1D expansion of a handwritten digits picture of size 28x28 pixels = 784
        int numOfClasses = 2;       // 10 classes (types of senders) in the data set. Zero indexing. Classes have integer values 0, 1 or 2 ... 9
        //int printIterationsNum = 20; // print score every 20 iterations

        
		 int batchSize = 64;     //Number of examples in each minibatch
	     int nEpochs = 4;        //Number of epochs (full passes of training data) to train on
	     int truncateReviewsToLength = 256;  //Truncate reviews with length (# words) greater than this
	     final int seed = 0;     //Seed for reproducibility

	       Nd4j.getMemoryManager().setAutoGcWindow(10000);  //https://deeplearning4j.org/workspaces

	       Configuration config = new Configuration();
	        config.setBoolean(SVMLightRecordReader.ZERO_BASED_INDEXING, true);
	        config.setInt(SVMLightRecordReader.NUM_FEATURES, numOfFeatures);
  
	       try {
	            SVMLightRecordReader trainRecordReader = new SVMLightRecordReader();
				trainRecordReader.initialize(config, new FileSplit(file_training));
				DataSetIterator trainIter = new RecordReaderDataSetIterator(trainRecordReader, batchSize, numOfFeatures, numOfClasses);

				String mean_results ="Test;Recall;Precision;F-Measure;MCC\n";
				for (Entry<String, File> entry : files_testing.entrySet())
				{
			        SVMLightRecordReader testRecordReader = new SVMLightRecordReader();
			        testRecordReader.initialize(config, new FileSplit(entry.getValue()));
			        DataSetIterator testIter = new RecordReaderDataSetIterator(testRecordReader, batchSize, numOfFeatures, numOfClasses);
		
			        String results_test_case="Epoch;Recall;Precision;F-Measure;MCC\n";

				
			      //Set up network configuration
			        System.out.println("Build model....");
			        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
			            .seed(seed)
			            .updater(new Adam(2e-2))
			            .l2(1e-5)
			            .weightInit(WeightInit.XAVIER)
			            .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue).gradientNormalizationThreshold(1.0)
			            .trainingWorkspaceMode(WorkspaceMode.SEPARATE).inferenceWorkspaceMode(WorkspaceMode.SEPARATE)   //https://deeplearning4j.org/workspaces
			            .list()
			            .layer(0, new LSTM.Builder().nIn(numOfFeatures).nOut(256)
			                .activation(Activation.TANH).build())
			            .layer(1, new RnnOutputLayer.Builder().activation(Activation.SOFTMAX)
			                .lossFunction(LossFunctions.LossFunction.MCXENT).nIn(256).nOut(2).build())
			            .pretrain(false).backprop(true).build();
		
			        MultiLayerNetwork net = new MultiLayerNetwork(conf);
			        net.init();
			        net.setListeners(new ScoreIterationListener(1));
	        
			        for ( int n = 0; n < nEpochs; n++) {
			        	
			            net.fit(trainIter);
		
			            
			            System.out.println("Epoch "+(n+1)+" finished training");

		                Evaluation eval = new Evaluation(numOfClasses);
		                testIter.reset();
		                while(testIter.hasNext()) {
		                    DataSet t = testIter.next();
		                    INDArray features = t.getFeatures();
		                    INDArray labels = t.getLabels();
		                    INDArray predicted = net.output(features, false);
		                    eval.eval(labels, predicted);
		                }
		                results_test_case = results_test_case +(n+1)+";"+eval.recall()+";"+eval.precision()+";"+eval.f1()+";"+eval.matthewsCorrelation(EvaluationAveraging.Micro)+"\n";

		                project.createFile(results_test_case,filepathReports+"/"+entry.getKey()+".csv");
		                
		                if (n+1 == nEpochs)
		                {
		                	mean_results = mean_results +entry.getKey()+";"+eval.recall()+";"+eval.precision()+";"+eval.f1()+";"+eval.matthewsCorrelation(EvaluationAveraging.Micro)+"\n";
		                }
			        }    
				}
				
			   project.createFile(mean_results,filepathReports+"/mean_results.csv");
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
