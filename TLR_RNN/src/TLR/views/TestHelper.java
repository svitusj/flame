package TLR.views;

import java.io.File;
import java.util.HashMap;

import RNN.TLRRNN;
import TLR.GO.io.ProjectHelper;

public class TestHelper {
	
		
		public static void launchRNN(String filepath_kb, String filespath_fragments_kb, String filespath_descriptions_kb, String filepath_datasets, String filepath_reports) 
		{
			ProjectHelper project_helper = ProjectHelper.getInstance();
			TLRRNN tlr_rnn = new TLRRNN(project_helper);
			File file_training = tlr_rnn.prepareTraining(filepath_kb, filespath_fragments_kb, filespath_descriptions_kb);
			HashMap<String, File> files_testing = tlr_rnn.prepareTesting(filepath_datasets);
			
			tlr_rnn.launchRNN(file_training, files_testing, filepath_reports);
		}


}


