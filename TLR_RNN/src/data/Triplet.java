package data;

public class Triplet implements Comparable<Triplet>{
	
	public int id_description;
	public String id_fragment;
	public float score;
	
	
	public Triplet(int id_description, String id_fragment, float score) {
		super();
		this.id_description = id_description;
		this.id_fragment = id_fragment;
		this.score = score;
	}
	
	public int getIdDescription() {
		return id_description;
	}
	public void setIdDescription(int id_description) {
		this.id_description = id_description;
	}
	public String getIdFragment() {
		return id_fragment;
	}
	public void setIdFragment(String id_fragment) {
		this.id_fragment = id_fragment;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	
	@Override
	public int compareTo(Triplet kbelement) {
		if(this.getIdDescription() < kbelement.getIdDescription())
		{
			return -1;
		}
		else if(this.getIdDescription() > kbelement.getIdDescription())
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	

}
