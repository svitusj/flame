package FNN;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.datavec.api.conf.Configuration;
import org.datavec.api.records.reader.impl.misc.SVMLightRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.eval.EvaluationAveraging;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.eclipse.core.resources.IFile;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.Constants;
import TLR.GO.utils.FeatureVectorGenerator;
import data.KnowledgeBase;
import data.TestCase;

public class TLRFNN {

	private ProjectHelper project = null;
	
	
	public TLRFNN(ProjectHelper project) {
		super();
		this.project = project;
	}

	public File prepareTraining(String filepath_kb, String filespath_fragments_kb, String filespath_descriptions_kb) 
	{
				
		//Load the Knowledge Base
		KnowledgeBase knowledge_base = project.loadKnowledgeBase(filepath_kb, filespath_fragments_kb, filespath_descriptions_kb);
		
		//Encode Feature Vectors
		String feature_vectors = knowledge_base.encodeToFeatureVectors();
		
		// Save Feature Vectors
		String filename_training = Constants.FILEPATH_TRAINING;
		IFile ifile = project.createFile(feature_vectors, filename_training);
		
		return ifile.getRawLocation().makeAbsolute().toFile();		
	}
	
	public HashMap<String, File> prepareTesting(String filespath)
	{
		HashMap<String, File> testing_files = new HashMap<String, File>();
		ProjectHelper project = ProjectHelper.getInstance();
		FeatureVectorGenerator feature_vector_generator = new FeatureVectorGenerator();

		//Load Test Cases from Dataset
		HashMap<String, TestCase> test_cases = project.loadTestCases(filespath);
		
		//Launch Test Cases
		for (Entry<String, TestCase> entry : test_cases.entrySet())
		{
			TestCase test_case =((TestCase)entry.getValue());

			String feature_vectors = feature_vector_generator.getFeatureVector(test_case.getId(), test_case.getDescription(), test_case.getModel());
			
			//Save Feature Vectors
			String filename_testing = Constants.FILEPATH_TESTING+test_case.getId();
			IFile ifile = project.createFile(feature_vectors, filename_testing);
			testing_files.put(test_case.getId(), ifile.getRawLocation().makeAbsolute().toFile());
		}
		return testing_files;
	}
	
	public void launchFNN(File file_training, HashMap<String, File> files_testing, String filepathReport) {
		
		
		int numOfFeatures = 94;     // For MNIST data set, each row is a 1D expansion of a handwritten digits picture of size 28x28 pixels = 784
        int numOfClasses = 2;       // 10 classes (types of senders) in the data set. Zero indexing. Classes have integer values 0, 1 or 2 ... 9
        int batchSize = 10;          // 1000 examples, with batchSize is 10, around 100 iterations per epoch
        int printIterationsNum = 20; // print score every 20 iterations

        int hiddenLayer1Num = 200;
        long seed = 42;
        int nEpochs = 4;

        Configuration config = new Configuration();
        config.setBoolean(SVMLightRecordReader.ZERO_BASED_INDEXING, true);
        config.setInt(SVMLightRecordReader.NUM_FEATURES, numOfFeatures);

        try {
            SVMLightRecordReader trainRecordReader = new SVMLightRecordReader();
			trainRecordReader.initialize(config, new FileSplit(file_training));
			DataSetIterator trainIter = new RecordReaderDataSetIterator(trainRecordReader, batchSize, numOfFeatures, numOfClasses);

			String mean_results ="Test;Recall;Precision;F-Measure;MCC\n";
			for (Entry<String, File> entry : files_testing.entrySet())
			{
		        SVMLightRecordReader testRecordReader = new SVMLightRecordReader();
		        testRecordReader.initialize(config, new FileSplit(entry.getValue()));
		        DataSetIterator testIter = new RecordReaderDataSetIterator(testRecordReader, batchSize, numOfFeatures, numOfClasses);
	
		        String results_test_case="Epoch;Recall;Precision;F-Measure;MCC\n";
		        
		        System.out.println("Build model....");
		        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
		            .seed(seed)
		            .trainingWorkspaceMode(WorkspaceMode.SEPARATE)
		            .activation(Activation.RELU)
		            .weightInit(WeightInit.XAVIER)
		            .updater(Adam.builder().learningRate(0.02).beta1(0.9).beta2(0.999).build())
		            .l2(1e-4)
		            .list()
		            .layer(0, new DenseLayer.Builder().nIn(numOfFeatures).nOut(hiddenLayer1Num)
		                .build())
		            .layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
		                .activation(Activation.SOFTMAX)
		                .nIn(hiddenLayer1Num).nOut(numOfClasses).build())
		            .backprop(true).pretrain(false)
		            .build();
	
		        //run the model
		        MultiLayerNetwork model = new MultiLayerNetwork(conf);
		        model.init();
		        model.setListeners(new ScoreIterationListener(printIterationsNum));
	
		        for ( int n = 0; n < nEpochs; n++) {
	
		            model.fit(trainIter);
	
		            
		            System.out.println("Epoch "+(n+1)+" finished training");
		            
	                Evaluation eval = new Evaluation(numOfClasses);
	                testIter.reset();
	                while(testIter.hasNext()) {
	                    DataSet t = testIter.next();
	                    INDArray features = t.getFeatures();
	                    INDArray labels = t.getLabels();
	                    INDArray predicted = model.output(features, false);
	                    eval.eval(labels, predicted);
	                }
	                results_test_case = results_test_case +(n+1)+";"+eval.recall()+";"+eval.precision()+";"+eval.f1()+";"+eval.matthewsCorrelation(EvaluationAveraging.Micro)+"\n";
	                
	                project.createFile(results_test_case,filepathReport+"/"+entry.getKey()+".csv");
	                
	                if (n+1 == nEpochs)
	                {
	                	mean_results = mean_results +entry.getKey()+";"+eval.recall()+";"+eval.precision()+";"+eval.f1()+";"+eval.matthewsCorrelation(EvaluationAveraging.Micro)+"\n";
	                }
		        }    
			}
			
		   project.createFile(mean_results,filepathReport+"/mean_results.csv");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
              
        System.out.println("Finished...");
		
	}
}
