package data;

import org.eclipse.emf.ecore.EObject;

import flame.GO.codec.CODECUtils;
import flame.GO.core.Individual;

public class Fragment extends Individual{
	
	private String id;
	private EObject fragment;
	
	
	public Fragment(String id, EObject fragment) {
		super(CODECUtils.encode(fragment));
		this.id = id;
		this.fragment = fragment;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}	
	
	public EObject getFragment() {
		return fragment;
	}
	
	public void setFragment(EObject fragment) {
		this.fragment = fragment;
	}
	
}
