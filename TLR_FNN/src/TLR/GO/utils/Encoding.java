package TLR.GO.utils;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import TrainControlDSL.Cabin;
import TrainControlDSL.Condition;
import TrainControlDSL.Equipment;
import TrainControlDSL.EquipmentGroup;
import TrainControlDSL.EvaluableProperty;
import TrainControlDSL.InhibitableElement;
import TrainControlDSL.InvocatedOrder;
import TrainControlDSL.NestedCondition;
import TrainControlDSL.Order;
import TrainControlDSL.OrderInhibitionRule;
import TrainControlDSL.OrderInvocationRule;
import TrainControlDSL.Property;
import TrainControlDSL.PropertySynchronizationRule;
import TrainControlDSL.Rule;
import TrainControlDSL.SingleCondition;
import ontology.Ontology;
import ontology.RelationConcepts;

public class Encoding {
	
	public static final Ontology ONTOLOGY = new Ontology();
	
	
	public static ArrayList<Float> encodeDescriptionElement(String description, EObject fragment)
	{
		ArrayList<Float> feature_vector = new ArrayList<Float>();
				
		encodeFragment(fragment, feature_vector);
		
		encodeDescription(description, feature_vector);
		
		return feature_vector;
		
	}

	private static void encodeDescription(String description, ArrayList<Float> feature_vector) {
		
		ArrayList<String> description_words = ONTOLOGY.getTerms(description);

		for (String concept: ONTOLOGY.getConcepts())
		{
			feature_vector.add(presenceOfConcept(concept, description_words));
		}
		
		for(String attribute : ONTOLOGY.getAttributes())
		{
			feature_vector.add(presenceOfConcept(attribute, description_words));
		}
	}

	private static void encodeFragment(EObject fragment, ArrayList<Float> feature_vector) {
		ArrayList<String> fragment_concepts = new ArrayList<String>();
		ArrayList<String> fragment_properties = new ArrayList<String>();
		ArrayList<ArrayList<String>> fragment_relations = new ArrayList<ArrayList<String>>();
		TreeIterator<EObject> iterator = fragment.eAllContents();
		
		while(iterator.hasNext())
		{
			EObject element = iterator.next();
			if (element instanceof Equipment)
			{
				fragment_concepts.add(ONTOLOGY.getWordRoot(((Equipment)element).getName()));
			}
			else if (element instanceof Property)
			{
				fragment_properties.add(ONTOLOGY.getWordRoot(((Property)element).getName()));
			}
			else if (element instanceof Rule)
			{
				fragment_relations.add(getKeywordsOfRule((Rule)element));
			}
		}
		
		for (String concept: ONTOLOGY.getConcepts())
		{
			feature_vector.add(presenceOfConcept(concept, fragment_concepts));
		}
		
		for(String attribute : ONTOLOGY.getAttributes())
		{
			feature_vector.add(presenceOfConcept(attribute, fragment_properties));
		}
		
		for ( Entry<Integer, RelationConcepts> entry :ONTOLOGY.getRelations().entrySet())
		{
			RelationConcepts relation = entry.getValue();
			feature_vector.add(presenceOfRelation(relation.getFirstConcept(), relation.getSecondConcept(), fragment_relations));
		}
	}
	
	private static Float presenceOfConcept(String concept, ArrayList<String> concepts_set) {
		float num_concepts = 0;
		
		for (int i=0; i<concepts_set.size(); i++)
		{
			if (concept.equals(concepts_set.get(i)))
			{
				num_concepts++;
			}
		}
		
		return num_concepts;
		
	}
	
	private static Float presenceOfRelation(String concept1, String concept2, ArrayList<ArrayList<String>> fragment_relations) {
		
		if (fragment_relations.contains(concept1) && fragment_relations.contains(concept2))
		{
			for (int n=0;n<fragment_relations.size(); n++)
			{
				if (fragment_relations.get(n).contains(concept1) && fragment_relations.get(n).contains(concept2))
				{
					return (float) 1;
				}
			}
			return (float) 0;
		}
		else if (!fragment_relations.contains(concept1) && !fragment_relations.contains(concept2))
		{
			for (int n=0;n<fragment_relations.size(); n++)
			{
				if (fragment_relations.get(n).contains(concept1) && fragment_relations.get(n).contains(concept2))
				{
					return (float) 0;
				}
			}
			return (float) 1;
		}
		else
		{
			return (float) 1;
		}
	}
	
	private static ArrayList<String> getKeywordsOfRule(Rule rule)
	{
		ArrayList<String> equimpents_related = new ArrayList<String>();
		
		//TRIGGER
		if(rule.getHasTrigger()!=null) 
		{
			if(rule.getHasTrigger().getProperty()!=null)
			{
				equimpents_related.addAll(getKeywordsOfProperty(rule.getHasTrigger().getProperty()));
			}
		}
			
		//SYNCHRONIZATION RULE
		if (rule instanceof PropertySynchronizationRule)
		{
			
			if(((PropertySynchronizationRule) rule).getProperty()!=null)
			{
				equimpents_related.addAll(getKeywordsOfProperty(((PropertySynchronizationRule)rule).getProperty()));
			}
			
			Condition condition = ((PropertySynchronizationRule) rule).getHasConditions();
			equimpents_related.addAll(getKeywordsOfConditions(condition));
						
		}
		//INVOCATION RULE
		else if (rule instanceof OrderInvocationRule){
			for (InvocatedOrder invocated_order : ((OrderInvocationRule)rule).getHasPhysicalEffects())
			{
				if(invocated_order.getOrder()!=null)
				{
					equimpents_related.addAll(getKeywordsOfOrders(invocated_order.getOrder()));
				}
				
				Condition condition = (invocated_order.getHasConditions());
				equimpents_related.addAll(getKeywordsOfConditions(condition));
			}
		}
		//INHIBITION RULE
		else
		{
			InhibitableElement inhibitable_element = ((OrderInhibitionRule)rule).getInhibitableElement();
			if(inhibitable_element !=null)
			{
				if (inhibitable_element instanceof Equipment)
				{
					equimpents_related.add(((Equipment)inhibitable_element).getName());
				}
				else if (inhibitable_element instanceof Order)
				{
					equimpents_related.addAll(getKeywordsOfOrders((Order)inhibitable_element));
				}
			}
			
			Condition condition = (((OrderInhibitionRule)rule).getHasConditions());
			equimpents_related.addAll(getKeywordsOfConditions(condition));
		}
		
		//Remove duplicated values
		equimpents_related = (ArrayList<String>) equimpents_related.stream().distinct().collect(Collectors.toList());
		
		//Format keywords in lower case and without numbers
		for(int i=0;i<equimpents_related.size();i++)
		{
			equimpents_related.set(i, ONTOLOGY .getWordRoot(equimpents_related.get(i)));
		}
		
		return equimpents_related;

	}
	
	private static ArrayList<String> getKeywordsOfOrders(Order order) 
	{	
		ArrayList<String> equipments_related = new ArrayList<String>();
		
		EObject econtainer = order.eContainer();
		if (econtainer instanceof Equipment)
		{
			equipments_related.add(((Equipment)econtainer).getName());
		}
		else if (econtainer instanceof EquipmentGroup)
		{
			for (Equipment equipment : ((EquipmentGroup)econtainer).getMembers())
			{
				equipments_related.add(equipment.getName());
			}
		}
		
		return equipments_related;
	}

	private static ArrayList<String> getKeywordsOfProperty(EvaluableProperty property)
	{
		ArrayList<String> keywords = new ArrayList<String>();
		
		if(property.eContainer() instanceof Equipment)
		{
			Equipment equipment = (Equipment)property.eContainer();
			keywords.add(equipment.getName());
			
			if(equipment.getInstalled()!=null)
			{
				keywords.add(equipment.getInstalled().getID());
			}
			
		}
		else if (property instanceof Cabin)
		{
			keywords.add(((Cabin)property).getID());
		}
		
		return keywords;
	}
	
	private static ArrayList<String> getKeywordsOfConditions(Condition condition) {
		ArrayList<String> keywords = new ArrayList<String>();
		if(condition != null)
		{
			if (condition instanceof NestedCondition){
				NestedCondition nested_condition = (NestedCondition)condition;
				keywords.addAll(getKeywordsOfConditions(nested_condition.getLeft()));
				keywords.addAll(getKeywordsOfConditions(nested_condition.getRight()));
				if(nested_condition.getProperty()!=null)
				{
					keywords.addAll(getKeywordsOfProperty(nested_condition.getProperty()));
				}
			}
			else
			{
				SingleCondition single_condition = (SingleCondition)condition;
				if (single_condition.getCondition() != null)
				{
					keywords.addAll(getKeywordsOfConditions(single_condition.getCondition()));
				}
				if (single_condition.getProperty()!= null)
				{
					keywords.addAll(getKeywordsOfProperty(single_condition.getProperty()));
				}
			}
		}
		return keywords;
	}

}
