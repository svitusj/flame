package TLR.GO.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import TrainControlDSL.Equipment;
import TrainControlDSL.Property;
import TrainControlDSL.Rule;
import data.Description;

public class FeatureVectorGenerator {

	private static DecimalFormat decimal_format_to_features = new DecimalFormat();
	private static DecimalFormat decimal_format_to_scores = new DecimalFormat();

	static {
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');

		decimal_format_to_features.setMinimumFractionDigits(6);
		decimal_format_to_features.setDecimalFormatSymbols(otherSymbols);

		decimal_format_to_scores.setMinimumFractionDigits(1);
		decimal_format_to_scores.setDecimalFormatSymbols(otherSymbols);
	}

	public String getFeatureVector(float score, Description description, EObject element, String id_model) {

		String feature_vector = "";
		
		if ((element instanceof Equipment) || (element instanceof Rule))
		{
			// Score
			feature_vector = feature_vector + decimal_format_to_scores.format(score) + " ";

			// ID feature vector
			int index = 1;
			feature_vector = feature_vector + "qid:" + description.getId() + " " ;

			// Features-Values (For: Description & Fragment) feature vector
			ArrayList<Float> encoding_description_fragment = Encoding.encodeDescriptionElement(description.getDescrition(), element);
			feature_vector = feature_vector + getFeatureVector(index, encoding_description_fragment);
			
			// Comments
			feature_vector = feature_vector + "# " + id_model+" "+getElementID(element) + "\n";
			
		}
		
		return feature_vector;

	}
	
	public String getFeatureVector(String id, String description, EObject model) {

		String feature_vector = "";
		TreeIterator<EObject> iterator = model.eAllContents();
		
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if ((element instanceof Equipment) || (element instanceof Rule))
			{
				// Score
				float score = 0;
				if (element instanceof Equipment)
				{ 
					 score = Float.parseFloat(""+((Equipment) element).getName().charAt(0));
				}
				else
				{
					score = Float.parseFloat(""+((Rule) element).getID().charAt(0));
				}
				feature_vector = feature_vector + decimal_format_to_scores.format(score) + " ";

				// ID feature vector
				int index = 1;
				feature_vector = feature_vector + "qid:" + id + " " ;

				// Features-Values (For: Description & Fragment) feature vector
				ArrayList<Float> encoding_description_fragment = Encoding.encodeDescriptionElement(description, element);
				feature_vector = feature_vector + getFeatureVector(index, encoding_description_fragment);
				
				// Comments
				feature_vector = feature_vector + "# " + getElementID(element) + "\n";
				
			}
		}
		
		return feature_vector;

	}
	
	private String getElementID(EObject element) {
		if ((element instanceof Equipment))
		{
			return ((Equipment)element).getName();
		}
		else
		{
			return ((Rule)element).getID();
		}
	}

	private String getFeatureVector(int index, ArrayList<Float> features)
	{
		String feature_vector = "";
		
		for(Float value: features)
		{
			feature_vector = feature_vector + index+":"+decimal_format_to_features.format(value)+" ";
			index++;
		}
		
		return feature_vector;
	}
	
}
