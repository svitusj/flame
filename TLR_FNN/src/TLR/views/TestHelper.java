package TLR.views;

import java.io.File;
import java.util.HashMap;

import FNN.TLRFNN;
import TLR.GO.io.ProjectHelper;

public class TestHelper {
	
		public static void launchFNN(String filepath_kb, String filespath_fragments_kb, String filespath_descriptions_kb, String filepath_datasets, String filepath_reports) 
		{
			ProjectHelper project_helper = ProjectHelper.getInstance();
			TLRFNN tlrFNN = new TLRFNN(project_helper);
			File file_training = tlrFNN.prepareTraining(filepath_kb, filespath_fragments_kb, filespath_descriptions_kb);
			HashMap<String, File> files_testing = tlrFNN.prepareTesting(filepath_datasets);
			
			tlrFNN.launchFNN(file_training, files_testing, filepath_reports);
		}

}


