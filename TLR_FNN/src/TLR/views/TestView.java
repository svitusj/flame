package TLR.views;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import TLR.GO.utils.Constants;
import eval.core.Evaluator;


public class TestView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "TLRFNN.views.TLRFNN";


	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		
		Composite panel = new Composite(parent, SWT.NONE);
		RowLayout layout = new RowLayout();
		panel.setLayout(layout);
		
		//Train Classifier.
		Button b1 = new Button(panel, SWT.NONE);
		b1.setText("Feedforward Neural Network");
		b1.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestHelper.launchFNN(Constants.FILEPATH_KNOWLEDGE_BASE, Constants.FOLDERPATH_KNOWLEDGE_BASE_FRAGMENTS, Constants.FOLDERPATH_KNOWLEDGE_BASE_DESCRIPTIONS, Constants.FOLDERPATH_DATASET, Constants.FOLDER_REPORTS);
			}
		});
		
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		
	}
}
