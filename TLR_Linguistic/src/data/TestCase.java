package data;

import org.eclipse.emf.ecore.EObject;

public class TestCase{
	
	private String id;
	private EObject model;
	private String description;
		
	public TestCase(String id, EObject model, String description) {
		this.id = id;
		this.model = model;
		this.description = description;
	}

	public String getId()
	{
		return id;
	}
	
	public EObject getModel() {
		return model;
	}

	public String getDescription()
	{
		return description;
	}

	
}
