package views;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import eval.core.Evaluator;
import mtv.GO.utils.Constants;


public class TestView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "tlr_linguistic.views.TLRLinguisticView";


	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		
		Composite panel = new Composite(parent, SWT.NONE);
		RowLayout layout = new RowLayout();
		panel.setLayout(layout);
		
		Button b1 = new Button(panel, SWT.NONE);
		b1.setText("TLR Linguistic");
		b1.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestHelper.tlrLinguistic();
			}
		});
		
		Button b2 = new Button(panel, SWT.NONE);
		b2.setText("Generate Reports");
		b2.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				Evaluator evaluator = new Evaluator(Constants.FOLDER_RESULTS, Constants.FOLDER_REPORTS);
				evaluator.evaluateResults();
			}
		});
		
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		
	}
}
