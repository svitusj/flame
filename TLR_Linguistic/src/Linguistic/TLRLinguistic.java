package Linguistic;

import java.util.HashMap;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EObject;

import TrainControlDSL.TrainControlDSLFactory;
import data.TestCase;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import mtv.GO.io.ProjectHelper;
import mtv.GO.utils.Constants;

public class TLRLinguistic {

	private static TrainControlDSLFactory factory = TrainControlDSLFactory.eINSTANCE;

	public static void tlrLinguistic(ProjectHelper project) 
	{
		MaxentTagger tagger = new MaxentTagger(project.getProjectLocation()+Constants.TAGGER_SETUP);
		
		//Load Test Cases
		HashMap<String, TestCase> test_cases = project.loadTestCases();
		
		for (Entry<String, TestCase> entry : test_cases.entrySet())
		{
			//POS tagging
			String tagged_description = tagger.tagString(entry.getValue().getDescription());
			
			//Apply the rules
			EObject fragment_solution = factory.createTrainUnit();
			EObject source_model = entry.getValue().getModel(); 
			fragment_solution = Rules.rule1(source_model, fragment_solution, tagged_description);
			fragment_solution = Rules.rule2(source_model, fragment_solution, tagged_description);
			fragment_solution = Rules.rule3(source_model, fragment_solution, tagged_description);
			
			//Save solution
			String filename = Constants.FOLDER_RESULTS+entry.getValue().getId()+"."+Constants.EXTENSION_MODELS;
			project.createFile(filename, fragment_solution);
		}

		
	}
}
