package Linguistic;

import java.util.ArrayList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import TrainControlDSL.Equipment;
import TrainControlDSL.Property;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainUnit;

public class Rules {

	public static EObject rule1(EObject source_model, EObject solution, String tagged_description) {
		
		ArrayList<String> tags = getTags(tagged_description);
		ArrayList<String> terms = getTerms(tagged_description);
		
		for (int i=0; i<tags.size(); i++)
		{
			if (tags.get(i).length()>=2)
			{
				if (tags.get(i).substring(0,2).equals("NN"))
				{
					searchEquipment(source_model, terms.get(i), solution);
				}
			}
		}
		
		return solution;
	}
	
	private static void searchEquipment(EObject source_model, String class_id, EObject solution) 
	{
		TreeIterator<EObject> eAllContents = source_model.eAllContents();
		while (eAllContents.hasNext()) {
		      EObject next_object = eAllContents.next();
		      if ((next_object instanceof Equipment) && getWordRoot(((Equipment)next_object).getName()).equals(class_id))
			  {
		    	  Equipment equipment = (Equipment) copyEObject(next_object);
		    	  ((TrainUnit)solution).getHasEquipments().add(equipment);
			  }
		 }
	}

	public static EObject rule2(EObject source_model, EObject solution, String tagged_description) {
		
		ArrayList<String> tags = getTags(tagged_description);
		ArrayList<String> terms = getTerms(tagged_description);
		
		for (int i=0; i<tags.size(); i++)
		{
			if (tags.get(i).length()>=2)
			{
				if (tags.get(i).equals("VBN") || tags.get(i).equals("JJ"))
				{
					searchProperty(source_model, terms.get(i), solution);
				}
			}
		}
		
		return solution;
	}
	
	private static void searchProperty(EObject source_model, String property_id, EObject solution) {
		TreeIterator<EObject> eAllContents = source_model.eAllContents();
		while (eAllContents.hasNext()) {
		      EObject next_object = eAllContents.next();
		      if ((next_object instanceof Property) && getWordRoot(((Property)next_object).getName()).equals(property_id))
			  {
		    	  Property property = (Property) copyEObject(next_object);
		    	  EObject container = next_object.eContainer();
		    	  if(container instanceof Equipment)
		    	  {
		    		  TreeIterator<EObject> solution_iterator = solution.eAllContents();
		    		  while(solution_iterator.hasNext())
		    		  {
		    			  EObject solution_object = solution_iterator.next();
		    			  if(solution_object instanceof Equipment)
		    			  {
		    				  if(((Equipment)solution_object).getName().equals(((Equipment)container).getName()))
		    				  {
		    					  ((Equipment)solution_object).getHasProperties().add(property);
		    				  }
		    			  }
		    		  }
		    	  }
		    	  
			  }
		 }
		
	}

	public static EObject rule3(EObject source_model, EObject solution, String tagged_description) {
		
		ArrayList<String> tags = getTags(tagged_description);
		ArrayList<String> terms = getTerms(tagged_description);
		
		for (int i=0; i<tags.size(); i++)
		{
			if (tags.get(i).length()>=2)
			{
				if (tags.get(i).substring(0,2).equals("NN"))
				{
					searchRule(source_model, terms.get(i), solution);
				}
			}
		}
		
		return solution;
	}

	private static void searchRule(EObject source_model, String rule_id, EObject solution) {
		
		TreeIterator<EObject> eAllContents = source_model.eAllContents();
		while (eAllContents.hasNext()) {
		      EObject next_object = eAllContents.next();
		      if (next_object instanceof Rule)
			  {
		    	  if(((Rule)next_object).getID()!=null)
					{
			    	  String[] rule = ((Rule)next_object).getID().split("_");
			    	  if (rule.length>0)
			    	  {
			    		  for(String keyword_rule : rule)
			    		  {
			    			  if(keyword_rule.equals(rule_id))
			    			  {
			    				  Rule solution_rule = (Rule) copyEObject(next_object);
			    				  ((TrainUnit)solution).getHasRules().add(solution_rule);
			    			  }
			    		  }
			    	  }		 
					}
			  }
		 }
		
	}

	
	private static EObject copyEObject(EObject oldObject)
	{
       
		EObject result = EcoreUtil.copy(oldObject);
        
        while(!result.eContents().isEmpty())
        {
        	result.eUnset(result.eContents().get(0).eContainingFeature());
        }
        
        return result;
	}
	
	private static String getWordRoot(String word)
	{
		if(word != null)
		{
			if (!word.isEmpty())
			{
				word = word.split(" ")[0];
				int index_last_character = word.length()-1;
				
				//If last character is a digit
				while (Character.isDigit(word.charAt(index_last_character))){
					word = word.substring(0, index_last_character);
					index_last_character--;
				}
				
				//If last character is 's'
				if (word.charAt(index_last_character) == 's'){
					word = word.substring(0, index_last_character);
				}
				
				return word.toLowerCase();
			}
		}
		
		return null;

	}

	private static ArrayList<String> getTags(String setence)
	{
		ArrayList<String> tags = new ArrayList<String>();
		
		String[] terms = setence.split(" ");
		for (String term: terms)
		{
			tags.add(term.split("_")[1]);
		}
		return tags;
	}
	
	private static ArrayList<String> getTerms(String setence)
	{
		ArrayList<String> tags = new ArrayList<String>();
		
		String[] terms = setence.split(" ");
		for (String term: terms)
		{
			tags.add(term.split("_")[0]);
		}
		return tags;
	}

}
