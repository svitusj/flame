package mtv.GO.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import data.TestCase;
import mtv.GO.utils.Constants;

public class ProjectHelper {

	private IProject project;
	private static ProjectHelper instance;
	
	public static ProjectHelper getInstance() {
		if (instance == null)
			instance = new ProjectHelper();

		return instance;
	}
	
	private ProjectHelper() {
		checkProject();
	}

	private void checkProject() {

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		project = root.getProject(Constants.PROJECT_NAME);
		try {
			if (!project.exists()) {
				project.create(null);
			}
			project.open(null);
			copyInputsToProject();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void copyInputsToProject() {
				
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		String location_inputs = root.getLocation().uptoSegment(root.getLocation().segmentCount()-1) + "/" + Constants.WORSPACE_PLUGIN + "/" + Constants.PROJECT_DATA;
		File srcfolder = new File (location_inputs);
				
		IContainer destFolder = root.getContainerForLocation(project.getLocation());
		
		copyFiles(srcfolder, destFolder);

	}
	
	private void copyFiles(File srcFolder, IContainer destFolder) {
		for (File f : srcFolder.listFiles()) {
			if (f.isDirectory()) {
				IFolder newFolder = destFolder.getFolder(new Path(f.getName()));
				try {
					if (!newFolder.exists()) {
						newFolder.create(true, true, null);
					}
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				copyFiles(f, newFolder);
			} else {
				IFile newFile = destFolder.getFile(new Path(f.getName()));
					if(!newFile.exists()){
						try {
							newFile.create(new FileInputStream(f), true, null);
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (CoreException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			}
		}
	}
	
	
	public String getProjectLocation()
	{
		return project.getLocation().toString();
	}

	public void createFile(String name, EObject content) {
		IFile f = project.getFile(name);
		if (!f.exists()) {
			ResourceSet resSet = new ResourceSetImpl();
			Resource resource = resSet.createResource(URI.createPlatformResourceURI(f.getFullPath().toString(), true));
			resource.getContents().add(content);
			try {
				resource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public HashMap<String, TestCase> loadTestCases()
	{
		HashMap<String, TestCase> test_cases = new HashMap<String, TestCase>();
		try {
			IFolder modelsfolder = project.getFolder(Constants.FOLDERPATH_DATASET);
			for (IResource res : modelsfolder.members()) {
				if (res instanceof IFolder) {
					IFolder test_case_folder = (IFolder) res;
					String id_model = test_case_folder.getName();
					
					EObject model = null;
					String description = null;
					for (IResource res2 : test_case_folder.members())
					{	
						if (res2 instanceof IFile)
						{
							IFile file = (IFile) res2;
							if (file.getName().equals(Constants.FILENAME_MODEL))
							{
								model = loadModel(file);
							}
							else if (file.getName().equals(Constants.FILENAME_REQUIREMENT))
							{
								description = loadDescription(file);
							}
						}
					}
					if(model!= null && description!=null)
					{
						test_cases.put(id_model, new TestCase(id_model, model, description));
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return test_cases;
	}

	private String loadDescription(IFile file) throws CoreException {
		if (file.getFileExtension().equals(Constants.EXTENSION_TXT)) {
			if (file.exists()) {
				InputStream stream = file.getContents();
				String description = new BufferedReader(new InputStreamReader(stream)).lines()
						.collect(Collectors.joining("\n"));
				return description;
			}
		}
		return null;
	}
	
	private EObject loadModel(IFile f) {
		
		if (f.exists()) {
	
			String filename = f.getFullPath().toString();
			ResourceSet resSet = new ResourceSetImpl();
			URI uri = URI.createFileURI(filename);
			Resource resource = resSet.getResource(uri, true);
			EObject myModel = (EObject) resource.getContents().get(0);
			return myModel;
		}
		return null;
	
	}
	
}
