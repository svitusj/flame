package mtv.GO.utils;

public class Constants {

	public static final String WORSPACE_PLUGIN = "workspace_TLR_Linguistic/TLR_Linguistic";
	public static final String PROJECT_NAME = "TRAINCONTROL";
	public static final String PROJECT_DATA = "data";
	
	public static final String EXTENSION_MODELS = "traincontroldsl";	
	public static final String EXTENSION_TXT = "txt";
	public static final String EXTENSION_XML = "xml";
	
	public static final String FOLDERPATH_DATASET = "dataset/";
	public static final String FILENAME_MODEL = "model" + "."+EXTENSION_MODELS;	
	public static final String FILENAME_REQUIREMENT = "requirement"+"."+EXTENSION_TXT;
	
	public static final String FOLDER_RESULTS = "results/";
	public static final String FOLDER_REPORTS = "reports/";
	
	public static final String TAGGER_SETUP = "/tagger/english-left3words-distsim.tagger";
	}
