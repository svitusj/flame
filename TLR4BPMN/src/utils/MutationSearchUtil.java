package utils;

import java.util.ArrayList;
import java.util.Random;

import classes.Model;
import classes.ModelElement;
import classes.ModelFragment;

public class MutationSearchUtil {
	
	private int populationSize;
	private ArrayList<ModelFragment> population;
	
	public MutationSearchUtil(int populationSize, Model model) {
		
		this.populationSize = populationSize;
		this.population = populationGeneration(populationSize, model);
	}
	

	/* Population methods */
	
	private ModelFragment fragmentGeneration(int id, Model model) {
		
		Random randomizer = new Random();
		
		// Generate seed element
		int seed = randomizer.nextInt(model.size());
		ModelElement seedElement = model.getElements().get(seed);		
		ModelElement neighbor = seedElement.getNeighbor();
		
		// Generate the list of elements
		ArrayList<ModelElement> elements = new ArrayList<ModelElement>();
		elements.add(seedElement);
		
		// Populate the list of elements
		int iterations = randomizer.nextInt(model.size());
		for (int i = 0; i < iterations; i++) {
			
			ModelElement previous = seedElement;
			seedElement = neighbor;
			elements.add(seedElement);
			neighbor = seedElement.getNeighbor(previous);
			
			if(neighbor.equals(null)) {
				break;				
			}
			
		}
		
		// Generate the fragment with the list of elements
		ModelFragment fragment = new ModelFragment(id, elements, model);
		return fragment;
		
	}
	
	private ArrayList<ModelFragment> populationGeneration(int populationSize, Model model) {
		
		ArrayList<ModelFragment> initialPopulation = new ArrayList<ModelFragment>();
		
		for(int i = 0; i < populationSize; i++) {
			
			Model parentModel = model;
			ModelFragment individual = fragmentGeneration(i, parentModel);
			initialPopulation.add(individual);
			
		}
		
		return initialPopulation;
		
	}
	
	/* Getters */

	public ArrayList<ModelFragment> getPopulation() {
		return population;
	}

	public int getPopulationSize() {
		return populationSize;
	}
	
}
