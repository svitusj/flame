package classes;

import java.util.ArrayList;

public class Model {
	
	private int id;
	private ArrayList<ModelElement> elements;
	
	public Model(int id, ArrayList<ModelElement> elements) {
		this.id = id;
		this.elements = elements;
	}

	public int getId() {
		return id;
	}

	public ArrayList<ModelElement> getElements() {
		return elements;
	}
	
	public int size() {
		return elements.size();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
