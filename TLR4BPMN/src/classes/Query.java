package classes;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class Query {
	
	private int id;
	private String text;
	private ArrayList<String> processedText;
	private ArrayList<String> posTags;
	private ArrayList<String> lemmas;
	private LinkedHashSet<String> terms;
	
	public Query(int id, String text) {
		
		this.id = id;
		this.text = text;
		
	}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}
	
	public ArrayList<String> getProcessedText() {
		return processedText;
	}
	
	public void setProcessedText(ArrayList<String> processedText) {
		this.processedText = processedText;
	}

	public ArrayList<String> getPosTags() {
		return posTags;
	}

	public void setPosTags(ArrayList<String> posTags) {
		this.posTags = posTags;
	}

	public ArrayList<String> getLemmas() {
		return lemmas;
	}

	public void setLemmas(ArrayList<String> lemmas) {
		this.lemmas = lemmas;
	}

	public LinkedHashSet<String> getTerms() {
		return terms;
	}

	public void setTerms(LinkedHashSet<String> terms) {
		this.terms = terms;
	}
}
