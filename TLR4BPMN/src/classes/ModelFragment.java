package classes;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Random;

public class ModelFragment implements Comparable<ModelFragment> {
	
	private int id;
	private ArrayList<ModelElement> elements;
	private Model parent;
	private double fitness;

	public ModelFragment(int id, ArrayList<ModelElement> elements, Model parent) {
		
		this.id = id;
		this.elements = elements;
		this.parent = parent;
		
	}
	
	public int size() {
		return elements.size();
	}

	public int getId() {
		return id;
	}

	public String getText() {
		String text = "";
		for(ModelElement element : elements) {
			text.concat(element.getText() + " ");
		}		
		text.trim();
		return text;
	}
	
	public ArrayList<String> getProcessedText() {
		ArrayList<String> processedText = new ArrayList<String>();
		for(ModelElement element : elements) {
			processedText.addAll(element.getProcessedText());
		}
		return processedText;
	}
	
	public LinkedHashSet<String> getTerms(){
		LinkedHashSet<String> terms = new LinkedHashSet<String>();
		for(ModelElement element : elements) {
			terms.addAll(element.getTerms());
		}
		return terms;
	}

	public ArrayList<ModelElement> getElements() {
		return elements;
	}
	
	public void setElements(ArrayList<ModelElement> elements) {
		this.elements = elements;
	}
	
	public Model getParent() {
		return parent;
	}
	
	public void setParent(Model parent) {
		this.parent = parent;
	}
	
	public double getFitness() {
		return fitness;
	}
	
	public void setFitness(double fitness) {
		this.fitness = fitness;
	}

	public int compareTo(ModelFragment fragment)
	{
		if (fragment.getFitness() > this.getFitness()) return 1;
		else if (fragment.getFitness() < this.getFitness()) return -1;
		else return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModelFragment other = (ModelFragment) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	public boolean isInsideModel(Model model) {
		
		if(model.getElements().containsAll(elements)) return true;
		else return false;
		
	}
	
	public ModelElement findAdditionCandidate() {
		
		ArrayList<ModelElement> candidates = new ArrayList<ModelElement>();
		
		for(ModelElement element : elements) {

			// If the element has more than one connection
			// we check that it has connections not in the model
			// and add them to the list of candidates
			if (element.getConnections().size() > 1) {
				
				ArrayList<ModelElement> connections = element.getConnections();
				for(ModelElement connection : connections) if(!elements.contains(connection)) candidates.add(connection);

			}

		}

		// Choose one of the candidates
		Random randomizer = new Random();
		int seed = randomizer.nextInt(candidates.size());
		ModelElement chosen = candidates.get(seed);
		
		return chosen;
	}
	
	public ModelElement findRemovalCandidate() {
		
		ArrayList<ModelElement> candidates = new ArrayList<ModelElement>();
		
		for(ModelElement element : elements) {
			
			// If the element has only one connection it is in the fragment and the element is a candidate
			if (element.getConnections().size() == 1) candidates.add(element);
			// Else count how many connections are inside the fragment
			else {				
				ArrayList<ModelElement> connections = element.getConnections();
				int count = 0;
				for(ModelElement connection : connections) if(elements.contains(connection)) count++;
				// If the count is only one, the element is a candidate
				if(count == 1) candidates.add(element);
			}
			
		}
		
		Random randomizer = new Random();
		int seed = randomizer.nextInt(candidates.size());
		ModelElement chosen = candidates.get(seed);
		
		return chosen;
	}
	
	
}
