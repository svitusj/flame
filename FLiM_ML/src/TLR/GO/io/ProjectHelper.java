package TLR.GO.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import FLiM_ML.Encoding.Encoding;
import TLR.GO.utils.Constants;
import TLR.GO.utils.FeatureVectorGenerator;
import data.Description;
import data.Fragment;
import data.TestCase;
import data.Result.Result;

public class ProjectHelper {

	public IProject project;
	private static ProjectHelper instance;

	public static ProjectHelper getInstance() {
		if (instance == null)
			instance = new ProjectHelper();

		return instance;
	}
	
	private ProjectHelper() {
		checkProject();
	}

	private void checkProject() {

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		project = root.getProject(Constants.PROJECT_NAME);
		try {
			if (!project.exists()) {
				project.create(null);
			}
			project.open(null);
			//copyInputsToProject();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public File getFile(String filepath)
	{
		IFile ifile = project.getFile(filepath);
		return ifile.getRawLocation().makeAbsolute().toFile();
	}
	
	/*private void copyInputsToProject() {
				
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		String location_inputs = root.getLocation().uptoSegment(root.getLocation().segmentCount()-1) + "/" + Constants.WORSPACE_PLUGIN + "/" + Constants.PROJECT_DATA;
		File srcfolder = new File (location_inputs);
				
		IContainer destFolder = root.getContainerForLocation(project.getLocation());
		
		copyFiles(srcfolder, destFolder);

	}*/
	
	/*private void copyFiles(File srcFolder, IContainer destFolder) {
		for (File f : srcFolder.listFiles()) {
			if (f.isDirectory()) {
				IFolder newFolder = destFolder.getFolder(new Path(f.getName()));
				try {
					if (!newFolder.exists()) {
						newFolder.create(true, true, null);
					}
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				copyFiles(f, newFolder);
			} else {
				IFile newFile = destFolder.getFile(new Path(f.getName()));
					if(!newFile.exists()){
						try {
							newFile.create(new FileInputStream(f), true, null);
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (CoreException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			}
		}
	}*/
	
	public String getProjectLocation()
	{
		return project.getLocation().toString();
	}

	public void createFile(String file_content, String filepath, String filename) {
	
		InputStream input = new ByteArrayInputStream(file_content.getBytes());
		
	    /*try {
	    	
	    	Path path = Paths.get(getProjectLocation()+filepath);
	    	if(Files.notExists(path)){
	            Files.createFile(Files.createDirectories(path)).toFile();
	        }
	    } catch (IOException e) {}*/
		IFile ifile = project.getFile(filepath+filename);
	    try {
	    	checkPath(project.getFolder(filepath));
	    	if (!ifile.exists()) {
	    		ifile.create(input, IResource.FORCE, null);
	    	} else {
	    		ifile.setContents(input, IResource.FORCE, null);
	    	}
	    } catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        try {
	            input.close();
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
	    }	
	}
	

	private void checkPath(final IResource resource) throws CoreException 
	{
		if (!resource.exists())
		{
			checkPath(resource.getParent());
			if(resource.getType()== IResource.FOLDER )
			{
				((IFolder) resource).create(IResource.NONE, true, null);
			}
		}
	}

	public void createFile(String name, EObject content) {
		IFile f = project.getFile(name);
		//if (!f.exists()) {
			ResourceSet resSet = new ResourceSetImpl();
			Resource resource = resSet.createResource(URI.createPlatformResourceURI(f.getFullPath().toString(), true));
			resource.getContents().add(content);
			try {
				resource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//}
	}

	public void encodeKnowledgeBase(Encoding encoding, String outputFilesPath)
	{
		String knowledgeBasePath = Constants.FOLDER_KNOWLEDGE_BASE;
		FeatureVectorGenerator featureVectorGenerator = new FeatureVectorGenerator();
		String trainingFeatureVectors = "";
		
		//Load descriptions
		HashMap<Integer, Description> descriptions = loadDescriptions(knowledgeBasePath+Constants.FOLDER_KNOWLEDGE_BASE_DESCRIPTIONS);
		
		try {
			File fXmlFile = new File(getProjectLocation()+knowledgeBasePath+Constants.FILENAME_KNOWLEDGE_BASE);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			
			doc.getDocumentElement().normalize();
	
			NodeList nList = doc.getElementsByTagName(Constants.TAG_FRAGMENT);
	
			for (int i = 0; i < nList.getLength(); i++) {
	
				Node nNode = nList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	
					Element eElement = (Element) nNode;
			
					//Load Score
					float score = Float.parseFloat(eElement.getElementsByTagName(Constants.TAG_SCORE).item(0).getTextContent());
					
					//Select suitable description from loaded descriptions
					int idDescription = Integer.parseInt(eElement.getElementsByTagName(Constants.TAG_ID_QUERY).item(0).getTextContent());
					Description description = descriptions.get(idDescription);
					
					//Load Fragment
					String idFragment = eElement.getElementsByTagName(Constants.TAG_FILENAME).item(0).getTextContent();
					String idModel = eElement.getElementsByTagName(Constants.TAG_MODEL).item(0).getTextContent();
					Fragment fragment = loadFragment(idFragment,knowledgeBasePath+Constants.FOLDER_KNOWLEDGE_BASE_FRAGMENTS+idFragment, knowledgeBasePath+Constants.FOLDER_KNOWLEDGE_BASE_MODELS+idModel);
					
					//Encode triplet: score-description-fragment
					trainingFeatureVectors = trainingFeatureVectors + featureVectorGenerator.getFeatureVector(score, description, fragment, encoding);

				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Save training feature vectors
		createFile(trainingFeatureVectors,Constants.FOLDERPATH_CLASSIFIER+outputFilesPath, Constants.FILENAME_TRAINING);
	}
	
	/*public KnowledgeBase loadKnowledgeBase(String filepath, String filespath_fragments, String filespath_descriptions) {
		
		KnowledgeBase knowledge_base = new KnowledgeBase();
		//knowledge_base.setFragments(loadFragments(filespath_fragments));
		knowledge_base.setDescriptions(loadDescriptions(filespath_descriptions));
		loadScoresAndFragments(knowledge_base, filepath);
		
		return knowledge_base;
		
	}*/

	/*private ArrayList<Element> loadScoresAndFragments(KnowledgeBase knowledge_base, String filepath) {
		ArrayList<Element> elements = new ArrayList<Element>();
		
		try {
			File fXmlFile = new File(project.getLocation().toString()+"/"+filepath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			
			doc.getDocumentElement().normalize();
	
			NodeList nList = doc.getElementsByTagName(Constants.TAG_FRAGMENT);
	
			for (int i = 0; i < nList.getLength(); i++) {
	
				Node nNode = nList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	
					Element eElement = (Element) nNode;
			
					String id_fragment = eElement.getElementsByTagName(Constants.TAG_FILENAME).item(0).getTextContent();
					String idModel = eElement.getElementsByTagName(Constants.TAG_MODEL).item(0).getTextContent();
					int id_description = Integer.parseInt(eElement.getElementsByTagName(Constants.TAG_ID_QUERY).item(0).getTextContent());
					float score = Float.parseFloat(eElement.getElementsByTagName(Constants.TAG_SCORE).item(0).getTextContent());
					knowledge_base.setKnowledgeElement(id_description, id_fragment, idModel, score);
				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return elements;
	}*/

	public Fragment loadFragment(String id, String filespathFragment, String filespathModel) {
		
		try {
			IFile fileFragment = project.getFile(filespathFragment);
			IFile fileModel = project.getFile(filespathModel);
			Fragment fragment = new Fragment(id, loadFragment(fileFragment), loadFragment(fileModel));
			return fragment;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public EObject loadFragment(String id, String filespathFragment) {
		
		try {
			IFile fileFragment = project.getFile(filespathFragment+id);
			return loadFragment(fileFragment);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private EObject loadFragment(IFile f) {
	
		if (f.exists()) {
	
			String filename = f.getFullPath().toString();
			ResourceSet resSet = new ResourceSetImpl();
			URI uri = URI.createFileURI(filename);
			Resource resource = resSet.getResource(uri, true);
			EObject myModel = (EObject) resource.getContents().get(0);
			return myModel;
		}
		return null;
	
	}

	private HashMap<Integer, Description> loadDescriptions(String filespath) {
		
		HashMap<Integer, Description> descriptions = new HashMap<Integer, Description>();
		try {
			IFolder modelsfolder = project.getFolder(filespath);
			for (IResource res : modelsfolder.members()) 
			{
				if (res instanceof IFile) 
				{
					IFile file = (IFile) res;
					Description description = loadDescription(file);
					if (description != null)
					{
						descriptions.put(description.getId(), description); 
					}
				}
			}
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return descriptions;
	}

	private Description loadDescription(IFile file) throws CoreException {
		if (file.getFileExtension().equals(Constants.EXTENSION_TXT)) {
			if (file.exists()) {
				InputStream stream = file.getContents();
				String description = new BufferedReader(new InputStreamReader(stream)).lines()
						.collect(Collectors.joining("\n"));
				String id_description = file.getName().substring(0, file.getName().lastIndexOf('.'));
				try {
					return new Description(Integer.parseInt(id_description), description);
			    } catch (NumberFormatException e) {
			    	return new Description(0, description);
			    }
				
			}
		}
		return null;
	}

	
	public HashMap<String, TestCase> loadTestCases()
	{
		
		HashMap<String, TestCase> test_cases = new HashMap<String, TestCase>();
		try {
			IFolder modelsfolder = project.getFolder(Constants.FOLDERPATH_TEST_CASES);
			for (IResource res : modelsfolder.members()) {
				if (res instanceof IFolder) {
					IFolder test_case_folder = (IFolder) res;
					
					String id_model = test_case_folder.getName();
					EObject model = null;
					EObject oracle = null;
					String description = null;
					ArrayList<String> testingFragments = new ArrayList<String>();
					
					for (IResource res2 : test_case_folder.members())
					{	
						if (res2 instanceof IFile)
						{
							IFile file = (IFile) res2;
							if (file.getName().equals(Constants.FILENAME_TEST_CASE_MODEL))
							{
								model = loadFragment(file);
							}
							else if (file.getName().equals(Constants.FILENAME_TEST_CASE_ORACLE))
							{
								oracle = loadFragment(file);
							}
							else if (file.getName().equals(Constants.FILENAME_TEST_CASE_DESCRIPTION))
							{
								description = loadDescription(file).getDescrition();
							}
						}
						else if (res2 instanceof IFolder)
						{
							if(((IFolder)res2).getName().equals("fragments"))
							{
								for (IResource res3 : ((IFolder)res2).members())
								{
								  testingFragments.add(((IFile) res3).getName());
								}
							}
						}
					}
					if(model!= null && description!=null)
					{
						test_cases.put(id_model, new TestCase(id_model, model, oracle, description, testingFragments));
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return test_cases;
	}
	
	public HashMap<String, TestCase> loadDataset()
	{
		
		HashMap<String, TestCase> test_cases = new HashMap<String, TestCase>();
		try {
			IFolder modelsfolder = project.getFolder(Constants.FOLDERPATH_DATASET);
			for (IResource res : modelsfolder.members()) {
				if (res instanceof IFolder) {
					IFolder test_case_folder = (IFolder) res;
					
					String id_model = test_case_folder.getName();
					EObject model = null;
					EObject oracle = null;
					String description = null;
					
					for (IResource res2 : test_case_folder.members())
					{	
						if (res2 instanceof IFile)
						{
							IFile file = (IFile) res2;
							if (file.getName().equals(Constants.FILENAME_TEST_CASE_MODEL))
							{
								model = loadFragment(file);
							}
							else if (file.getName().equals(Constants.FILENAME_TEST_CASE_ORACLE))
							{
								oracle = loadFragment(file);
							}
							else if (file.getName().equals(Constants.FILENAME_TEST_CASE_DESCRIPTION))
							{
								description = loadDescription(file).getDescrition();
							}
						}
					}
					if(model!= null && description!=null)
					{
						test_cases.put(id_model, new TestCase(id_model, model, oracle, description, null));
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return test_cases;
	}

  public void loadResults(TestCase testCase, String filepath) 
  {	
		String line = "";
	    FileReader file_ranking;
		try {
			
			file_ranking = new FileReader(project.getLocation()+filepath+Constants.FILENAME_RANKING);
			BufferedReader buffered_ranking = new BufferedReader(file_ranking);
		    
			float score = -1;
			int idx = 0;
			while((line = buffered_ranking.readLine())!=null) {
		         String[] fields_ranking = line.split("\t");
		        	         
		         if (fields_ranking.length>1)
		         {
		        		score = Float.parseFloat(fields_ranking[Constants.RANKING_FILE_SCORE_POSITION]);
		        		testCase.setScore(idx,score);		
		         }
		         idx++;
		    }
		    buffered_ranking.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/*
	public void createFeatureVectorFiles(Map<String, ArrayList<Integer>> files) {
		
		for (Entry<String, ArrayList<Integer>> entry : files.entrySet())
		{
			createFeatureVectorFiles(entry.getKey(), entry.getValue());
		}
		
	}

	public void createFeatureVectorFiles(KnowledgeBase knowledge_base, int kfolds){//, ArrayList<Boolean>features) {
		IFolder folder = project.getFolder("temps");
		try {
			if (!folder.exists())
			{
					folder.create(IResource.NONE, true, null);
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<ArrayList<Element>> knowledgeBase_in_Kfolds = new ArrayList<ArrayList<Element>>();//= knowledge_base.divideKnowledgeBaseIntoKFolds(kfolds);
		
		FeatureVectorGenerator feature_vector = new FeatureVectorGenerator();
		
		ArrayList<Element> training;
		ArrayList<Element> testing;
		for (int i=1; i<= kfolds; i++)
		{
			training = new ArrayList<Element>();
			testing = new ArrayList<Element>();
			
			for (int n=0; n< knowledgeBase_in_Kfolds.size();n++)
			{
				if (i==n+1){
					testing.addAll(knowledgeBase_in_Kfolds.get(n));
				}
				else
				{
					training.addAll(knowledgeBase_in_Kfolds.get(n));
				}
			}
			
		}
	}

	*/

	public float retrieveScores()
	{
		float fMeasure = 0;
		
		for (int n=0; n<Constants.KFLOD; n++)
		{
			Result results = new Result(); 
						
			FileReader fileTesting;
		    FileReader fileRanking;
		    String lineTesting = "";
		    String lineRanking = "";
		    
			try {
				fileTesting = new FileReader(project.getLocation()+Constants.FOLDER_CROSS_VALIDATION+n+"/"+Constants.FILENAME_TESTING);
				BufferedReader bufferedTesting = new BufferedReader(fileTesting);
				fileRanking = new FileReader(project.getLocation()+Constants.FOLDER_CROSS_VALIDATION+n+"/"+Constants.FILENAME_RANKING);
				BufferedReader bufferedRanking = new BufferedReader(fileRanking);
				while((lineTesting = bufferedTesting.readLine())!=null) {
					if(lineTesting!="")
					{
						if ((lineRanking = bufferedRanking.readLine())!=null) {
							//From Testing file: idDescription, idFragment, realScore
							String[] fields = lineTesting.split(" ");
					    	String idDescription = fields[1].split(":")[1];
					    	float realScore =  Float.parseFloat(fields[0]);
					    	String idFragment = lineTesting.split("#")[1];
					    	
					    	//Form Ranking file: calculatedScore
					    	String[] fieldsRanking = lineRanking.split("\t");
					        float calculatedScore = Float.parseFloat(fieldsRanking[2]);
					        
					        results.addResult(idDescription, idFragment, realScore, calculatedScore);
						}
					}
				}
				
				bufferedRanking.close();
				bufferedTesting.close();
				
			}catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			fMeasure = fMeasure + results.compareResults();
		}
		return fMeasure / Constants.KFLOD;
	}

  public void prepareDatasetForCrossValidation(String trainingFilePath)
  {
	  int testingIndex = 0;
	  int trainingIndex = 1;
	  
	  HashMap<Integer, String[]> files = new HashMap<Integer, String[]>();
	  for(int n = 0; n<Constants.KFLOD; n++)
	  {
		  files.put(n, new String[]{"",""});
	  }
		String line = "";
	    FileReader datasetFile;
	    int index = 0;
		
	    try {
			datasetFile = new FileReader(project.getLocation()+trainingFilePath);
			BufferedReader buffered = new BufferedReader(datasetFile);
		    while((line = buffered.readLine())!=null) {
		    	
		    	for(int n=0; n<Constants.KFLOD; n++)
		    	{
		    		if(n==index) files.get(n)[testingIndex]= files.get(n)[testingIndex]+line+"\n";
		    		else files.get(n)[trainingIndex]= files.get(n)[trainingIndex]+line+"\n";
		    	}
		    			    	
		    	index++;
		    	if(index>=Constants.KFLOD)
		    	{
		    		index = 0;
		    	}
		    }
		    
		    for (Entry<Integer, String[]> file : files.entrySet())
		    {
		    	createFile(file.getValue()[trainingIndex], Constants.FOLDER_CROSS_VALIDATION+file.getKey()+"/",Constants.FILENAME_TRAINING);
		    	createFile(file.getValue()[testingIndex], Constants.FOLDER_CROSS_VALIDATION+file.getKey()+"/",Constants.FILENAME_TESTING);
		    }
		    buffered.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
  
  public void prepareDatasetForTuning(String filepath)
  {
	  //Prepare training and testing files
	  prepareDatasetForCrossValidation(filepath+Constants.FILENAME_TRAINING);
	  
	  //Prepare features file
	  IFile file = project.getFile(filepath+Constants.FILENAME_FEATURES);
	  if (file.getFileExtension().equals(Constants.EXTENSION_TXT)) {
			if (file.exists()) {
				try {
					InputStream stream = file.getContents();
					String features = new BufferedReader(new InputStreamReader(stream)).lines()
							.collect(Collectors.joining("\n"));
					createFile(features, Constants.FOLDER_CROSS_VALIDATION, Constants.FILENAME_FEATURES);
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	  }
  }
  
	
	
}
