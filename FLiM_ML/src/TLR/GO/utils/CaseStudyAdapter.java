package TLR.GO.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.nd4j.linalg.api.ops.impl.transforms.IsFinite;

import TLR.GO.io.ProjectHelper;
import TrainControlDSL.Equipment;
import TrainControlDSL.EquipmentGroup;
import TrainControlDSL.Fail;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainModule;
import TrainControlDSL.TrainUnit;
import TrainControlDSL.impl.TrainControlDSLFactoryImpl;
import data.Description;
import data.Fragment;
import data.TestCase;
import evaluation.ConfusionMatrix;

public class CaseStudyAdapter {

	
	//private static final String[] namesModelFragmentsSets = {"De00-25","De25-50","De50-75","De75-100","De00-100","Di00-25","Di25-50","Di50-75","Di75-100","Di00-100","Mu0-0","Mu1-1","Mu0-1"};
	
	private static final String[] namesModelFragmentsSets = {"De00-50","De50-100","De00-100","Di00-50","Di50-100","Di00-100","Mu0-0","Mu1-1","Mu0-1"};
	public static void generateTestCases(HashMap<String, TestCase> dataset, String filepathTestCases, ProjectHelper project)
	{	
		int numMaxModelFragmentsInSets = 100;
		
		HashMap<String,String> descriptions = new HashMap<String, String>();
		HashMap<String, EObject> models = new HashMap<String, EObject>();
		HashMap<String, Fragment> oracles = new HashMap<String, Fragment>();
		
		for (Entry<String, TestCase> entry : dataset.entrySet())
		{
			TestCase infoForProductModel = entry.getValue();
			//Get descriptions
			if(!descriptions.containsValue(infoForProductModel.getDescription()))
			{
				descriptions.put(infoForProductModel.getId(), infoForProductModel.getDescription());
			}
			
			//Get models
			models.put(infoForProductModel.getId(), infoForProductModel.getModel());
			
			//Get oracles
			oracles.put(infoForProductModel.getId(), infoForProductModel.getOracle());
		}
		
		ArrayList<ArrayList<TestCase>> testCases = new ArrayList<ArrayList<TestCase>>();
		//For all the product models in the dataset
		for (Entry<String, EObject> model : models.entrySet())
		{
			//For all the feature descriptions in the dataset
			for (Entry<String, String> description : descriptions.entrySet())
			{
				ArrayList<TestCase> testCaseForModelDescription = new ArrayList<TestCase>();
				boolean regenerate = false;
				//Create 13 test cases
				for (int i=0; i<namesModelFragmentsSets.length; i++)
				{	
					String idTestCase = "M"+model.getKey()+"_FD"+description.getKey()+"_"+namesModelFragmentsSets[i];
					String filepath = Constants.FOLDERPATH_TEST_CASES+idTestCase+"/";
					//Save the product model
					//project.createFile(filepath+Constants.FILENAME_TEST_CASE_MODEL, model.getValue());
					//Save feature description
					//project.createFile(description.getValue(), filepath, Constants.FILENAME_TEST_CASE_DESCRIPTION);
					//Save oracle
					//project.createFile(filepath+Constants.FILENAME_TEST_CASE_ORACLE, oracles.get(description.getKey()).getFragment());
					
					testCaseForModelDescription.add(new TestCase(idTestCase, model.getValue(), oracles.get(description.getKey()).getFragment(), description.getValue()));
					
					IFolder modelsfolder = project.project.getFolder(Constants.FOLDERPATH_TEST_CASES+idTestCase+"/fragments/");
					try {
						if(modelsfolder.members().length<numMaxModelFragmentsInSets)
						{
							System.out.println("error length:"+idTestCase);
							regenerate = true;
						}
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				if (regenerate)
					{
					generateFragmentsFromModel(project, numMaxModelFragmentsInSets, testCaseForModelDescription, oracles);
					}

					
				testCases.add(testCaseForModelDescription);
			}
		}
		
		//For each test case new model fragments taking into account density, multiplicity, and dispersion
		/*for (ArrayList<TestCase> testCasesForModelDescription: testCases)
		{
			generateFragmentsFromModel(project, numMaxModelFragmentsInSets, testCasesForModelDescription, oracles);
		}*/
	}

	private static void generateFragmentsFromModel(ProjectHelper project, int numMaxModelFragmentsInSets, ArrayList<TestCase> testCases, HashMap<String, Fragment> oracles) 
	{
		int idFragment = 0;
		TestCase oneTestCase = null;
		for (TestCase testCase : testCases)
		{
			idFragment=0;
			//Save all the oracles as fragments
			for (Entry<String, Fragment> oracle: oracles.entrySet())
			{
				String filepath = Constants.FOLDERPATH_TEST_CASES+testCase.getId()+"/fragments/";
				project.createFile(filepath+Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS, oracle.getValue().getFragment());
				idFragment++;
			}
			oneTestCase = testCase;
		}
		
		int idFragment_de0050 = idFragment;
		int idFragment_de50100 = idFragment;
		int idFragment_de00100 = idFragment;
		int idFragment_di0050 = idFragment;
		int idFragment_di50100 = idFragment;
		int idFragment_di00100 = idFragment;
		int idFragment_mu00 = idFragment;
		int idFragment_mu11 = idFragment;
		int idFragment_mu01 = idFragment;

		String commonId = oneTestCase.getId().split("_")[0]+"_"+oneTestCase.getId().split("_")[1]+"_";
		while(idFragment_de0050<numMaxModelFragmentsInSets || idFragment_de50100<numMaxModelFragmentsInSets || idFragment_de00100<numMaxModelFragmentsInSets 
				|| idFragment_di0050<numMaxModelFragmentsInSets || idFragment_di50100<numMaxModelFragmentsInSets || idFragment_di00100<numMaxModelFragmentsInSets
				|| idFragment_mu00<numMaxModelFragmentsInSets || idFragment_mu11<numMaxModelFragmentsInSets || idFragment_mu01<numMaxModelFragmentsInSets)
		{
			Random rand = new Random();	
			int percentage = rand.nextInt(9)+1;
			
			Fragment fragment = generateRandomFragment(Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS, (TrainUnit)oneTestCase.getModel(),percentage);
			//This indirection verifies that the model fragment is not empty (this problem is solved in FLiMEA version using indexes)
			project.createFile("temp/"+fragment.getId(), fragment.getFragment());
			fragment = new Fragment(fragment.getId(), project.loadFragment(fragment.getId(), "temp/"), oneTestCase.getModel());

			//Density
			if (fragment.getDensity()<=50 && idFragment_de0050<numMaxModelFragmentsInSets)
			{
				String id = Constants.NAME_FRAGMENT+idFragment_de0050+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[0]+"/fragments/"+id, fragment.getFragment());
				idFragment_de0050++;
			}
			if (fragment.getDensity()>=50 && idFragment_de50100<numMaxModelFragmentsInSets)
			{
				String id = Constants.NAME_FRAGMENT+idFragment_de50100+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[1]+"/fragments/"+id, fragment.getFragment());
				idFragment_de50100++;
			}
			if (idFragment_de00100<numMaxModelFragmentsInSets)
			{
				String id = Constants.NAME_FRAGMENT+idFragment_de00100+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[2]+"/fragments/"+id, fragment.getFragment());
				idFragment_de00100++;
			}
			
			//Dispersion
			if (fragment.getDispersion()<=0.5 && idFragment_di0050<numMaxModelFragmentsInSets)
			{
				String id = Constants.NAME_FRAGMENT+idFragment_di0050+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[3]+"/fragments/"+id, fragment.getFragment());
				idFragment_di0050++;
			}
			if (fragment.getDispersion()>=0.5 && idFragment_di50100<numMaxModelFragmentsInSets)
			{
				String id = Constants.NAME_FRAGMENT+idFragment_di50100+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[4]+"/fragments/"+id, fragment.getFragment());
				idFragment_di50100++;
			}
			if (idFragment_di00100<numMaxModelFragmentsInSets)
			{
				String id = Constants.NAME_FRAGMENT+idFragment_di00100+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[5]+"/fragments/"+id, fragment.getFragment());
				idFragment_di00100++;
			}

			//Multiplicity
			if (fragment.getMultiplicity()<1 && idFragment_mu00<numMaxModelFragmentsInSets)
			{
				String id = Constants.NAME_FRAGMENT+idFragment_mu00+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[6]+"/fragments/"+id, fragment.getFragment());
				idFragment_mu00++;
			}
			if ((fragment.getMultiplicity()>=1 && idFragment_mu11<numMaxModelFragmentsInSets))
			{
				String id = Constants.NAME_FRAGMENT+idFragment_mu11+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[7]+"/fragments/"+id, fragment.getFragment());
				idFragment_mu11++;
			}
			if (idFragment_mu01<numMaxModelFragmentsInSets)
			{
				String id = Constants.NAME_FRAGMENT+idFragment_mu01+"."+Constants.EXTENSION_MODELS;
				project.createFile(Constants.FOLDERPATH_TEST_CASES+commonId+namesModelFragmentsSets[8]+"/fragments/"+id, fragment.getFragment());
				idFragment_mu01++;
			}
		}
	}
	
	private static void generateFragmentsFromModel(ProjectHelper project, int numMaxModelFragmentsInSets, int idFragment, TestCase testCase) 
	{
		String measurement = testCase.getId().split("_")[2];
		String idMeasurement = measurement.substring(0, 2);
		int minMeasurement = Integer.parseInt(measurement.substring(2).split("-")[0]);
		int maxMeasurement = Integer.parseInt(measurement.substring(2).split("-")[1]);
		
		Fragment fragment;
		while(idFragment<numMaxModelFragmentsInSets)
		{
			int percentage = 0;
			if(idMeasurement.equals("De"))
			{
				Random rand = new Random();	
				/*if (maxMeasurement==25)	 percentage = rand.nextInt(2)+1;
				else if (maxMeasurement==50) percentage = rand.nextInt(5)+1;
				else if (maxMeasurement==75) percentage = rand.nextInt(7)+1;
				else percentage = rand.nextInt(10)+1;*/
				if (maxMeasurement==50)	 percentage = rand.nextInt(5)+1;
				else if (maxMeasurement==100) percentage = rand.nextInt(10)+1;
			}
			else 
			{
				Random rand = new Random();
				percentage = rand.nextInt(5)+1;
			}
			
			fragment = generateRandomFragment(Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS, (TrainUnit)testCase.getModel(),percentage);
		
			//This indirection verifies that the model fragment is not empty (this problem is solved in FLiMEA version using indexes)
			project.createFile("temp/"+fragment.getId(), fragment.getFragment());
			fragment = new Fragment(fragment.getId(), project.loadFragment(fragment.getId(), "temp/"), testCase.getModel());

			if (idMeasurement.equals("De"))
			{
				float density = fragment.getDensity();
				if (density>=minMeasurement && density<=maxMeasurement)
				{
					String id = Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS;
					project.createFile(Constants.FOLDERPATH_TEST_CASES+testCase.getId()+"/fragments/"+id, fragment.getFragment());
					idFragment++;
				}
			}
			else if (idMeasurement.equals("Di"))
			{
				float dispersion = fragment.getDispersion()*100;
				if (dispersion>=minMeasurement && dispersion<=maxMeasurement)
				{
					String id = Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS;
					project.createFile(Constants.FOLDERPATH_TEST_CASES+testCase.getId()+"/fragments/"+id, fragment.getFragment());
					idFragment++;
				}
			}
			else if (idMeasurement.equals("Mu"))
			{
				float multiplicity = fragment.getMultiplicity()-1;
				if (multiplicity>=minMeasurement && multiplicity<=maxMeasurement)
				{
					String id = Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS;
					project.createFile(Constants.FOLDERPATH_TEST_CASES+testCase.getId()+"/fragments/"+id, fragment.getFragment());
					idFragment++;
				}
			}
		}
	}

	public static void generateKnowledgeBase(HashMap<String, TestCase> dataset, String filepathKnowledgeBase, ProjectHelper project)
	{	
		int[] numSamplesForScore = {0,0,0,0,0,0,0,0};
		int numMaxSamplesForScore = 200;
		int numMaxSamplesForProductModelAndScore = 50;
		int idFragment = 0;
		
		String knowledgeBaseFileDescription = "<?xml version=\"1.0\" encoding=\"utf-8\"?> \n<samples>";

		//Launch Test Cases for Training
		for (Entry<String, TestCase> entry : dataset.entrySet())
		{
			if (isCompleteKB(numMaxSamplesForScore,numSamplesForScore)) break;
			
			TestCase testCase = entry.getValue();
			int[] numSamplesForProductModel = {0,0,0,0,0,0,0,0};
			
			//Save description
			Description description = new Description(Integer.parseInt(testCase.getId()), testCase.getDescription());
			project.createFile(description.getDescrition(), filepathKnowledgeBase+"descriptions/",description.getId()+"."+Constants.EXTENSION_TXT);
			project.createFile(filepathKnowledgeBase+"models/"+testCase.getId()+"."+Constants.EXTENSION_MODELS, testCase.getModel());	
			
			//save oracle as fragment
		    project.createFile(filepathKnowledgeBase+"fragments/"+Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS, testCase.getOracle().getFragment());
			knowledgeBaseFileDescription = knowledgeBaseFileDescription +"<fragment><filename>"+Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS+"</filename><model>"+testCase.getId()+"."+Constants.EXTENSION_MODELS+"</model><query_id>"+description.getId()+"</query_id><score>4</score></fragment>\n";
			numSamplesForScore[7]++;
			numSamplesForProductModel[7]++;
			idFragment++;
			
			//20%
			Random rand = new Random();
			int percentage = rand.nextInt(1)+1;
			
			//Add fragments
			int index = 1;
			int numEvaluatedFragments = 0;
			Fragment fragment;
			ConfusionMatrix confusion_matrix;
			while(index < numMaxSamplesForProductModelAndScore*8 && numEvaluatedFragments<10000)
			{
				fragment = generateRandomFragment(Constants.NAME_FRAGMENT+idFragment+"."+Constants.EXTENSION_MODELS, (TrainUnit)testCase.getModel(), percentage);
				
				//This indirection verifies that the model fragment is not empty (this problem is solved in FLiMEA version using indexes)
				project.createFile(filepathKnowledgeBase+"fragments/"+fragment.getId(), fragment.getFragment());
				fragment = new Fragment(fragment.getId(), project.loadFragment(fragment.getId(), filepathKnowledgeBase+"fragments/"), testCase.getModel());
				if (fragment.getNumberOfFragmentElements()>=1)
				{
					
					confusion_matrix = new ConfusionMatrix(testCase.getId(),testCase.getOracle().getFragment(),fragment.getFragment(),testCase.getModel());
					float score = confusion_matrix.getF_Measure()*4/100;
					if(isValidFragmentForBalance(numSamplesForScore, score, numMaxSamplesForScore, numSamplesForProductModel, numMaxSamplesForProductModelAndScore))
					{
						//Save triplet
						knowledgeBaseFileDescription = knowledgeBaseFileDescription +"<fragment><filename>"+fragment.getId()+"</filename><model>"+testCase.getId()+"."+Constants.EXTENSION_MODELS+"</model><query_id>"+description.getId()+"</query_id><score>"+score+"</score></fragment>\n";
						idFragment++;
						index++;
					}					
				}
				numEvaluatedFragments++;
			}
			System.out.println(Arrays.toString(numSamplesForScore));
		}
		
		knowledgeBaseFileDescription = knowledgeBaseFileDescription +"</samples>";
		//Save description of Knowledge Base with scores
		project.createFile(knowledgeBaseFileDescription, filepathKnowledgeBase,Constants.FILENAME_KNOWLEDGE_BASE);//"knowledge_base"+testCase.getId()+"."+Constants.EXTENSION_XML);
	}
	
	private static boolean isCompleteKB(int numMaxSamplesForScore, int[] numSamplesForScore) {
		
		for(int numSampleForScore : numSamplesForScore)
		{
			if (numSampleForScore==numMaxSamplesForScore) return true;
		}
		return false;
	}

	private static boolean isValidFragmentForBalance(int[] numSamplesForScore,float score, int numMaxSamplesForScore, int[] numSamplesForProductModel, int numMaxSamplesForProductModelAndScore)
	{
		if(score>0 && score<=0.5 && numSamplesForScore[0]<numMaxSamplesForScore &&  numSamplesForProductModel[0]<numMaxSamplesForProductModelAndScore)
		{
			numSamplesForScore[0]++;
			numSamplesForProductModel[0]++;
		}
		else if (score>0.5 && score<=1 && numSamplesForScore[1]<numMaxSamplesForScore &&  numSamplesForProductModel[1]<numMaxSamplesForProductModelAndScore)
		{
			numSamplesForScore[1]++;
			numSamplesForProductModel[1]++;
		}
		else if (score>1 && score<=1.5 && numSamplesForScore[2]<numMaxSamplesForScore &&  numSamplesForProductModel[2]<numMaxSamplesForProductModelAndScore)
		{
			numSamplesForScore[2]++;
			numSamplesForProductModel[2]++;
		}
		else if (score>1.5 && score<=2 && numSamplesForScore[3]<numMaxSamplesForScore &&  numSamplesForProductModel[3]<numMaxSamplesForProductModelAndScore)
		{
			numSamplesForScore[3]++;
			numSamplesForProductModel[3]++;
		}
		else if (score>2 && score<=2.5 && numSamplesForScore[4]<numMaxSamplesForScore &&  numSamplesForProductModel[4]<numMaxSamplesForProductModelAndScore)
		{
			numSamplesForScore[4]++;
			numSamplesForProductModel[4]++;
		}
		else if (score>2.5 && score<=3 && numSamplesForScore[5]<numMaxSamplesForScore &&  numSamplesForProductModel[5]<numMaxSamplesForProductModelAndScore)
		{
			numSamplesForScore[5]++;
			numSamplesForProductModel[5]++;
		}
		else if (score>3 && score<=3.5 && numSamplesForScore[6]<numMaxSamplesForScore &&  numSamplesForProductModel[6]<numMaxSamplesForProductModelAndScore)
		{
			numSamplesForScore[6]++;
			numSamplesForProductModel[6]++;
		}
		else if (score>3.5 && score<=4 && numSamplesForScore[7]<numMaxSamplesForScore &&  numSamplesForProductModel[7]<numMaxSamplesForProductModelAndScore)
		{
			numSamplesForScore[7]++;
			numSamplesForProductModel[7]++;
		}
		else return false;
		
		return true;
	}
	
	private static Fragment generateRandomFragment (String id, TrainUnit model, int percentage)
	{		
		TrainControlDSLFactoryImpl factory = new TrainControlDSLFactoryImpl();
		TrainUnit targetTrain = factory.createTrainUnit();
				
		for(Rule rule:model.getHasRules())
		{
			if (addEObject(percentage)) targetTrain.getHasRules().add((Rule)copyEObject(rule));
		}
		for(Equipment equipment: model.getHasEquipments())
		{
			if(addEObject(percentage)) targetTrain.getHasEquipments().add((Equipment)copyEObject(equipment));
		}
		for(EquipmentGroup group: model.getHasEquipmentGroups())
		{
			if(addEObject(percentage)) targetTrain.getHasEquipmentGroups().add((EquipmentGroup) copyEObject(group));
		}
		for(Fail fail: model.getHasFails())
		{
			if(addEObject(percentage)) targetTrain.getHasFails().add((Fail)copyEObject(fail));
		}
		for(TrainModule trainModule: model.getHasTrainModules())
		{
			if(addEObject(percentage)) targetTrain.getHasTrainModules().add((TrainModule) copyEObject(trainModule));
		}
		targetTrain.setName(model.getName());
		return new Fragment(id,targetTrain,model);
	}
	
	private static EObject copyEObject(EObject oldObject)
	{
        EObject result = EcoreUtil.copy(oldObject);
        return result;
	}
	
	private static boolean addEObject(int percentage)
	{
		Random rand = new Random();
		int max = 11;
		int min = 0;
		return(((rand.nextInt((max - min) + 1) + min))<percentage);
	}
}
