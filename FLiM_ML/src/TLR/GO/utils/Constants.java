package TLR.GO.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Constants {

	//PLUGIN AND PROJECT
	public static final String WORSPACE_PLUGIN = "workspace/FLiM_ML";
	public static final String PROJECT_NAME = "TRAINCONTROL";
	public static final String PROJECT_DATA = "data";
	
	//EXTENSIONS
	public static final String EXTENSION_MODELS = "traincontroldsl";	
	public static final String EXTENSION_TXT = "txt";
	public static final String EXTENSION_XML = "xml";
	
	//KNOWLEDGE BASE
	//public static final String FOLDER_KB = "/knowledgeBase/";
	public static final String FOLDER_KNOWLEDGE_BASE = "/knowledgeBase/";
	public static final String FOLDER_KNOWLEDGE_BASE_FRAGMENTS = "fragments/";
	public static final String FOLDER_KNOWLEDGE_BASE_MODELS = "models/";
	public static final String FOLDER_KNOWLEDGE_BASE_DESCRIPTIONS = "descriptions/";	
	public static final String FILENAME_KNOWLEDGE_BASE = "knowledge_base." + EXTENSION_XML;

	    //Read Knowledge Base File
		public static final String TAG_MODEL = "model";
		public static final String TAG_FRAGMENT = "fragment";
		public static final String TAG_FILENAME = "filename";
		public static final String TAG_ID_QUERY = "query_id";
		public static final String TAG_SCORE = "score";
		
	
	//DATASET
	public static final String FOLDERPATH_DATASET = "/dataset/";
	public static final String FILENAME_TEST_CASE_MODEL = "model" + "."+EXTENSION_MODELS;
	public static final String FILENAME_TEST_CASE_ORACLE ="oracle"+"."+EXTENSION_MODELS;
	public static final String FILENAME_TEST_CASE_DESCRIPTION = "requirement"+"."+EXTENSION_TXT;
	public static final String NAME_FRAGMENT = "fragment_";
	
	//TEST CASES
	public static final String FOLDERPATH_TEST_CASES = "/testCases/";
	
	//CLASSIFIER
	public static final String FOLDERPATH_CLASSIFIER = "/classifier/";
	public static final String FILENAME_TRAINING = "training."+EXTENSION_TXT;
	public static final String FILEPATH_TRAINING = FOLDERPATH_CLASSIFIER+FILENAME_TRAINING;
	public static final String FILENAME_FEATURES = "features."+EXTENSION_TXT;
	public static final String FILEPATH_FEATURES = FOLDERPATH_CLASSIFIER+FILENAME_FEATURES;
	public static final String FILENAME_TESTING = "testing."+EXTENSION_TXT;
	public static final String FILEPATH_TESTING = FOLDERPATH_CLASSIFIER+FILENAME_TESTING;
	public static final String FILENAME_CLASSIFIER = "classifier."+EXTENSION_TXT;
	public static final String FILEPATH_CLASSIFIER = FOLDERPATH_CLASSIFIER+"classifier."+EXTENSION_TXT;
	public static final String FILENAME_RANKING = "ranking"+"."+EXTENSION_TXT;
	public static final String FILEPATH_RANKING = FOLDERPATH_CLASSIFIER + FILENAME_RANKING;
	public static final String FILENAME_TUNING = "tuning."+Constants.EXTENSION_TXT;
	
	public static final float MIN_SCORE = 0;
		//CROSS-VALIDATION USING KFOLD
		public static final int KFLOD = 4;
		public static final String FOLDER_CROSS_VALIDATION = "/cross_validation/";
		public static final String FILENAME_CROSS_VALIDATION_TRAINING_FILES = "train_kfold_";
		public static final String FILENAME_CROSS_VALIDATION_TESTING_FILES = "test_kfold_";
		public static final String FILENAME_CROSS_VALIDATION_FEATURES_FILE = "features."+EXTENSION_TXT;
		
		//Read Ranking File
		public static final int RANKING_FILE_DESCRIPTION_POSITION = 0;
		public static final int RANKING_FILE_SCORE_POSITION = 2;	
	
	
	//Test Save Results
	public static final String FOLDER_RESULTS = "results/";
	public static final String FOLDER_REPORTS = "reports/";
	
	//SET UP SEARCH RANDOM
	private static final int INDIVIDUALS_IN_POPULATION = 120;
	private static final int NUM_ITERATIONS = 200;
	public static final int NUM_TOTAL_INDIVIDUALS_TO_TEST = INDIVIDUALS_IN_POPULATION * NUM_ITERATIONS;
	public static final double PERCENTAGE_ELEMENTS_INITIAL_INDIVIDUALS = 0.2;
	
	public static final DecimalFormat decimalFormat = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));
}
