package TLR.GO.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import FLiM_ML.Encoding.Encoding;
import data.Description;
import data.Fragment;

public class FeatureVectorGenerator {

	private static DecimalFormat decimal_format_to_features = new DecimalFormat();
	private static DecimalFormat decimal_format_to_scores = new DecimalFormat();

	static {
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');

		decimal_format_to_features.setMinimumFractionDigits(6);
		decimal_format_to_features.setDecimalFormatSymbols(otherSymbols);

		decimal_format_to_scores.setMinimumFractionDigits(1);
		decimal_format_to_scores.setDecimalFormatSymbols(otherSymbols);
	}

	public String getFeatureVector(float score, Description description, Fragment fragment, Encoding encoding) {

		// Score
		String feature_vector = decimal_format_to_scores.format(score) + " ";

		// ID feature vector
		int index = 1;
		feature_vector = feature_vector + "qid:" + description.getId() + " " ;

		// Features-Values (For: Description & Fragment) feature vector
		ArrayList<Float> encoding_description_fragment = encoding.encodeDescriptionFragment(description.getDescrition(), fragment.getFragment());
		feature_vector = feature_vector + getFeatureVector(index, encoding_description_fragment);
		
		// Comments
		feature_vector = feature_vector + "# " + fragment.getId() + "\n";
		
		return feature_vector;

	}
	
	private String getFeatureVector(int index, ArrayList<Float> features)
	{
		String feature_vector = "";
		
		for(Float value: features)
		{
			feature_vector = feature_vector + index+":"+decimal_format_to_features.format(value)+" ";
			index++;
		}
		
		return feature_vector;
	}
	
}
