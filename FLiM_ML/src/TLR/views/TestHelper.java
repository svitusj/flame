package TLR.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.deeplearning4j.eval.curves.PrecisionRecallCurve.Confusion;
import org.eclipse.emf.ecore.EObject;

import FLiM_ML.FeatureSelection;
import FLiM_ML.Encoding.Encoding;
import MLTechniques.MLTechnique;
import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.CaseStudyAdapter;
import TLR.GO.utils.Constants;
import TLR.GO.utils.FeatureVectorGenerator;
import data.Description;
import data.Fragment;
import data.TestCase;
import evaluation.ConfusionMatrix;

public class TestHelper {

	
	public static void generateDataExperiment()
	{
		ProjectHelper projectHelper = ProjectHelper.getInstance();
		HashMap<String, TestCase> dataset = projectHelper.loadDataset();
		
		//CaseStudyAdapter.generateKnowledgeBase(dataset, Constants.FOLDER_KB, projectHelper);
		
		CaseStudyAdapter.generateTestCases(dataset, Constants.FOLDERPATH_TEST_CASES, projectHelper);
	}
	
	public static void runExperiment(Encoding encoding, MLTechnique mlTechnique)
	{
		ProjectHelper projectHelper = ProjectHelper.getInstance();
		
		//Encode knowledge base
		String outputFilesPath = encoding.getClass().getSimpleName()+"_"+mlTechnique.getClass().getSimpleName()+"/";
		projectHelper.encodeKnowledgeBase(encoding, outputFilesPath);
		
		//Feature Selection
		//FeatureSelection featureSelection = new FeatureSelection(encoding, projectHelper, mlTechnique);
		
		//Tuning
		//projectHelper.prepareDatasetForTuning(Constants.FOLDERPATH_CLASSIFIER+outputFilesPath);
		//mlTechnique.tuningClassifier(projectHelper.getProjectLocation()+Constants.FOLDER_CROSS_VALIDATION, Constants.FOLDERPATH_CLASSIFIER+outputFilesPath);
		
		//Training classifier
		projectHelper.createFile("", Constants.FOLDERPATH_CLASSIFIER+outputFilesPath, Constants.FILENAME_TESTING);
		mlTechnique.trainClassifier(projectHelper.getProjectLocation()+Constants.FOLDERPATH_CLASSIFIER+outputFilesPath);
		
		//Testing test cases
			HashMap<String, TestCase> testCases = projectHelper.loadTestCases();
			String report = "TestCase;Precision;Recall;F-measure;MCC\n";
			for (Entry<String, TestCase> entry: testCases.entrySet())
			{
				report = report + launchTestCase(entry.getValue(), encoding, mlTechnique, projectHelper, outputFilesPath);
			}
		
			//Save report
			projectHelper.createFile(report, outputFilesPath+Constants.FOLDER_REPORTS, "report."+Constants.EXTENSION_TXT);
	}
	
	private static String launchTestCase(TestCase testCase, Encoding encoding, MLTechnique mlTechnique, ProjectHelper projectHelper, String filepath) {
		
		//Prepare testing file encoding population of fragments
		String classifierFilespath = Constants.FOLDERPATH_CLASSIFIER+filepath;
		FeatureVectorGenerator featureVectorGenerator = new FeatureVectorGenerator();
		String testingFeatureVectors = "";
		for (String idFragment: testCase.getFragments())
		{
			Fragment fragment = testCase.getFragment(idFragment);
			int idDescription = Integer.parseInt(testCase.getId().split("_")[1].substring(2));
			testingFeatureVectors =  testingFeatureVectors + featureVectorGenerator.getFeatureVector(Constants.MIN_SCORE, new Description(idDescription, testCase.getDescription()), fragment, encoding);
		}
		
		//Save Feature Vectors
		projectHelper.createFile(testingFeatureVectors, classifierFilespath, Constants.FILENAME_TESTING);
		
		//Test Feature Vectors
		mlTechnique.testUsingClassifier(projectHelper.getProjectLocation()+classifierFilespath);
		
		//Retrieve Ranking
		projectHelper.loadResults(testCase,classifierFilespath);
		
		//Save results
		return saveFragmentAt1(testCase.getId(), testCase, projectHelper,filepath);
	}
	
	private static String saveFragmentAt1(String id_model, TestCase testCase, ProjectHelper projectHelper, String filepath) 
	{
		//Select Fragment at 1
		Fragment fragment = testCase.getBestFragment();
		
		//Save Selected Fragment
		filepath = filepath+Constants.FOLDER_RESULTS+id_model+"."+Constants.EXTENSION_MODELS;
		projectHelper.createFile(filepath, fragment.getFragment());
		
		//Report for fragment at 1
		ConfusionMatrix confusionMatrix = new ConfusionMatrix(testCase.getId(), testCase.getOracle().getFragment(), fragment.getFragment(), testCase.getModel());
		return confusionMatrix.toString();
	}

}


