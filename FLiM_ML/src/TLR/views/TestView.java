package TLR.views;


import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import FLiM_ML.Encoding.EncodingBasedExtendedOntology;
import FLiM_ML.Encoding.EncodingBasedOnComparison;
import FLiM_ML.Encoding.EncodingBasedOnComparison3;
import FLiM_ML.Encoding.EncodingBasedOntology;
import FLiM_ML.Encoding.EncodingManual;
import MLTechniques.FNN;
import MLTechniques.RNN;
import MLTechniques.Rankboost;
import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.CaseStudyAdapter;
import data.TestCase;


public class TestView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "FL_KB.views.FL_KB";


	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		
		Composite panel = new Composite(parent, SWT.NONE);
		RowLayout layout = new RowLayout();
		panel.setLayout(layout);
		
		String[] encodings = new String[] { "Based on ontology", "Based on extended ontology", "Based on comparisons"};
		Combo encodingList = new Combo(panel, SWT.DROP_DOWN);
		encodingList.setItems(encodings);
		
		String[] mlTechniques = new String[] { "LtoR/RankBoost", "FNN", "RNN"};
		Combo mlTechniquesList = new Combo(panel, SWT.DROP_DOWN);
		mlTechniquesList.setItems(mlTechniques);
		
		Button b1 = new Button(panel, SWT.NONE);
		b1.setText("Run");
		b1.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
			/*	Encoding encoding;
				System.out.println(encodingList.getSelectionIndex()+" "+mlTechniquesList.getSelectionIndex());
				switch (encodingList.getSelectionIndex())
				{
					case 0: 
						encoding = new EncodingBasedOntology();
						break;
					case 1:
						encoding = new EncodingBasedExtendedOntology();
						break;
					default:
						encoding = new EncodingBasedOnComparison();
				}
				
				MLTechnique mlTechnique = null;
				switch (mlTechniquesList.getSelectionIndex())
				{
					case 0: 
						mlTechnique = new Rankboost();
						break;
					case 1:
						//mlTTechnique = FNN
						break;
					default:
						//mlTTechnique = RNN
				}
				
				TestHelper.runExperiment(encoding,mlTechnique);*/
				//System.out.println("\n\n\n\nOntology\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedOntology(), new Rankboost(100,2));
				//System.out.println("\n\n\n\nExtended Ontology\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedExtendedOntology(), new Rankboost(100,2));
				//System.out.println("\n\n\n\nComparison\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedOnComparison(), new Rankboost(100,10));

				/*System.out.println("\n\n\n\nComparison3\n***********************************************************\n");
				TestHelper.runExperiment(new EncodingBasedOnComparison3(), new Rankboost(100,10));
				
				System.out.println("\n\n\n\nComparison\n***********************************************************\n");
				TestHelper.runExperiment(new EncodingBasedOnComparison(), new Rankboost(100,10));*/

				//System.out.println("\n\n\n\nBaseline\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedOntology(), new Rankboost(100,10));

				//System.out.println("\n\n\n\nExtended\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedExtendedOntology(), new Rankboost(100,10));

				//System.out.println("\n\n\n\nComparison\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedOnComparison3(), new Rankboost(100,10));
				
				//System.out.println("\n\n\n\nComparison\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedOnComparison3(), new Rankboost(100,10));

				System.out.println("\n\n\n\nManual Encoding\n***********************************************************\n");
				TestHelper.runExperiment(new EncodingManual(), new Rankboost(100,10));
				
				/*System.out.println("\n\n\n\nOntology\n***********************************************************\n");
				TestHelper.runExperiment(new EncodingBasedOntology(), new FNN(66,10));*/
				
				/*System.out.println("\n\n\n\nExtended Ontology\n***********************************************************\n");
				TestHelper.runExperiment(new EncodingBasedExtendedOntology(), new FNN(104,10));
				*/
				//System.out.println("\n\n\n\nComparison\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedOnComparison3(), new FNN(7,10));
				
				//System.out.println("\n\n\n\nComparison\n***********************************************************\n");
				//TestHelper.runExperiment(new EncodingBasedOnComparison3(), new RNN(7,10));
			}
				
		});
		
		//Uses the dataset provided by CAF to generate the necessary artifacts
		Button b2 = new Button(panel, SWT.NONE);
		b2.setText("Generate the data experiment");
		b2.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				TestHelper.generateDataExperiment();
				
			}
			
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		
	}
}
