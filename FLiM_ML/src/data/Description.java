package data;

public class Description{
	
	private int id;
	private String description;	
	
	public Description(int id, String descrition) {
		super();
		this.id = id;
		this.description = descrition;		
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescrition() {
		return description;
	}
	
	public void setDescrition(String descrition) {
		this.description = descrition;
	}
	
}
