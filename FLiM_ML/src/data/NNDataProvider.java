package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.datavec.api.conf.Configuration;
import org.datavec.api.records.reader.impl.misc.SVMLightRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.arbiter.optimize.api.data.DataProvider;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import TLR.GO.utils.Constants;
import flame.GO.core.BitSet;

public class NNDataProvider implements DataProvider {

	private int batchSize;
	private int numClasses;
	private int numFeatures;
	private String dataPath;
	private BitSet mask;
	
	public NNDataProvider(int batchSize, int numClasses, int numFeatures, String dataPath, String maskPath) {
		super();
		this.batchSize = batchSize;
		this.numClasses = numClasses;
		this.numFeatures = numFeatures;
		this.dataPath = dataPath;
		getMask(maskPath);
	}
	
	
	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}


	private void getMask(String dataPath) {
		File fileMask = new File(dataPath+Constants.FILENAME_FEATURES);
		mask = new BitSet(numFeatures);
		mask.negate();
		
		try {
            BufferedReader b = new BufferedReader(new FileReader(fileMask));
            String readLine = "";

            while ((readLine = b.readLine()) != null) {
            	mask.flip(Integer.parseInt(readLine)-1);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
		
	}

	@Override
	public Class<?> getDataType() {
		return DataSetIterator.class;
	}

	@Override
	public Object testData(Map<String, Object> arg0) {
		try {
			File fileTesting = new File (dataPath+Constants.FILENAME_TESTING);		
			        
	        SVMLightRecordReader testRecordReader = new SVMLightRecordReader();
	        testRecordReader.initialize(getConfiguration(), new FileSplit(maskFile(fileTesting, Constants.FILENAME_TESTING)));
	        DataSetIterator testIter = new RecordReaderDataSetIterator(testRecordReader, batchSize, numFeatures, numClasses);	//	System.out.println(testIter.totalExamples()+" "+testIter.numExamples());
			return testIter;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object trainData(Map<String, Object> arg0) {

		File fileTraining = new File(dataPath+Constants.FILENAME_TRAINING);
        
		SVMLightRecordReader trainRecordReader = new SVMLightRecordReader();
		try {
			trainRecordReader.initialize(getConfiguration(), new FileSplit(maskFile(fileTraining, Constants.FILENAME_TRAINING)));
			DataSetIterator trainIter = new RecordReaderDataSetIterator(trainRecordReader, batchSize, numFeatures, numClasses);
			return trainIter;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private Configuration getConfiguration()
	{
		Configuration config = new Configuration();
        config.setBoolean(SVMLightRecordReader.ZERO_BASED_INDEXING, true);
        config.setInt(SVMLightRecordReader.NUM_FEATURES, numFeatures);
        //config.setInt(SVMLightRecordReader.NUM_ATTRIBUTES, numFeatures+1);
        return config;
	}

	
	private File maskFile(File file, String filename)
	{
		String fileContent="";
		try {
			BufferedReader b = new BufferedReader(new FileReader(file));
            String readLine = "";
            while ((readLine = b.readLine()) != null) {
            	String[] fields = readLine.split(" ");
            	String newLine = ""+validateTarget(fields[0]);//+" "+fields[1];
            	for(int i=2;i<fields.length-2; i++)
            	{
            		if (mask.get(i-2)) newLine = newLine +" "+fields[i];
            	}
            	newLine = newLine + " " + fields[fields.length-2] + " " + fields[fields.length-1];
                fileContent = fileContent + newLine + "\n";
            }

            File fout = new File(dataPath+"simplified_"+filename);
            FileOutputStream out = new FileOutputStream(fout);
            out.write(fileContent.getBytes());
            out.close();
            return fout;
        } catch (IOException e) {
            e.printStackTrace();
        }
		return null;
	}

	private int validateTarget(String target) 
	{
		float currentTarget = Float.parseFloat(target);
		if (currentTarget==0) return 0;
		else if (currentTarget<0.5) return 1;
		else if (currentTarget<1) return 2;
		else if (currentTarget<1.5) return 3;
		else if (currentTarget<2) return 4;
		else if (currentTarget<2.5) return 5;
		else if (currentTarget<3) return 6;
		else if (currentTarget<3.5) return 7;
		else if (currentTarget<4) return 8;
		return 9;
	}
}
