package data;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import TrainControlDSL.Cabin;
import TrainControlDSL.Equipment;
import TrainControlDSL.Order;
import TrainControlDSL.Property;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainUnit;
import flame.GO.codec.CODECUtils;
import flame.GO.core.Individual;
import ontology.TCMLOntology;

public class Fragment extends Individual{
	
	private String id;
	private EObject fragment;
	private EObject model;
	
	public Fragment(String id, EObject fragment) {
		super(CODECUtils.encode(fragment));
		this.id = id;
		this.fragment = fragment;
		this.model = null;
	}
	
	public Fragment(String id, EObject fragment, EObject model) {
		super(CODECUtils.encode(fragment));
		this.id = id;
		this.fragment = fragment;
		this.model = model;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}	
	
	public EObject getFragment() {
		return fragment;
	}
	
	public void setFragment(EObject fragment) {
		this.fragment = fragment;
	}
	
	public float getDensity()
	{
		return ((float)(getNumberOfElements(fragment)*100)/getNumberOfElements(model));
	}
	
	private int getNumberOfElements(EObject eobject)
	{
		int num_elements = 0;
		TreeIterator<EObject> iterator = eobject.eAllContents();
		while(iterator.hasNext())
		{
			iterator.next();
			num_elements++;
		}
		return num_elements;
	}
	public int getMultiplicity()
	{
		int multiplicity = 0;
		int multiplicityAux = 0;
		TrainUnit modelTrain = (TrainUnit) model;
		TrainUnit fragmentTrain = (TrainUnit) fragment;
		
		
		for (Rule ruleFragment: fragmentTrain.getHasRules())
		{
			for(Rule rule: modelTrain.getHasRules())
			{
				if(rule.getID()!=null && ruleFragment.getID()!=null)
				{
					if(rule.getID().equals(ruleFragment.getID())) multiplicityAux++;
					if (multiplicityAux>1) return multiplicityAux;
				}
			}
			if(multiplicityAux>0) multiplicity = multiplicityAux;
			multiplicityAux = 0;
		}
		
		for (Equipment equipmentFragment: fragmentTrain.getHasEquipments())
		{
			for (Equipment equipment: modelTrain.getHasEquipments())
			{
				boolean isEqual = true;
				if(!getEquipmentNameRoot(equipmentFragment.getName()).equals(equipment.getName())) isEqual = false;
				else 
				{
					if(equipment.getHasProperties().size() == equipmentFragment.getHasProperties().size())
					{
						for(Property property: equipment.getHasProperties())
						{
							for(Property propertyFragment: equipmentFragment.getHasProperties())
							{
								if(!getRootName(property.getName()).equals(getRootName(propertyFragment.getName()))) 
								{
									isEqual = false;
									break;
								}
							}
							if (!isEqual) break;
						}
					} 
					else isEqual = false;

					if(equipment.getHasOrders().size() == equipmentFragment.getHasOrders().size())
					{
						for(Order order: equipment.getHasOrders())
						{
							for(Order orderFragment: equipmentFragment.getHasOrders())
							{
								if(!getRootName(order.getName()).equals(getRootName(orderFragment.getName()))) 
								{
									isEqual = false;
									break;
								}
							}
							if (!isEqual) break;
						}
					}
					else isEqual = false;
				}
				
				if(isEqual) multiplicityAux++;

				if(multiplicityAux>1) return multiplicityAux;
			}
			
		}
		
		/*
		TreeIterator<EObject> iteratorModel = model.eAllContents();
		while(iteratorModel.hasNext())
		{
			EObject elementModel = iteratorModel.next();
			if(elementModel instanceof Rule)
			{
				TreeIterator<EObject> iteratorFragment = fragment.eAllContents();
				while (iteratorFragment.hasNext())
				{
					EObject elementFragment = iteratorFragment.next();
					if(elementModel.getClass().equals(elementFragment.getClass()))
					{
						if (isFragmentInModel(elementModel,elementFragment)) multiplicity++;
					}
				}
			}
		}*/
		if(multiplicityAux>0) return multiplicityAux;
		else return multiplicity;
	}
	
	private String getRootName(String name)
	{
		return name.split(" ")[0];
	}
	
	public static String getEquipmentNameRoot (String word)
	{
		if(word != null)
		{
			if (!word.isEmpty())
			{
				word = word.split(" ")[0];
				int index_last_character = word.length()-1;
				
				//If last character is a digit
				while (Character.isDigit(word.charAt(index_last_character))){
					word = word.substring(0, index_last_character);
					index_last_character--;
				}
				
				//If last character is 's'
				if (word.charAt(index_last_character) == 's'){
					word = word.substring(0, index_last_character);
				}
				
				return word.toLowerCase();
			}
		}
		
		return null;

	}
	
	private boolean isFragmentInModel(EObject elementModel, EObject elementFragment)
	{
		if(!elementModel.getClass().equals(elementFragment.getClass())) return false;
		if(elementModel instanceof Equipment || elementModel instanceof Property || elementModel instanceof Order)
		{
				String nameElementModel = TCMLOntology.getWordRoot(elementModel.eGet(elementModel.eClass().getEStructuralFeature("name")).toString().split(" ")[0]);
				String nameElementFragment = TCMLOntology.getWordRoot(elementFragment.eGet(elementModel.eClass().getEStructuralFeature("name")).toString().split(" ")[0]);
				if(!nameElementModel.equals(nameElementFragment)) return false;
		}
		else if (elementModel instanceof Cabin)
		{
			String nameElementModel = TCMLOntology.getWordRoot(elementModel.eGet(elementModel.eClass().getEStructuralFeature("iD")).toString().split(" ")[0]);
			String nameElementFragment = TCMLOntology.getWordRoot(elementFragment.eGet(elementModel.eClass().getEStructuralFeature("iD")).toString().split(" ")[0]);
			if(!nameElementModel.equals(nameElementFragment))	return false;
		}
		
		//Compare EObjects content
		TreeIterator<EObject> iteratorModel = elementModel.eAllContents();
		TreeIterator<EObject> iteratorFragment = elementFragment.eAllContents();
		while (iteratorModel.hasNext() && iteratorFragment.hasNext())
		{
			EObject nextModel = iteratorModel.next();
			EObject nextFragment = iteratorFragment.next();
			if(!isFragmentInModel(nextModel, nextFragment)) return false;
		}
		if (iteratorFragment.hasNext()) return false;
		
		EList<EObject> referencesElementModel = elementModel.eCrossReferences();
		EList<EObject> referencesElementFragment = elementFragment.eCrossReferences();
		if(referencesElementModel.size()<referencesElementFragment.size()) return false;	
		boolean existReference = false;
		for (EObject referenceFragment :referencesElementFragment)
		{
			for (EObject referenceModel : referencesElementModel)
			{
				if(referenceFragment instanceof Property || referenceFragment instanceof Order)
				{
					if(referenceFragment.eContainer()!=null && referenceModel.eContainer()!=null)
					{
						if (isFragmentInModel(referenceModel.eContainer(), referenceFragment.eContainer()))
						{
							existReference = true;
							break;
						}
					}
				}
				else {
					if (isFragmentInModel(referenceModel, referenceFragment))
					{
						existReference = true;
						break;
					}
				}
			}
			if(!existReference)	return false;
		}

		return true;
	}

	public float getDispersion()
	{
		TrainUnit train = (TrainUnit)fragment;
		int dispersion = train.getHasEquipmentGroups().size();
		dispersion = dispersion + train.getHasFails().size();
		dispersion = dispersion + train.getHasRules().size();
		dispersion = dispersion + train.getHasTrainModules().size();
		/*for (Equipment equipment : train.getHasEquipments())
		{
			for (EquipmentGroup group : train.getHasEquipmentGroups())
			{
				if(group.getMembers().contains(equipment)) break;
				dispersion++;
			}
		}*/
		dispersion = dispersion + train.getHasEquipments().size();
		return ((float)dispersion/getNumberOfElements(fragment));
	}
	
	public int getNumberOfFragmentElements()
	{
		int num_elements = 0;
		TreeIterator<EObject> iterator = fragment.eAllContents();
		while(iterator.hasNext())
		{
			iterator.next();
			num_elements++;
		}
		return num_elements;
	}
	
	public int getNumberOfModelElements()
	{
		int num_elements = 0;
		TreeIterator<EObject> iterator = fragment.eAllContents();
		while(iterator.hasNext())
		{
			iterator.next();
			num_elements++;
		}
		return num_elements;
	}
}
