package data.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class Result {

	private HashMap<String, ArrayList<ValuedFragment>> resultsForDescription;

	
	public Result(){
		resultsForDescription = new HashMap<String, ArrayList<ValuedFragment>>();
	}

	public Result(HashMap<String, ArrayList<ValuedFragment>> resultsForDescription) {
		super();
		this.resultsForDescription = resultsForDescription;
	}
	
	public void addResult(String idDescription, String idFragment, float realScore, float calculatedScore)
	{
		if(!resultsForDescription.containsKey(idDescription))
		{
			ArrayList<ValuedFragment> valuedFragments = new ArrayList<ValuedFragment>();
			valuedFragments.add(new ValuedFragment(idFragment, realScore, calculatedScore));
			resultsForDescription.put(idDescription, valuedFragments);
		}
		else
		{
			resultsForDescription.get(idDescription).add(new ValuedFragment(idFragment, realScore, calculatedScore));
		}
	}
	
	private ArrayList<String> getFragmentsWithMaxRealScore(ArrayList<ValuedFragment> valuedFragments)
	{
		ArrayList<String> fragments = new ArrayList<String>();
		float maxScore = valuedFragments.get(0).getRealScore();
		for (ValuedFragment fragment : valuedFragments)
		{
			if (fragment.getRealScore()>maxScore)
			{
				maxScore = fragment.getRealScore();
				fragments = new ArrayList<String>();
				fragments.add(fragment.getIdFragment());
			}
			else if(fragment.getRealScore() == maxScore)
			{
				fragments.add(fragment.getIdFragment());
			}
		}
		return fragments;
	}
	
	private ArrayList<String> getFragmentsWithMaxCalculatedScore(ArrayList<ValuedFragment> valuedFragments)
	{
		ArrayList<String> fragments = new ArrayList<String>();
		float maxScore = valuedFragments.get(0).getCalculatedScore();
		for (ValuedFragment fragment : valuedFragments)
		{
			if (fragment.getCalculatedScore()>maxScore)
			{
				maxScore = fragment.getCalculatedScore();
				fragments = new ArrayList<String>();
				fragments.add(fragment.getIdFragment());
			}
			else if(fragment.getCalculatedScore() == maxScore)
			{
				fragments.add(fragment.getIdFragment());
			}
		}
		return fragments;
	}
	
	public float compareResults()
	{
		float meanFMeasure = 0;
		for(Entry<String, ArrayList<ValuedFragment>> entry: resultsForDescription.entrySet())
		{
			float meanScoreForDescription = 0;
			float numFragmentsWIthMaxCalculatedScore = 0;
			float maxCalculatedScore = 0;
			float maxRealScore = 0;
			for (ValuedFragment fragment : entry.getValue())
			{
				if(fragment.getRealScore()>maxRealScore)
				{
					maxRealScore = fragment.getRealScore();
				}
				if (fragment.getCalculatedScore()>maxCalculatedScore)
				{
					maxCalculatedScore = fragment.getCalculatedScore();
					meanScoreForDescription = fragment.getRealScore();
					numFragmentsWIthMaxCalculatedScore = 1;
				}
				else if(fragment.getCalculatedScore() == maxCalculatedScore)
				{
					meanScoreForDescription = meanScoreForDescription + fragment.getRealScore();
					numFragmentsWIthMaxCalculatedScore++;
				}
			}
			meanScoreForDescription = meanScoreForDescription / numFragmentsWIthMaxCalculatedScore;
			meanFMeasure = meanFMeasure +( meanScoreForDescription * 100 / maxRealScore);
		}

		return meanFMeasure / resultsForDescription.size();
	}
	/*public float compareResults()
	{
		float meanFMeasure = 0;

			ArrayList<String> fragmentsWithMaxRealScore = getFragmentsWithMaxRealScore(entry.getValue());
			ArrayList<String> fragmentsWithMaxCalculatedScore = getFragmentsWithMaxCalculatedScore(entry.getValue());

			float TP = 0;
			float FP = 0;
			float FN = 0;
			for(String idFragment: fragmentsWithMaxRealScore)
			{
				if(fragmentsWithMaxCalculatedScore.contains(idFragment)) TP++;
				else FN++;
			}
			for(String idFragment: fragmentsWithMaxCalculatedScore)
			{
				if(!fragmentsWithMaxRealScore.contains(idFragment)) FP++;
			}
			float precision = TP / (TP+FP);
			float recall = TP / (TP+FN);
			if(recall!=0 && precision!=0)
			{
				float fMeasure = 2*(precision*recall)/(precision+recall);
				meanFMeasure = meanFMeasure + fMeasure;
			}
		}
		return (meanFMeasure / resultsForDescription.size());
	}*/
}
