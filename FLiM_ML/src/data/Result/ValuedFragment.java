package data.Result;

import java.util.Comparator;

public class ValuedFragment {

	private String idFragment;
	private float realScore;
	private float calculatedScore;
	
	
	public ValuedFragment(String idFragment, float realScore, float calculatedScore) {
		super();
		this.idFragment = idFragment;
		this.realScore = realScore;
		this.calculatedScore = calculatedScore;
	}
	
	public String getIdFragment() {
		return idFragment;
	}
	
	public float getRealScore() {
		return realScore;
	}
	
	public float getCalculatedScore() {
		return calculatedScore;
	}
	
	public static Comparator<ValuedFragment> getRealScoreComparator() {
        return new Comparator<ValuedFragment>() {

			@Override
			public int compare(ValuedFragment o1, ValuedFragment o2) {
				if(o1.getRealScore() < o2.getRealScore())
				{
					return -1;
				}
				else if(o1.getRealScore() > o1.getRealScore())
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
        };
    }

    public static Comparator<ValuedFragment> getCalculatedValueComparator() {
    	return new Comparator<ValuedFragment>() {

			@Override
			public int compare(ValuedFragment o1, ValuedFragment o2) {
				if(o1.getCalculatedScore() < o2.getCalculatedScore())
				{
					return -1;
				}
				else if(o1.getCalculatedScore() > o1.getCalculatedScore())
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
        };
    }
	
}
