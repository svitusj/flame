package data;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import org.eclipse.emf.ecore.EObject;

import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.Constants;


public class TestCase{
	
	
	private String id;
	private EObject model;
	private Fragment oracle;
	private String description;
	private ArrayList<String> idFragmentsDataset;
	private ArrayList<Float> scoreFragmentsDataset;
		
	public TestCase(String id, EObject model, EObject oracle, String description) {
		this.id = id;
		this.model = model;
		this.oracle = new Fragment("oracle", oracle, model);
		this.description = description;
	}

	public TestCase(String id, EObject model, EObject oracle, String description, ArrayList<String> fragments) {
		this.id = id;
		this.model = model;
		this.oracle = new Fragment("oracle", oracle, model);
		this.description = description;
		this.idFragmentsDataset = fragments;
		scoreFragmentsDataset = new ArrayList<Float>();
	}
	
	public String getId()
	{
		return id;
	}
	
	public EObject getModel() {
		return model;
	}

	public Fragment getOracle()
	{
		return oracle;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public Fragment getFragment(String id_fragment)
	{
		ProjectHelper project = ProjectHelper.getInstance();
		String filepathFragment = Constants.FOLDERPATH_TEST_CASES+id+"/fragments/";		
		return new Fragment(id_fragment,project.loadFragment(id_fragment, filepathFragment), model);
	}
	
	public ArrayList<String> getFragments ()
	{
		return idFragmentsDataset;
	}
	
	public void setScore(int index, float score)
	{
		scoreFragmentsDataset.add(index, score);
	}
	
	public Fragment getBestFragment()
	{
		System.out.println(idFragmentsDataset);
		System.out.println(scoreFragmentsDataset);
		ArrayList<Integer> indexesBestFragments = new ArrayList<Integer>();
		float bestScore = -1;
		for (int i=0; i<scoreFragmentsDataset.size(); i++)
		{
			if (bestScore<scoreFragmentsDataset.get(i))
			{
				indexesBestFragments = new ArrayList<Integer>();
				indexesBestFragments.add(i);
				bestScore = scoreFragmentsDataset.get(i);
			}
			else if (bestScore==scoreFragmentsDataset.get(i))
			{
				indexesBestFragments.add(i);
			}
		}
		
		for (int i=0;i<indexesBestFragments.size();i++)
		{
			System.out.println("Index: "+indexesBestFragments.get(i) +" fragment: "+idFragmentsDataset.get(indexesBestFragments.get(i)));
		}
		int randomNum = ThreadLocalRandom.current().nextInt(0, indexesBestFragments.size());
		return getFragment(idFragmentsDataset.get(indexesBestFragments.get(randomNum)));
	}
}
