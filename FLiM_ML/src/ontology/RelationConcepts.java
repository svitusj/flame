package ontology;

public class RelationConcepts {
	
	private String concept1;
	private String concept2;
	
	public RelationConcepts(String concept1, String concept2) {
		
		this.concept1 = concept1;
		this.concept2 = concept2;
	}
	
	public String toString()
	{
		return (concept1 + "-" + concept2);
	}
	
	public boolean isEqualRelation (String concept1, String concept2)
	{
		if (concept1.equals(this.concept1) && concept2.equals(this.concept2))
			return true;
		else if (concept1.equals(this.concept2) && concept2.equals(this.concept1)) {
			return true;
		}
		return false;
	}
	
	public String getFirstConcept()
	{
		return concept1;
	}
	
	public String getSecondConcept()
	{
		return concept2;
	}
	
	@Override
    public boolean equals(Object o) { 
  
		RelationConcepts object = (RelationConcepts) o;
		
		if (concept1.equals(object.concept1) && concept2.equals(object.concept2))
			return true;
		else if (concept1.equals(object.concept2) && concept2.equals(object.concept1)) {
			return true;
		}
		return false;
	}
}
