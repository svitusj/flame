package ontology;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class TCMLOntology {

	private final ArrayList<String> CONCEPTS_TCML = new ArrayList<String>();
	private final HashMap<Integer, RelationConcepts> RELATIONS_TCML = new HashMap<Integer, RelationConcepts>();

	public int getNumberConcepts()
	{
		return CONCEPTS_TCML.size();
	}
	
	public ArrayList<String> getConcepts()
	{
		return CONCEPTS_TCML;
	}
	
	public int getNumberRelations()
	{
		return RELATIONS_TCML.size();
	}
	
	public HashMap<Integer, RelationConcepts> getRelations()
	{
		return RELATIONS_TCML;
	}
	
	public ArrayList<String> getTerms (String sentence)
	{
		ArrayList<String> terms = new ArrayList<String>();
		
		sentence = sentence.replace(".", "");
		sentence = sentence.replace(",", "");

		String[] words = sentence.split(" ");
		for (int i=0;i<words.length-1;i++)
		{
			terms.add(getWordRoot(words[i]));
			terms.add(getWordRoot(words[i])+"_"+getWordRoot(words[i+1]));
		}
		terms.add(getWordRoot(words[words.length-1]));
		return terms;
	}
	public static String getWordRoot (String word)
	{
		if(word != null)
		{
			if (!word.isEmpty())
			{
				word = word.split(" ")[0];
				int index_last_character = word.length()-1;
				
				//If last character is a digit
				while (Character.isDigit(word.charAt(index_last_character))){
					word = word.substring(0, index_last_character);
					index_last_character--;
				}
				
				//If last character is 's'
				if (word.charAt(index_last_character) == 's'){
					word = word.substring(0, index_last_character);
				}
				
				return word.toLowerCase();
			}
		}
		
		return null;

	}
	
	public int getIndexConcept (String word)
	{
		if (word != null)
		{
			return CONCEPTS_TCML.indexOf(getWordRoot(word));
		}
		
		return -1;
	}
	
	public int getIndexRelations(String word1, String word2){
		
		if ( word1!=null && word2 != null ){
			for ( Entry<Integer, RelationConcepts> relation : RELATIONS_TCML.entrySet())
			{
				if((relation.getValue()).isEqualRelation(getWordRoot(word1), getWordRoot(word2))){
					return relation.getKey();
				}
			}
		}
		return -1;
	}
	
	public TCMLOntology(){
		
		String pantograph = "pantograph";
		String circuit_breaker = "circuit_breaker";
		String ccu = "ccu";
		String acr = "acr";
		String outdoor_outlet = "outdoor_outlet";
		String button = "button";
		String desk = "desk";
		String cabin = "cabin";
		String door = "door";
		String car = "car";
		String coupling = "coupling";
		String insulation_wrench = "insulation_wrench";
		String act = "act";
		String light = "light";
		String communication = "communication";
		String compressor = "compressor";
		String brake = "brake";
		String hvac = "hvac";
		String emergency_loop = "emergency_loop";
		String battery = "battery";
		String ramp = "ramp";
		String register = "register";
		String siv = "siv";
		String traction = "traction";

		
		ArrayList<String> KEYWORDS_TCML = CONCEPTS_TCML;
		KEYWORDS_TCML.add(pantograph);
		KEYWORDS_TCML.add(circuit_breaker);
		KEYWORDS_TCML.add(ccu);
		KEYWORDS_TCML.add(acr);
		KEYWORDS_TCML.add(outdoor_outlet);
		KEYWORDS_TCML.add(button);
		KEYWORDS_TCML.add(desk);
		KEYWORDS_TCML.add(cabin);
		KEYWORDS_TCML.add(door);
		KEYWORDS_TCML.add(car);
		KEYWORDS_TCML.add(coupling);
		KEYWORDS_TCML.add(insulation_wrench);
		KEYWORDS_TCML.add(act);
		KEYWORDS_TCML.add(light);
		KEYWORDS_TCML.add(communication);
		KEYWORDS_TCML.add(compressor);
		KEYWORDS_TCML.add(brake);
		KEYWORDS_TCML.add(hvac);
		KEYWORDS_TCML.add(emergency_loop);
		KEYWORDS_TCML.add(battery);
		KEYWORDS_TCML.add(ramp);
		KEYWORDS_TCML.add(register);
		KEYWORDS_TCML.add(siv);
		KEYWORDS_TCML.add(traction);
		
		
		HashMap<Integer, RelationConcepts> RELATION_KEYWORDS_TCML = RELATIONS_TCML;
		//int key = 0;
		RELATION_KEYWORDS_TCML.put(0, new RelationConcepts(pantograph, button));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(pantograph, circuit_breaker));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(pantograph, insulation_wrench));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(insulation_wrench, cabin));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(cabin, desk));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(desk, button));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(button, door));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(door, car));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(car, coupling));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(coupling, cabin));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(car, ccu));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(car, acr));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(car, outdoor_outlet));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(outdoor_outlet, circuit_breaker));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(acr, circuit_breaker));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(ccu, circuit_breaker));
		RELATION_KEYWORDS_TCML.put(RELATION_KEYWORDS_TCML.size(), new RelationConcepts(circuit_breaker, button));
		
	}

	public boolean containsConcept(String term) {
		term = getWordRoot(term);		
		return CONCEPTS_TCML.contains(term);
	}
	
	public RelationConcepts containsRelation(String concept1, String concept2) {
		String c1 = getWordRoot(concept1);
		String c2 = getWordRoot(concept2);
		RelationConcepts relation = new RelationConcepts(c1, c2);
		if (RELATIONS_TCML.containsValue(relation)) return relation;
		
		relation =new RelationConcepts(c2, c1); 
		if (RELATIONS_TCML.containsValue(relation)) return relation;
		
		return null;
	}
	
	public int containsRelations(ArrayList<String> terms)
	{
		int num_relations = 0;
		
		for(int i1=0;i1<terms.size()-1; i1++)
		{
			for(int i2=i1+1; i2<terms.size() ;i2++)
			{
				if(getIndexRelations(getWordRoot(terms.get(i1)), getWordRoot(terms.get(i2))) != -1)
				{
					num_relations++;
				}
			}
		}
		
		return num_relations;
	}
}
