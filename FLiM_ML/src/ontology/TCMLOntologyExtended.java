package ontology;

import java.util.ArrayList;

public class TCMLOntologyExtended extends TCMLOntology {

	private final ArrayList<String> ATTRIBUTES_TCML = new ArrayList<String>(); 

	public int getNumberAttributes()
	{
		return ATTRIBUTES_TCML.size();
	}
	
	public ArrayList<String> getAttributes()
	{
		return ATTRIBUTES_TCML;
	}
	
	public int getIndexAttribute (String word)
	{
		if (word != null)
		{
			return ATTRIBUTES_TCML.indexOf(getWordRoot(word));
		}
		
		return -1;
	}
	
	public TCMLOntologyExtended(){
		
		super();
		
		String open = "open";
		String closed = "closed";
		String up = "up";
		String down = "down";
		String middle = "middle";
		String actuated = "actuated"; 
		String unkown = "unknown";
		String enabled = "enabled";
		String disabled = "disabled";
		String active = "active";
		String isolated = "isolated";
		String on = "on";
		String off = "off";
		String present = "present";
		
		ArrayList<String> ATTRIBUTES_KEYWORDS_TCML = ATTRIBUTES_TCML;
		ATTRIBUTES_KEYWORDS_TCML.add(open);
		ATTRIBUTES_KEYWORDS_TCML.add(closed);
		ATTRIBUTES_KEYWORDS_TCML.add(up);
		ATTRIBUTES_KEYWORDS_TCML.add(down);
		ATTRIBUTES_KEYWORDS_TCML.add(middle);
		ATTRIBUTES_KEYWORDS_TCML.add(actuated);
		ATTRIBUTES_KEYWORDS_TCML.add(unkown);
		ATTRIBUTES_KEYWORDS_TCML.add(enabled);
		ATTRIBUTES_KEYWORDS_TCML.add(disabled);
		ATTRIBUTES_KEYWORDS_TCML.add(active);
		ATTRIBUTES_KEYWORDS_TCML.add(isolated);
		ATTRIBUTES_KEYWORDS_TCML.add(on);
		ATTRIBUTES_KEYWORDS_TCML.add(off);
		ATTRIBUTES_KEYWORDS_TCML.add(present);
				
	}

	public boolean containsAttribute(String term) {
		term = getWordRoot(term);		
		return ATTRIBUTES_TCML.contains(term);
	}
}
