package evaluation;

import java.util.ArrayList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil.EqualityHelper;

import TrainControlDSL.Equipment;
import TrainControlDSL.EquipmentGroup;
import TrainControlDSL.Order;
import TrainControlDSL.OrderInhibitionRule;
import TrainControlDSL.OrderInvocationRule;
import TrainControlDSL.Property;
import TrainControlDSL.PropertySynchronizationRule;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainModule;
import TrainControlDSL.TrainUnit;

public class ConfusionMatrix {

	private String idTestCase;
	private EObject oracle;
	private EObject fragment;
	private EObject model;
	
	private float TP = 0;
	private float TN = 0;
	private float FP = 0;
	private float FN = 0;
	
	public ConfusionMatrix(String idTestcase, EObject oracle, EObject top_fragment, EObject model)
	{
		super();
		this.idTestCase = idTestcase;
		this.oracle = oracle;
		this.fragment = top_fragment;
		this.model = model;
		compareFragments();
		evaluateTrueNegative();
	}
	
	private void evaluateTrueNegative() {
		
		TreeIterator<EObject> iterator_model = model.eAllContents();
		while (iterator_model.hasNext())
		{
			EObject element_model = iterator_model.next();
			if (!isElementInEObject(element_model, fragment) && !isElementInEObject(element_model, oracle))
			{
				TN++;
			}
		}
		
	}

	private boolean isElementInEObject(EObject element_model, EObject fragment) {
		TreeIterator<EObject> iterator = fragment.eAllContents();
		EqualityHelper equality_helper = new EqualityHelper();

		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if (equality_helper.equals(element_model, element))
			{
				return true;
			}
		}
		return false;
	}
	
	public EObject getOracleFragment()
	{
		return oracle;
	}
	
	public EObject getTopFragment()
	{
		return fragment;
	}
	
	public float getRecall()
	{
		return (TP / ( TP + FN ))*100;
	}
	
	public float getPrecision()
	{
		if (TP==0 && FP==0)
		{
			return 0;
		}
		return (TP / (TP + FP))*100;
	}
	
	public float getF_Measure()
	{
		float precision = getPrecision();
		float recall = getRecall();
		
		if (precision==0 && recall==0){
				return 0;
		}
		return 2*((precision*recall)/(precision+recall));
	}
	
	public float getMCC()
	{
		if ( (TP+FP)*(TP+FN)*(TN+FP)*(TN+FN) == 0)
		{
			return 0;
		}
		return (((TP*TN)-(FP*FN))/ ((float)(Math.sqrt((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN)))));
	}
	
	public String getWordRoot (String word)
	{
		if(word != null)
		{
			if (!word.isEmpty())
			{
				int index_last_character = word.length()-1;
				
				//If last character is a digit
				while (Character.isDigit(word.charAt(index_last_character))){
					word = word.substring(0, index_last_character);
					index_last_character--;
				}
				
				//If last character is 's'
				if (word.charAt(index_last_character) == 's'){
					word = word.substring(0, index_last_character);
				}
				
				return word.toLowerCase();
			}
		}
		
		return null;

	}
	
	/*private ConfusionMatrix compareFragments()
	{
		TrainUnit base_fragment = (TrainUnit) oracle_fragment.getFragment();
		TrainUnit compared_fragment = (TrainUnit) top_fragment.getFragment();
		
		TreeIterator<EObject> base_iterator = base_fragment.eAllContents();
		TreeIterator<EObject> compared_iterartor = compared_fragment.eAllContents();
		int num_elements=0;
		while(base_iterator.hasNext())
		{
			EObject base_object = base_iterator.next();
			if(compared_iterartor.hasNext())
			{
				EObject compared_object = compared_iterartor.next();
				System.out.println(base_object);
				System.out.println(compared_object);
			}
			else
			{
				false_negative++;
			}
			num_elements++;
			
		}
		System.out.println(num_elements);
		return this;
	}*/
	private ConfusionMatrix compareFragments()
	{
		
		TrainUnit base_fragment = (TrainUnit) oracle;
		TrainUnit compared_fragment = (TrainUnit) fragment;
		compareEquipments(base_fragment, compared_fragment);

		//compareGroupEquipments(oracle_fragment, ranked_fragment);
		compareModules(base_fragment, compared_fragment);

		compareRules(base_fragment, compared_fragment);
		
		return this;
	}

	private void compareStringArray(ArrayList<String> base_strings, ArrayList<String> compared_strings) {
	
		for (String base_element : base_strings)
		{
			if (compared_strings.contains(base_element))
			{
				TP++;
				compared_strings.remove(base_element);
			}
			else
			{
				FN++;
			}	
		}
		FP = FP + compared_strings.size();		
	}
	
	private void compareEquipments(TrainUnit base_fragment, TrainUnit compared_fragment)
	{
		ArrayList<String> base_equipments = new ArrayList<String>();
		for (Equipment equipment : base_fragment.getHasEquipments())
		{
			base_equipments.add(getWordRoot(equipment.getName()));
		}
		ArrayList<String> compared_equipments = new ArrayList<String>();
		for (Equipment equipment : compared_fragment.getHasEquipments())
		{
			compared_equipments.add(getWordRoot(equipment.getName()));
		}
		compareStringArray(base_equipments, compared_equipments);
		
		ArrayList<String> base_contents = new ArrayList<String>();
		TreeIterator<EObject> iterator = base_fragment.eAllContents();
		while(iterator.hasNext())
		{
			EObject base_object = iterator.next();
			if(base_object instanceof Property)
			{
				base_contents.add(((Property)base_object).getName());
			}
			else if (base_object instanceof Order)
			{
				base_contents.add(((Order)base_object).getName());
			}
		}
		ArrayList<String> compared_contents = new ArrayList<String>();
		iterator = compared_fragment.eAllContents();
		while(iterator.hasNext())
		{
			EObject base_object = iterator.next();
			if(base_object instanceof Property)
			{
				compared_contents.add(((Property)base_object).getName());
			}
			else if (base_object instanceof Order)
			{
				compared_contents.add(((Order)base_object).getName());
			}
		}
		compareStringArray(base_contents, compared_contents);
	}
	
	private void compareGroupEquipments(TrainUnit base_fragment, TrainUnit compared_fragment)
	{
		ArrayList<String> base_group_equipments = new ArrayList<String>();
		for (EquipmentGroup equipment_group : base_fragment.getHasEquipmentGroups())
		{
			base_group_equipments.add(getWordRoot(equipment_group.getName()));
		}
		ArrayList<String> compared_group_equipments = new ArrayList<String>();
		for (Equipment equipment : compared_fragment.getHasEquipments())
		{
			compared_group_equipments.add(getWordRoot(equipment.getName()));
		}
		compareStringArray(base_group_equipments, compared_group_equipments);
	}
	
	private void compareModules(TrainUnit base_fragment, TrainUnit compared_fragment)
	{
		ArrayList<String> base_modules = new ArrayList<String>();
		for (TrainModule oracle_module : base_fragment.getHasTrainModules())
		{
			base_modules.add(getWordRoot(oracle_module.getID()));
		}
		ArrayList<String> compared_modules = new ArrayList<String>();
		for (TrainModule ranked_module : compared_fragment.getHasTrainModules())
		{
			compared_modules.add(getWordRoot(ranked_module.getID()));
		}
		compareStringArray(base_modules, compared_modules);
	}
	
	private void compareRules(TrainUnit base_fragment, TrainUnit compared_fragment)
	{
		boolean found = false;
		
		ArrayList<Rule> compared_rules = new ArrayList<Rule>(compared_fragment.getHasRules());
		for ( Rule base_rule : base_fragment.getHasRules())
		{

			for (Rule compared_rule : compared_rules)
			{
				if (compareRules(base_rule, compared_rule))
				{
					TP++;
					compared_rules.remove(compared_rule);
					found = true;
					break;
				}
			}
			if (!found)
			{
				//Exist the rule in oracle, and not exist in ranked
				FN++;
				
				if(base_rule.getHasTrigger()!=null)
				{
					FN++;
					if(base_rule.getHasTrigger().getProperty()!=null)
					{
						FN++;
					}
				}
			}
		}
		
		for(Rule ranked_rule : compared_rules)
		{
			FP++;
			if(ranked_rule.getHasTrigger()!=null)
			{
				FP++;
				if(ranked_rule.getHasTrigger().getProperty()!=null)
				{
					FP++;
				}
			}
		}
	}
	
	private boolean compareRules(Rule rule1, Rule rule2)
	{

		if (rule1 instanceof PropertySynchronizationRule)
		{
			if (rule2 instanceof PropertySynchronizationRule)
			{
				//compareTrigger(rule1, rule2);
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (rule1 instanceof OrderInvocationRule)
		{
			if (rule2 instanceof OrderInvocationRule)
			{
				//compareTrigger(rule1, rule2);
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (rule2 instanceof OrderInhibitionRule)
			{
				//compareTrigger(rule1, rule2);
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	private void compareTrigger(Rule rule1, Rule rule2) {
	
		if(rule1.getHasTrigger()!=null && rule2.getHasTrigger()!=null)
		{
			TP++;
			
			//Trigger type
			/*
			if(rule1.getHasTrigger().getType()==rule2.getHasTrigger().getType())
			{
				true_positive++;
			}
			else
			{
				false_negative++;
				false_positive++;
			}*/
			
			//Trigger property
			compareProperty(rule1.getHasTrigger().getProperty(), rule2.getHasTrigger().getProperty());	
			
		}
		else if (rule1.getHasTrigger()!=null)
		{
			FN++;
		}
		else if (rule2.getHasTrigger() != null)
		{				
			FP++;
		}
		
	}
	
	private void compareProperty(Property property1, Property property2)
	{

		//Trigger property
		if(property1!=null && property2!=null)
		{
			TP++;
		}
		else if (property1!=null)
		{
			FN++;
		}
		else if (property2!=null)
		{
			FP++;
		}
	}
	
	public String getReport() 
	{
		String report = "REPORT FOR TEST CASE " + idTestCase + "\n\n";

		report = report + "Recall:\t\t" + getRecall()+ "\n";
		report = report + "Precision:\t\t" + getPrecision()+ "\n";
		report = report + "F-measure:\t\t" + getF_Measure() + "\n";
		report = report + "MCC:\t\t" + getMCC()+ "\n";
		
		return report;
	}
	
	@Override
	public String toString()
	{
		String report =idTestCase+";"+getPrecision()+";"+getRecall()+";"+getF_Measure()+";"+getMCC()+"\n";
		return report;
	}
}
