package FLiM_ML;

import java.util.ArrayList;
import java.util.Random;

import FLiM_ML.Encoding.Encoding;
import MLTechniques.MLTechnique;
import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.Constants;
import flame.GO.core.BitSet;

public class FeatureSelection {

	private static final int sizeMaskPopulation = 10;
	private static final int stopCondition = 5;
	
	private BitSet mask;
	private float maskFitnessValue = -1;
	
	public FeatureSelection(Encoding encoding, ProjectHelper project, MLTechnique mlTechnique)
	{
		this.mask = new BitSet(encoding.getNumberOfFeatures());
		
		//Prepare dataset to perform feature selection
		String trainingFilesPath = Constants.FOLDERPATH_CLASSIFIER+encoding.getClass().getSimpleName()+"_"+mlTechnique.getClass().getSimpleName()+"/"+Constants.FILENAME_TRAINING;
		project.prepareDatasetForCrossValidation(trainingFilesPath);
		
		//Generate a initial population of masks
		ArrayList<BitSet> maskPopulation = generateInitialMasksPopulation(sizeMaskPopulation, encoding);
		//System.out.println(maskPopulation);
		
		//Evolutionary Algorithm
		for (int iteration=0; iteration<stopCondition; iteration++)
		{
			//Evaluate the masks in the population through the Fitness Function
			fitnessFunction(maskPopulation, project, mlTechnique);
			
			//Evolve the masks in the population
			evolveMasksPopulation(maskPopulation);
		}
		mask = maskPopulation.get(0);
		
		//Save the mask
		String filepath = Constants.FOLDERPATH_CLASSIFIER + encoding.getClass().getSimpleName() + "_" +mlTechnique.getClass().getSimpleName()+"/";
		project.createFile(maskToString(mask), filepath, Constants.FILENAME_FEATURES);
	}

	public BitSet getMask()
	{
		return mask;
	}
	
	private ArrayList<BitSet> generateInitialMasksPopulation(int sizePopulation, Encoding encoding)
	{
		ArrayList<BitSet> masksPopulation = new ArrayList<BitSet>();
		masksPopulation.add(createFullMask(encoding));
		for (int i=1; i<sizePopulation; i++)
		{
			masksPopulation.add(createRandomMask(encoding));
		}
		return masksPopulation;
	}
	
	
	private BitSet createRandomMask(Encoding encoding) {
		BitSet randomMask = new BitSet(encoding.getNumberOfFeatures());
		Random randomGenerator = new Random();
	    for (int i = 0; i < encoding.getNumberOfFeatures() ; ++i)
	    {
	    	randomMask.set(randomGenerator.nextBoolean(), i);
	    }
	    if(isValidMask(randomMask)) return randomMask;
	    else return createRandomMask(encoding);
	}
	
	private boolean isValidMask(BitSet mask) {
		for(int indexMask=0;indexMask<mask.size();indexMask++)
		{
			if(mask.get(indexMask)) return true;
		}
		return false;
	}

	private BitSet createFullMask(Encoding encoding) {
		BitSet randomMask = new BitSet(encoding.getNumberOfFeatures());
	    for (int i = 0; i < encoding.getNumberOfFeatures() ; ++i)
	    {
	    	randomMask.set(true, i);
	    }
		return randomMask;
	}

	private ArrayList<BitSet> evolveMasksPopulation(ArrayList<BitSet> masksPopulation)
	{
		for(int i=0; i<masksPopulation.size(); i++)
		{
			mutate(masksPopulation.get(i));
		}
		return masksPopulation;
	}
	
	private BitSet mutate(BitSet maskToMutate)
	{
		Random randomGenerator = new Random();
		
		int min = 0;
		int max = maskToMutate.size() - 1;
		int randomPosition = randomGenerator.nextInt((max - min) + 1) + min;

		maskToMutate.flip(randomPosition);
		
		if(isValidMask(maskToMutate)) return maskToMutate;
		else return mutate(maskToMutate);
	}
	
	private void fitnessFunction(ArrayList<BitSet> masksPopulation, ProjectHelper project, MLTechnique mlTechnique)
	{
		
		for(int i=0; i<masksPopulation.size();i++)
		{
			//Save mask in a txt file
			project.createFile(maskToString(masksPopulation.get(i)), Constants.FOLDER_CROSS_VALIDATION, Constants.FILENAME_CROSS_VALIDATION_FEATURES_FILE);
			
			//Cross-validation
			mlTechnique.crossValidation(project.getProjectLocation()+Constants.FOLDER_CROSS_VALIDATION);
			
			//Retrieve cross-validation results
			retrieveResults(masksPopulation.get(i), project);
		}
		
	}
	
	private String maskToString(BitSet maskToParse)
	{
		String parsedMask = "";
		for (int n=0; n<maskToParse.size(); n++)
		{
			if(maskToParse.get(n))
			{
				parsedMask = parsedMask + (n+1) + "\n";
			}
		}
		return parsedMask;
	}
	
	
	private void retrieveResults(BitSet evaluatingMask, ProjectHelper project){
				
		float meanFMeasure = project.retrieveScores();
				
		if(meanFMeasure>maskFitnessValue)
		{
			maskFitnessValue = meanFMeasure;
			mask = evaluatingMask;
		}
	}
	
	
}
