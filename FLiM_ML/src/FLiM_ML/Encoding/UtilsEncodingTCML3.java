package FLiM_ML.Encoding;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import TrainControlDSL.Cabin;
import TrainControlDSL.Condition;
import TrainControlDSL.Equipment;
import TrainControlDSL.EquipmentGroup;
import TrainControlDSL.EvaluableProperty;
import TrainControlDSL.InhibitableElement;
import TrainControlDSL.InvocatedOrder;
import TrainControlDSL.NestedCondition;
import TrainControlDSL.Order;
import TrainControlDSL.OrderInhibitionRule;
import TrainControlDSL.OrderInvocationRule;
import TrainControlDSL.Property;
import TrainControlDSL.PropertySynchronizationRule;
import TrainControlDSL.Rule;
import TrainControlDSL.SingleCondition;
import ontology.RelationConcepts;
import ontology.TCMLOntology;

public class UtilsEncodingTCML3 {
	
	public static Float presenceOfConcept(String concept, ArrayList<String> descriptionConcepts, ArrayList<String> fragmentConcepts) {
		float numConceptDescription = 0;
		float numConceptFragment = 0;
		for (int i=0; i<descriptionConcepts.size(); i++)
		{
			if (concept.equals(TCMLOntology.getWordRoot(descriptionConcepts.get(i))))
			{
				numConceptDescription++;
			}
		}
		
		for (int i=0;i<fragmentConcepts.size(); i++)
		{
			if(concept.equals(TCMLOntology.getWordRoot(fragmentConcepts.get(i))))
			{
				numConceptFragment++;
			}
		}

		return 1/(1+Math.abs(numConceptDescription-numConceptFragment));
		
	}
	
	public static Float presenceOfRelation(RelationConcepts relation, ArrayList<RelationConcepts> descriptionRelations, ArrayList<RelationConcepts> fragmentRelations) {
		float numRelationDescription = 0;
		float numRealtionFragment = 0;
		for(int n=0;n<descriptionRelations.size();n++)
		{
			if (relation.equals(descriptionRelations.get(n)))
			{
				numRelationDescription++;
			}
		}
		
		for (int n=0;n<fragmentRelations.size(); n++)
		{
			if (relation.equals(fragmentRelations.get(n)))
			{
				numRealtionFragment++;
				break;
			}
		}
		return 1/(1+Math.abs(numRelationDescription-numRealtionFragment));
	}
	
	public static Float presenceOfConceptInRelation(ArrayList<String> fragmentConcepts, EObject model)
	{
		float relevanceOfConceptInRelation = 0;
		float numRules = 0;
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject object =iterator.next();
			float numConceptsInRelation = 0;
			if(object instanceof Rule)
			{
				numRules++;
				ArrayList<String> keywords = getKeywordsOfRule((Rule)object);
				for(String keyword: keywords)
				{
					if(fragmentConcepts.contains(keyword)) numConceptsInRelation++; 
				}
				relevanceOfConceptInRelation = relevanceOfConceptInRelation + 1/(1+Math.abs(keywords.size()-numConceptsInRelation));
			}
		}
		if (numRules==0) return (float) 0;
		return relevanceOfConceptInRelation/numRules;
	}
	
	public static ArrayList<String> getKeywordsOfRule(Rule rule)
	{
		ArrayList<String> equimpents_related = new ArrayList<String>();
		
		//TRIGGER
		if(rule.getHasTrigger()!=null) 
		{
			if(rule.getHasTrigger().getProperty()!=null)
			{
				equimpents_related.addAll(getKeywordsOfProperty(rule.getHasTrigger().getProperty()));
			}
		}
			
		//SYNCHRONIZATION RULE
		if (rule instanceof PropertySynchronizationRule)
		{
			
			if(((PropertySynchronizationRule) rule).getProperty()!=null)
			{
				equimpents_related.addAll(getKeywordsOfProperty(((PropertySynchronizationRule)rule).getProperty()));
			}
			
			Condition condition = ((PropertySynchronizationRule) rule).getHasConditions();
			equimpents_related.addAll(getKeywordsOfConditions(condition));
						
		}
		//INVOCATION RULE
		else if (rule instanceof OrderInvocationRule){
			for (InvocatedOrder invocated_order : ((OrderInvocationRule)rule).getHasPhysicalEffects())
			{
				if(invocated_order.getOrder()!=null)
				{
					equimpents_related.addAll(getKeywordsOfOrders(invocated_order.getOrder()));
				}
				
				Condition condition = (invocated_order.getHasConditions());
				equimpents_related.addAll(getKeywordsOfConditions(condition));
			}
		}
		//INHIBITION RULE
		else
		{
			InhibitableElement inhibitable_element = ((OrderInhibitionRule)rule).getInhibitableElement();
			if(inhibitable_element !=null)
			{
				if (inhibitable_element instanceof Equipment)
				{
					equimpents_related.add(((Equipment)inhibitable_element).getName());
				}
				else if (inhibitable_element instanceof Order)
				{
					equimpents_related.addAll(getKeywordsOfOrders((Order)inhibitable_element));
				}
			}
			
			Condition condition = (((OrderInhibitionRule)rule).getHasConditions());
			equimpents_related.addAll(getKeywordsOfConditions(condition));
		}
		
		//Remove duplicated values
		equimpents_related = (ArrayList<String>) equimpents_related.stream().distinct().collect(Collectors.toList());
		
		//Format keywords in lower case and without numbers
		for(int i=0;i<equimpents_related.size();i++)
		{
			equimpents_related.set(i, TCMLOntology.getWordRoot(equimpents_related.get(i)));
		}
		
		return equimpents_related;

	}
	
	public static ArrayList<String> getKeywordsOfOrders(Order order) 
	{	
		ArrayList<String> equipments_related = new ArrayList<String>();
		
		EObject econtainer = order.eContainer();
		if (econtainer instanceof Equipment)
		{
			equipments_related.add(((Equipment)econtainer).getName());
		}
		else if (econtainer instanceof EquipmentGroup)
		{
			for (Equipment equipment : ((EquipmentGroup)econtainer).getMembers())
			{
				equipments_related.add(equipment.getName());
			}
		}
		
		return equipments_related;
	}

	public static ArrayList<String> getKeywordsOfProperty(EvaluableProperty property)
	{
		ArrayList<String> keywords = new ArrayList<String>();
		
		if(property.eContainer() instanceof Equipment)
		{
			Equipment equipment = (Equipment)property.eContainer();
			keywords.add(equipment.getName());
			
			if(equipment.getInstalled()!=null)
			{
				keywords.add(equipment.getInstalled().getID());
			}
			
		}
		else if (property instanceof Cabin)
		{
			keywords.add(((Cabin)property).getID());
		}
		
		return keywords;
	}
	
	public static ArrayList<String> getKeywordsOfConditions(Condition condition) {
		ArrayList<String> keywords = new ArrayList<String>();
		if(condition != null)
		{
			if (condition instanceof NestedCondition){
				NestedCondition nested_condition = (NestedCondition)condition;
				keywords.addAll(getKeywordsOfConditions(nested_condition.getLeft()));
				keywords.addAll(getKeywordsOfConditions(nested_condition.getRight()));
				if(nested_condition.getProperty()!=null)
				{
					keywords.addAll(getKeywordsOfProperty(nested_condition.getProperty()));
				}
			}
			else
			{
				SingleCondition single_condition = (SingleCondition)condition;
				if (single_condition.getCondition() != null)
				{
					keywords.addAll(getKeywordsOfConditions(single_condition.getCondition()));
				}
				if (single_condition.getProperty()!= null)
				{
					keywords.addAll(getKeywordsOfProperty(single_condition.getProperty()));
				}
			}
		}
		return keywords;
	}

protected static String getMMText(EObject object) {
		
		if(object instanceof EReference) {
			return ((EReference)object).getName();
		}
		else if(object instanceof EStructuralFeature){
			return ((EStructuralFeature)object).getName()+ object.eGet((EStructuralFeature)object);
		}
		else{
			String [] terms = object.eClass().getInstanceClassName().split("\\.");
			if(terms.length==0)
				return "";
			return terms[terms.length-1];
		}	
	}

}
