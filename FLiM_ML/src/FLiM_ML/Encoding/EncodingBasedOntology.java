package FLiM_ML.Encoding;

import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.emf.ecore.EObject;

import FLiM_ML.Encoding.Encoding;
import FLiM_ML.Encoding.UtilsEncodingTCML;
import TrainControlDSL.Equipment;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainModule;
import TrainControlDSL.TrainUnit;
import ontology.TCMLOntology;

public class EncodingBasedOntology extends Encoding{
	
	public EncodingBasedOntology() {
		super();
		ONTOLOGY = new TCMLOntology();
	}
	
@Override
	protected ArrayList<Float> encodeDescription(String description) {
		ArrayList<Float> encoding_description = new ArrayList<Float>(Collections.nCopies(ONTOLOGY.getNumberConcepts(), (float) 0));
				
		ArrayList<String> descriptionTerms = ONTOLOGY.getTerms(description);
		
		int index = -1;
		for (String word : descriptionTerms){
			index = ONTOLOGY.getIndexConcept(word);
 			//int index = index_value[0];
			if(index >= 0){
				encoding_description.set(index, encoding_description.get(index)+1);
			}
		}			
		
		return encoding_description;
	}

	@Override
	protected ArrayList<Float> encodeFragment(EObject model) 
	{
		ArrayList<Float> feature_vector = encodeConceptsFragment(model);			
		feature_vector.addAll(encodeRelationsFragment(model));
		
		return feature_vector;
	}

	private ArrayList<Float> encodeConceptsFragment(EObject model) {
		
		ArrayList<Float> encoding_concepts = new ArrayList<Float>(Collections.nCopies(ONTOLOGY.getNumberConcepts(),(float)0));
		
		TrainUnit train = (TrainUnit) model;
		
		int index = ONTOLOGY.getIndexConcept(train.getName());
		if (index >= 0)
		{
			encoding_concepts.set(index, encoding_concepts.get(index)+1);
		}
			
		for(TrainModule module : train.getHasTrainModules()){
			index = ONTOLOGY.getIndexConcept(module.getID());
			if (index >= 0)
			{
				encoding_concepts.set(index, encoding_concepts.get(index)+1);
			}
		}
	
		for(Equipment equipment : train.getHasEquipments()){
			index = ONTOLOGY.getIndexConcept(equipment.getName());
			if (index >= 0)
			{
				encoding_concepts.set(index, encoding_concepts.get(index)+1);
			}
		}
		
		return encoding_concepts;
	}

	private ArrayList<Float> encodeRelationsFragment(EObject model) {
		
		ArrayList<Float> relations_feature_vector = new ArrayList<Float>(Collections.nCopies(ONTOLOGY.getNumberRelations(),(float)0));
		
		TrainUnit train = (TrainUnit) model;
		int index = -1;
		
		ArrayList<String> equipments_related = new ArrayList<String>();
		for(Rule rule : train.getHasRules())
		{
			equipments_related = UtilsEncodingTCML.getKeywordsOfRule(rule);	
		}
		
		for(int i1=0;i1<equipments_related.size()-1; i1++)
		{
			for(int i2=1; i2<equipments_related.size() ;i2++)
			{
				index = ONTOLOGY.getIndexRelations(equipments_related.get(i1), equipments_related.get(i2));
				if(index != -1)
				{
					relations_feature_vector.set(index, relations_feature_vector.get(index)+1);
				}
			}
		}
		
		return relations_feature_vector;
		
	}

	@Override
	public int getNumberOfFeatures() {
		return ((ONTOLOGY.getNumberConcepts()*2)+ONTOLOGY.getNumberRelations());
	}

}
