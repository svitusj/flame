package FLiM_ML.Encoding;

import java.util.ArrayList;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import FLiM_ML.Encoding.Encoding;
import FLiM_ML.Encoding.UtilsEncodingTCML3;
import TrainControlDSL.Equipment;
import TrainControlDSL.Property;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainModule;
import ontology.RelationConcepts;
import ontology.TCMLOntology;
import ontology.TCMLOntologyExtended;

public class EncodingBasedOnComparison3 extends Encoding{
	
	public final TCMLOntologyExtended ONTOLOGY = new TCMLOntologyExtended();
	
	public ArrayList<Float> encodeDescriptionFragment(String description, EObject model)
	{
		ArrayList<Float> feature_vector = new ArrayList<Float>();
		ArrayList<String> descriptionConcepts = getDescriptionConcepts(description);
		ArrayList<String> descriptionAttributes = getDescriptionAttributes(description);
		ArrayList<RelationConcepts> descriptionRelations = getDescriptionRelations(descriptionConcepts);
		ArrayList<String> fragmentConcepts = getFragmentConcepts(model);
		ArrayList<String> fragmentAttributes = getFragmentAttributes(model);
		ArrayList<RelationConcepts> fragmentRelations = getFragmentRelations(model);

		//1: Compare number of concepts
		// 1/(1+|Number concepts in description - Number of concepts in fragment|)
		feature_vector.add( (1/(1+Math.abs((float)descriptionConcepts.size()-(float)fragmentConcepts.size()))));
		
		//2: Compare number of attributes
		//  1/(1 + |Number of attributes in description - Number of attributes in fragment|)
		feature_vector.add((1/(1+Math.abs((float)descriptionAttributes.size()-(float)fragmentAttributes.size()))));
		
		//3: Compare number of relations
		//  1/(1 + |Number of relations in description - Number of relations in fragment|)
		float numDescriptionRelations = descriptionRelations.size();
		float numFragmentRelations = fragmentRelations.size();
		if(numDescriptionRelations<=0) numDescriptionRelations = 1; //There is one rule as minimum
		if (numFragmentRelations<=0) numFragmentRelations = countNumRules(model); //Rules contains the relations
		feature_vector.add(1/(1+Math.abs(numDescriptionRelations-numFragmentRelations)));
		
		//4: Relevance of concepts
		// (Summation of 1/(1+|a-b|))/c, for each concept in the ontology a=Number of this concept in the description, and b=Number of this concept in the fragment
		float relevanceOfConcepts = 0;
		for (String concept: ONTOLOGY.getConcepts())
		{
			relevanceOfConcepts =  relevanceOfConcepts + UtilsEncodingTCML3.presenceOfConcept(concept, descriptionConcepts, fragmentConcepts);
		}
		feature_vector.add(relevanceOfConcepts/ONTOLOGY.getNumberConcepts());
		
		//5: Relevance of attributes
		// (Summation of 1/(1+|a-b|))/c, for each attribute in the ontology a=Number of this attribute in the description, and b=Number of this attribute in the fragment
		float relevanceOfAttributes = 0;
		for (String attribute: ONTOLOGY.getAttributes())
		{
			relevanceOfAttributes =  relevanceOfAttributes + UtilsEncodingTCML3.presenceOfConcept(attribute, descriptionAttributes, fragmentAttributes);
		}
		feature_vector.add(relevanceOfAttributes/ONTOLOGY.getNumberAttributes());
		
		//6: Relevance of relations
		//(Summation of 1/(1+|a-b|))/c, for each relation in the ontology a=Number of this relation in the description, and b=Number of this relation in the fragment
		float relevanceOfRelations = 0;
		if(descriptionRelations.size()<=0 && fragmentRelations.size()<=0)
		{
			//There are rules but concepts are not related.
			relevanceOfRelations = UtilsEncodingTCML3.presenceOfConceptInRelation(fragmentConcepts,model);
		}
		else
		{
			for ( Entry<Integer, RelationConcepts> entry :ONTOLOGY.getRelations().entrySet())
			{
				RelationConcepts relation = entry.getValue();
				relevanceOfRelations =  relevanceOfRelations + UtilsEncodingTCML3.presenceOfRelation(relation, descriptionRelations, fragmentRelations);
			}
			relevanceOfRelations = relevanceOfRelations/ONTOLOGY.getNumberRelations();
		}
		feature_vector.add(relevanceOfRelations);
		
		return feature_vector;
	}
	
	private float countNumRules(EObject model) {
		float numRules = 0;
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Rule)
			{
				numRules++;
			}
		}
		return numRules;
	}

	private ArrayList<RelationConcepts> getFragmentRelations(EObject model) {
		ArrayList<RelationConcepts> relations = new ArrayList<RelationConcepts>();
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Rule)
			{
				ArrayList<String> keywordsRule = UtilsEncodingTCML3.getKeywordsOfRule((Rule)element);
				for (int i=0; i<keywordsRule.size()-1; i++)
				{
					for (int n=i+1; n<keywordsRule.size();n++)
					{
						String concept1 = keywordsRule.get(i);
						String concept2 = keywordsRule.get(n);
						RelationConcepts relation = ONTOLOGY.containsRelation(concept1, concept2);
						if(relation!=null) relations.add(relation);
					}
				}
			}
		}
		return relations;
	}

	private ArrayList<String> getFragmentAttributes(EObject model) {
		ArrayList<String> attributes = new ArrayList<String>();
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Property)
			{
				if (ONTOLOGY.containsAttribute(((Property)element).getName()))
				{
					attributes.add(ONTOLOGY.getWordRoot(((Property)element).getName()));
				}
			}
		}
		return attributes;
	}

	private ArrayList<String> getFragmentConcepts(EObject model) {
		ArrayList<String> concepts = new ArrayList<String>();
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
		//	System.out.println(getMMText(element));
			String term = "";
			if(element instanceof Equipment)
			{
				term = ((Equipment)element).getName();
			}
			else if (element instanceof TrainModule)
			{
				term = ((TrainModule)element).getID();
			}
			
			if(ONTOLOGY.containsConcept(term)) concepts.add(ONTOLOGY.getWordRoot(term));
		}
		return concepts;
	}

	private ArrayList<RelationConcepts> getDescriptionRelations(ArrayList<String> descriptionConcepts) {
		ArrayList<RelationConcepts> relations = new ArrayList<RelationConcepts>();
		for (int i=0; i<descriptionConcepts.size()-1; i++)
		{
			for (int n=i+1; n<descriptionConcepts.size();n++)
			{
				String concept1 = descriptionConcepts.get(i);
				String concept2 = descriptionConcepts.get(n);
				RelationConcepts relation = ONTOLOGY.containsRelation(concept1, concept2);
				if(relation!=null) relations.add(relation);
			}
		}
		return relations;
	}

	private ArrayList<String> getDescriptionAttributes(String description) {
		ArrayList<String> attributes = new ArrayList<String>();
		for(String term: ONTOLOGY.getTerms(description))
		{
			if (ONTOLOGY.containsAttribute(term)) attributes.add(term);
		}
		return attributes;
	}

	private ArrayList<String> getDescriptionConcepts(String description) {
		ArrayList<String> concepts = new ArrayList<String>();
		for(String term: ONTOLOGY.getTerms(description))
		{
			if (ONTOLOGY.containsConcept(term)) concepts.add(term);
		}
		return concepts;
	}

	@Override
	protected ArrayList<Float> encodeDescription(String description) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ArrayList<Float> encodeFragment(EObject model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumberOfFeatures() {
		return 6;
	}

	protected String getMMText(EObject object) {
		
		if(object instanceof EReference) {
			return ((EReference)object).getName();
		}
		else if(object instanceof EStructuralFeature){
			return ((EStructuralFeature)object).getName();
		}
		else{
			String [] terms = object.eClass().getInstanceClassName().split("\\.");
			if(terms.length==0)
				return "";
			return terms[terms.length-1];
		}	
	}
}
