package FLiM_ML.Encoding;

import java.util.ArrayList;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import FLiM_ML.Encoding.Encoding;
import FLiM_ML.Encoding.UtilsEncodingTCML;
import TrainControlDSL.Equipment;
import TrainControlDSL.Property;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainModule;
import ontology.RelationConcepts;
import ontology.TCMLOntologyExtended;

public class EncodingBasedOnComparison extends Encoding{
	
	public final TCMLOntologyExtended ONTOLOGY = new TCMLOntologyExtended();
	
	public ArrayList<Float> encodeDescriptionFragment(String description, EObject model)
	{
		ArrayList<Float> feature_vector = new ArrayList<Float>();
		ArrayList<String> fragment_concepts = new ArrayList<String>();
		ArrayList<String> fragment_attributes = new ArrayList<String>();
		ArrayList<ArrayList<String>> fragment_relations = new ArrayList<ArrayList<String>>();

		//1: Compare number of concepts
		// 1/(1+|Number concepts in description - Number of concepts in fragment|)
		float num_concepts_description = 0;
		float num_concepts_fragment = 0;
		ArrayList<String> terms = ONTOLOGY.getTerms(description);
		for (int i=0; i<terms.size(); i++)
		{
			if (ONTOLOGY.containsConcept(terms.get(i)))
			{
				num_concepts_description++;
			}
		}
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Equipment)
			{
				if (ONTOLOGY.containsConcept(((Equipment)element).getName()))
				{
					num_concepts_fragment++;
					fragment_concepts.add(ONTOLOGY.getWordRoot(((Equipment)element).getName()));
				}
			}
			else if (element instanceof TrainModule)
			{
				if (ONTOLOGY.containsConcept(((TrainModule)element).getID()))
				{
					num_concepts_fragment++;
					fragment_concepts.add(ONTOLOGY.getWordRoot(((TrainModule)element).getID()));
				}
			}
		}
		feature_vector.add(1/(1+Math.abs(num_concepts_description-num_concepts_fragment)));
		
		//2: Compare number of attributes
		//  1/(1 + |Number of attributes in description - Number of attributes in fragment|)
		float num_attributes_description = 0;
		float num_attributes_fragment = 0;
		terms = ONTOLOGY.getTerms(description);
		for (int i=0; i<terms.size(); i++)
		{
			if (ONTOLOGY.containsAttribute(terms.get(i)))
			{
				num_attributes_description++;
			}
		}
		iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Property)
			{
				if (ONTOLOGY.containsAttribute(((Property)element).getName()))
				{
					num_attributes_fragment++;
					fragment_attributes.add(ONTOLOGY.getWordRoot(((Property)element).getName()));
				}
			}
		}
		feature_vector.add(1/(1+Math.abs(num_attributes_description-num_attributes_fragment)));
		
		//3: Compare number of relations
		//  1/(1 + |Number of relations in description - Number of relations in fragment|)
		float num_rules_description = 0;
		terms = ONTOLOGY.getTerms(description);
		for (Entry<Integer, RelationConcepts> entry :ONTOLOGY.getRelations().entrySet())
		{
			RelationConcepts relation = entry.getValue();
			if(terms.contains(relation.getFirstConcept()) && terms.contains(relation.getSecondConcept())) num_rules_description ++;
		}
		float num_rules_fragment = 0;
		iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Rule)
			{
				ArrayList<String> keywordsRule = UtilsEncodingTCML.getKeywordsOfRule((Rule)element);
				for (Entry<Integer, RelationConcepts> entry :ONTOLOGY.getRelations().entrySet())
				{
					RelationConcepts relation = entry.getValue();
					if(keywordsRule.contains(relation.getFirstConcept()) && keywordsRule.contains(relation.getSecondConcept())) num_rules_fragment++;
				}	
				fragment_relations.add(keywordsRule);
			}
		}
		feature_vector.add(1/(1+Math.abs(num_rules_description-num_rules_fragment)));
		
		//4: Relevance of concepts
		// Summation of 1/(1+|a-b|), for each concept in the ontology a=Number of this concept in the description, and b=Number of this concept in the fragment
		float relevance_of_concepts = 0;
		terms = ONTOLOGY.getTerms(description);
		for (String concept: ONTOLOGY.getConcepts())
		{
			relevance_of_concepts =  relevance_of_concepts + UtilsEncodingTCML.presenceOfConcept(concept, terms, fragment_concepts);
		}
		feature_vector.add(relevance_of_concepts/ONTOLOGY.getNumberConcepts());
		
		//5: Relevance of attributes
		// Summation of 1/(1+|a-b|), for each attribute in the ontology a=Number of this attribute in the description, and b=Number of this attribute in the fragment
		float relevance_of_attributes = 0;
		terms = ONTOLOGY.getTerms(description);
		for (String attribute: ONTOLOGY.getAttributes())
		{
			relevance_of_attributes =  relevance_of_attributes + UtilsEncodingTCML.presenceOfConcept(attribute, terms, fragment_attributes);
		}
		feature_vector.add(relevance_of_attributes/ONTOLOGY.getNumberAttributes());
		
		//6: Relevance of relations
		//Number of relations based on the summation of 1/(1+|a-b|), for each relation in the ontology a=Number of this relation in the description, and b=Number of this relation in the fragment
		float relevance_of_relations = 0;
		terms = ONTOLOGY.getTerms(description);
		for ( Entry<Integer, RelationConcepts> entry :ONTOLOGY.getRelations().entrySet())
		{
			RelationConcepts relation = entry.getValue();
			relevance_of_relations =  relevance_of_relations + UtilsEncodingTCML.presenceOfRelation(relation.getFirstConcept(), relation.getSecondConcept(), terms, fragment_relations);
		}
		feature_vector.add(relevance_of_relations/ONTOLOGY.getNumberRelations());
		
		return feature_vector;
	}
	
	@Override
	protected ArrayList<Float> encodeDescription(String description) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ArrayList<Float> encodeFragment(EObject model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumberOfFeatures() {
		return 6;
	}

}
