package FLiM_ML.Encoding;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;

import ontology.TCMLOntology;

public abstract class Encoding {
	
	protected TCMLOntology ONTOLOGY;
	
	protected abstract ArrayList<Float> encodeDescription(String description);
	protected abstract ArrayList<Float> encodeFragment(EObject model);
	
	public ArrayList<Float> encodeDescriptionFragment(String description, EObject model)
	{
		ArrayList<Float> feature_vector = new ArrayList<Float>();
		feature_vector.addAll(encodeDescription(description));
		feature_vector.addAll(encodeFragment(model));
		
		return feature_vector;
	}
	
	public abstract int getNumberOfFeatures();
}
