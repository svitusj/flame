package FLiM_ML.Encoding;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;

import FLiM_ML.Encoding.Encoding;
import TrainControlDSL.Equipment;
import TrainControlDSL.InvocatedOrder;
import TrainControlDSL.Order;
import TrainControlDSL.OrderInhibitionRule;
import TrainControlDSL.OrderInvocationRule;
import TrainControlDSL.Property;
import TrainControlDSL.PropertySynchronizationRule;
import TrainControlDSL.Rule;
import TrainControlDSL.TrainUnit;

public class EncodingManual extends Encoding{

	public ArrayList<Float> encodeDescriptionFragment(String description, EObject model)
	{
		ArrayList<Float> feature_vector = new ArrayList<Float>();
		feature_vector.addAll(encodeDescription(description,model));
		feature_vector.addAll(encodeFragment(model));
		
		return feature_vector;
	}
	
	
	@Override
	protected ArrayList<Float> encodeDescription(String description) {
		return null;
	}

	private ArrayList<Float> encodeDescription(String description, EObject model) {

		ArrayList<Float> feature_vector = new ArrayList<Float>();
		
		TrainUnit train  = (TrainUnit) model;
		ArrayList<String> descriptionTerms = getTerms(description);
		
		//Equipments
		int numEquipments = 0;
		ArrayList<String> properties = new ArrayList<String>();
		ArrayList<String> orders = new ArrayList<String>();
		for(Equipment equipment: train.getHasEquipments())
		{
			for(String term:descriptionTerms)
			{
				if(term.equals(getWordRoot(equipment.getName())))
				{
					numEquipments++;
				}
						
			}
			for(Property property:equipment.getHasProperties())
			{
				properties.add(getWordRoot(property.getName()));
			}
			for(Order order: equipment.getHasOrders())
			{
				orders.add(getWordRoot(order.getName()));
			}
		}
		feature_vector.add((float) numEquipments);
		
		//Rules
		feature_vector.add((float)1);
		
		//Properties
		int numProperties = 0;
		for(String property : properties)
		{
			for(String term: descriptionTerms)
			{
				if (term.equals(property))
				{
					numProperties++;
				}
			}
		}
		feature_vector.add((float)numProperties);
		
		//Orders
		int numOrders = 0;
		for(String order: orders)
		{
			for(String term:descriptionTerms)
			{
				if(term.equals(getWordRoot(order)))
				{
					numOrders++;
				}
						
			}
		}
		feature_vector.add((float)numOrders);
		
		return feature_vector;
	}

	private ArrayList<String> getTerms (String sentence)
	{
		ArrayList<String> terms = new ArrayList<String>();
		
		sentence = sentence.replace(".", "");
		sentence = sentence.replace(",", "");

		String[] words = sentence.split(" ");
		for (int i=0;i<words.length-1;i++)
		{
			terms.add(getWordRoot(words[i]));
			terms.add(getWordRoot(words[i])+"_"+getWordRoot(words[i+1]));
		}
		terms.add(getWordRoot(words[words.length-1]));
		return terms;
	}
	
	private static String getWordRoot (String word)
	{
		if(word != null)
		{
			if (!word.isEmpty())
			{
				word = word.split(" ")[0];
				int index_last_character = word.length()-1;
				
				//If last character is a digit
				while (Character.isDigit(word.charAt(index_last_character))){
					word = word.substring(0, index_last_character);
					index_last_character--;
				}
				
				//If last character is 's'
				if (word.charAt(index_last_character) == 's'){
					word = word.substring(0, index_last_character);
				}
				
				return word.toLowerCase();
			}
		}
		
		return null;

	}
	
	@Override
	protected ArrayList<Float> encodeFragment(EObject model) {
		
		ArrayList<Float> feature_vector = new ArrayList<Float>();
		
		TrainUnit train = (TrainUnit) model;
		//Equipments
		feature_vector.add((float) train.getHasEquipments().size());
		
		//Exists some rule
		if(train.getHasRules().size()>0) feature_vector.add((float) 1);
		else feature_vector.add((float) 0);
		
		//Rules
		feature_vector.add((float)train.getHasRules().size());
		
		int numProperties = 0;
		int numOrders = 0;
		for(Equipment equipment: train.getHasEquipments())
		{
			for(Property property:equipment.getHasProperties())
			{
				numProperties++;
			}
			for(Order order:equipment.getHasOrders())
			{
				numOrders++;
			}
		}
		//Properties
		feature_vector.add((float)numProperties);
		//Orders
		feature_vector.add((float)numOrders);
		
		
		int numTriggers = 0;
		int numConditions = 0;
		int numActions = 0;
		for(Rule rule: train.getHasRules())
		{
			if(rule.getHasTrigger()!=null) numTriggers++;
			if(rule instanceof OrderInhibitionRule)
			{
				if(((OrderInhibitionRule)rule).getHasConditions()!=null) numConditions++;
				if(((OrderInhibitionRule)rule).getInhibitableElement()!=null) numActions++;
			}
			else if (rule instanceof OrderInvocationRule)
			{
				if(((OrderInvocationRule)rule).getHasPhysicalEffects().size()>0)
				{
					for(InvocatedOrder order:((OrderInvocationRule)rule).getHasPhysicalEffects())
					{
						if(order.getHasConditions()!=null) numConditions++;
						if(order.getOrder()!=null) numActions++;
					}
				}
			}
			else if (rule instanceof PropertySynchronizationRule)
			{
				if(((PropertySynchronizationRule)rule).getHasConditions()!=null) numConditions++;
				if(((PropertySynchronizationRule)rule).getProperty()!=null) numActions++;
			}
		}
		//Trigger vs rules
		feature_vector.add((float)numTriggers);
		//Conditions vs rules
		feature_vector.add((float)numConditions);
		//Actions vs rules
		feature_vector.add((float)numActions);
		
		return feature_vector;
	}

	@Override
	public int getNumberOfFeatures() {
		return 12;
	}

}
