package FLiM_ML.Encoding;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

import TrainControlDSL.Cabin;
import TrainControlDSL.Condition;
import TrainControlDSL.Equipment;
import TrainControlDSL.EquipmentGroup;
import TrainControlDSL.EvaluableProperty;
import TrainControlDSL.InhibitableElement;
import TrainControlDSL.InvocatedOrder;
import TrainControlDSL.NestedCondition;
import TrainControlDSL.Order;
import TrainControlDSL.OrderInhibitionRule;
import TrainControlDSL.OrderInvocationRule;
import TrainControlDSL.PropertySynchronizationRule;
import TrainControlDSL.Rule;
import TrainControlDSL.SingleCondition;
import ontology.TCMLOntology;

public class UtilsEncodingTCML {
	
	public static Float presenceOfConcept(String concept, ArrayList<String> description_words, ArrayList<String> fragment_concepts) {
		float num_concept_description = 0;
		float num_concept_fragment = 0;
		for (int i=0; i<description_words.size(); i++)
		{
			if (concept.equals(description_words.get(i)))
			{
				num_concept_description++;
			}
		}
		
		for (int i=0;i<fragment_concepts.size(); i++)
		{
			if(concept.equals(fragment_concepts.get(i)))
			{
				num_concept_fragment++;
			}
		}

		return 1/(1+Math.abs(num_concept_description-num_concept_fragment));
		
	}
	
	public static Float presenceOfRelation(String concept1, String concept2, ArrayList<String> description_words, ArrayList<ArrayList<String>> fragment_relations) {
		
		float numRelationDescription = 0;
		float numRealtionFragment = 0;
		if (description_words.contains(concept1) && description_words.contains(concept2))
		{
			numRelationDescription++;
		}
		
		for (int n=0;n<fragment_relations.size(); n++)
		{
			if (fragment_relations.get(n).contains(concept1) && fragment_relations.get(n).contains(concept2))
			{
				numRealtionFragment++;
				break;
			}
		}
		return 1/(1+Math.abs(numRelationDescription-numRealtionFragment));
	}
	
	public static ArrayList<String> getKeywordsOfRule(Rule rule)
	{
		ArrayList<String> equimpents_related = new ArrayList<String>();
		
		//TRIGGER
		if(rule.getHasTrigger()!=null) 
		{
			if(rule.getHasTrigger().getProperty()!=null)
			{
				equimpents_related.addAll(getKeywordsOfProperty(rule.getHasTrigger().getProperty()));
			}
		}
			
		//SYNCHRONIZATION RULE
		if (rule instanceof PropertySynchronizationRule)
		{
			
			if(((PropertySynchronizationRule) rule).getProperty()!=null)
			{
				equimpents_related.addAll(getKeywordsOfProperty(((PropertySynchronizationRule)rule).getProperty()));
			}
			
			Condition condition = ((PropertySynchronizationRule) rule).getHasConditions();
			equimpents_related.addAll(getKeywordsOfConditions(condition));
						
		}
		//INVOCATION RULE
		else if (rule instanceof OrderInvocationRule){
			for (InvocatedOrder invocated_order : ((OrderInvocationRule)rule).getHasPhysicalEffects())
			{
				if(invocated_order.getOrder()!=null)
				{
					equimpents_related.addAll(getKeywordsOfOrders(invocated_order.getOrder()));
				}
				
				Condition condition = (invocated_order.getHasConditions());
				equimpents_related.addAll(getKeywordsOfConditions(condition));
			}
		}
		//INHIBITION RULE
		else
		{
			InhibitableElement inhibitable_element = ((OrderInhibitionRule)rule).getInhibitableElement();
			if(inhibitable_element !=null)
			{
				if (inhibitable_element instanceof Equipment)
				{
					equimpents_related.add(((Equipment)inhibitable_element).getName());
				}
				else if (inhibitable_element instanceof Order)
				{
					equimpents_related.addAll(getKeywordsOfOrders((Order)inhibitable_element));
				}
			}
			
			Condition condition = (((OrderInhibitionRule)rule).getHasConditions());
			equimpents_related.addAll(getKeywordsOfConditions(condition));
		}
		
		//Remove duplicated values
		equimpents_related = (ArrayList<String>) equimpents_related.stream().distinct().collect(Collectors.toList());
		
		//Format keywords in lower case and without numbers
		for(int i=0;i<equimpents_related.size();i++)
		{
			equimpents_related.set(i, TCMLOntology.getWordRoot(equimpents_related.get(i)));
		}
		
		return equimpents_related;

	}
	
	public static ArrayList<String> getKeywordsOfOrders(Order order) 
	{	
		ArrayList<String> equipments_related = new ArrayList<String>();
		
		EObject econtainer = order.eContainer();
		if (econtainer instanceof Equipment)
		{
			equipments_related.add(((Equipment)econtainer).getName());
		}
		else if (econtainer instanceof EquipmentGroup)
		{
			for (Equipment equipment : ((EquipmentGroup)econtainer).getMembers())
			{
				equipments_related.add(equipment.getName());
			}
		}
		
		return equipments_related;
	}

	public static ArrayList<String> getKeywordsOfProperty(EvaluableProperty property)
	{
		ArrayList<String> keywords = new ArrayList<String>();
		
		if(property.eContainer() instanceof Equipment)
		{
			Equipment equipment = (Equipment)property.eContainer();
			keywords.add(equipment.getName());
			
			if(equipment.getInstalled()!=null)
			{
				keywords.add(equipment.getInstalled().getID());
			}
			
		}
		else if (property instanceof Cabin)
		{
			keywords.add(((Cabin)property).getID());
		}
		
		return keywords;
	}
	
	public static ArrayList<String> getKeywordsOfConditions(Condition condition) {
		ArrayList<String> keywords = new ArrayList<String>();
		if(condition != null)
		{
			if (condition instanceof NestedCondition){
				NestedCondition nested_condition = (NestedCondition)condition;
				keywords.addAll(getKeywordsOfConditions(nested_condition.getLeft()));
				keywords.addAll(getKeywordsOfConditions(nested_condition.getRight()));
				if(nested_condition.getProperty()!=null)
				{
					keywords.addAll(getKeywordsOfProperty(nested_condition.getProperty()));
				}
			}
			else
			{
				SingleCondition single_condition = (SingleCondition)condition;
				if (single_condition.getCondition() != null)
				{
					keywords.addAll(getKeywordsOfConditions(single_condition.getCondition()));
				}
				if (single_condition.getProperty()!= null)
				{
					keywords.addAll(getKeywordsOfProperty(single_condition.getProperty()));
				}
			}
		}
		return keywords;
	}

}
