package MLTechniques;

public interface MLTechnique {

	public void tuningClassifier(String filespath, String outputFilepath);
	public void trainClassifier(String filespath);
	public void crossValidation(String filespath);
	public void testUsingClassifier(String filespath);
	public void setConfiguration(String filespath);
}
