package MLTechniques;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.deeplearning4j.arbiter.MultiLayerSpace;
import org.deeplearning4j.arbiter.conf.updater.SgdSpace;
import org.deeplearning4j.arbiter.layers.DenseLayerSpace;
import org.deeplearning4j.arbiter.layers.LSTMLayerSpace;
import org.deeplearning4j.arbiter.layers.OutputLayerSpace;
import org.deeplearning4j.arbiter.optimize.api.CandidateGenerator;
import org.deeplearning4j.arbiter.optimize.api.OptimizationResult;
import org.deeplearning4j.arbiter.optimize.api.ParameterSpace;
import org.deeplearning4j.arbiter.optimize.api.data.DataProvider;
import org.deeplearning4j.arbiter.optimize.api.saving.ResultReference;
import org.deeplearning4j.arbiter.optimize.api.saving.ResultSaver;
import org.deeplearning4j.arbiter.optimize.api.termination.MaxCandidatesCondition;
import org.deeplearning4j.arbiter.optimize.api.termination.MaxTimeCondition;
import org.deeplearning4j.arbiter.optimize.api.termination.TerminationCondition;
import org.deeplearning4j.arbiter.optimize.config.OptimizationConfiguration;
import org.deeplearning4j.arbiter.optimize.generator.RandomSearchGenerator;
import org.deeplearning4j.arbiter.optimize.parameter.continuous.ContinuousParameterSpace;
import org.deeplearning4j.arbiter.optimize.parameter.discrete.DiscreteParameterSpace;
import org.deeplearning4j.arbiter.optimize.parameter.integer.IntegerParameterSpace;
import org.deeplearning4j.arbiter.optimize.runner.IOptimizationRunner;
import org.deeplearning4j.arbiter.optimize.runner.LocalOptimizationRunner;
import org.deeplearning4j.arbiter.saver.local.FileModelSaver;
import org.deeplearning4j.arbiter.scoring.impl.EvaluationScoreFunction;
import org.deeplearning4j.arbiter.task.MultiLayerNetworkTaskCreator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.LSTM;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.Constants;
import data.NNDataProvider;

public class RNN implements MLTechnique{

    private final int batchSize = 10;
    private final int printIterationsNum = 20;
    private final int nEpochs = 4;
    private final int seed = 42;
    
	private int numOfFeatures;
	private int numOfClasses;
	
	private MultiLayerConfiguration defaultConf;
	private MultiLayerConfiguration conf;
	
	private MultiLayerNetwork model;
	
	private DataNormalization normalizer;
	
	public RNN(int numOfFeatures, int numOfClasses)//NNConfiguration nnConfiguration)
	{
		super();
		
		//org.apache.log4j.BasicConfigurator.configure();

		this.numOfFeatures = numOfFeatures;
		this.numOfClasses = numOfClasses;
		
		 defaultConf = new NeuralNetConfiguration.Builder()
				.seed(seed)
	            .trainingWorkspaceMode(WorkspaceMode.SEPARATE)
	            .activation(Activation.TANH)
	            .weightInit(WeightInit.XAVIER)
	            .updater(new Adam(2e-2))
	            .l2(1e-5)
	            .list()
	            .layer(0, new LSTM.Builder().nIn(numOfFeatures).nOut(200)
	                .build())
	            .layer(1, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
	                .activation(Activation.SOFTMAX)
	                .nIn(200).nOut(numOfClasses).build())
	            .backprop(true).pretrain(false)
	            .build();

	}

	@Override
	public void testUsingClassifier(String filespath) 
	{
		NNDataProvider dataProvider = new NNDataProvider(batchSize, numOfClasses, numOfFeatures, filespath, filespath);
		testUsingClassifier(dataProvider, filespath);		
	}
	
	private void testUsingClassifier(DataProvider dataProvider, String filespath)
	{          
			DataSetIterator testIter= (DataSetIterator) dataProvider.testData(null);
	                    
            Evaluation eval = new Evaluation(numOfClasses);
            //testIter.reset();
            ((NNDataProvider)dataProvider).setBatchSize(1);
			testIter= (DataSetIterator) dataProvider.testData(null);

			String ranking = "";
			int index = 0;
            while(testIter.hasNext()) {
                DataSet t = testIter.next();
                INDArray features = t.getFeatures();
                INDArray labels = t.getLabels();
              //  System.out.println("labels: "+labels.toString());
                INDArray predicted = model.output(features, false);
               
                //System.out.println("predicted: "+predicted.toString());
                eval.eval(labels, predicted);
                ranking = ranking+"1"+"\t"+index+"\t"+ retrieveResults(predicted)+"\n";
                index++;
            }
            File file = new File(filespath+Constants.FILENAME_RANKING);
            FileOutputStream out;
			try {
				out = new FileOutputStream(file);
	            out.write(ranking.getBytes());
	            out.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	private int retrieveResults(INDArray predicted) {
		float[] predictedProbabilities = predicted.data().asFloat();
		float maxValue=-1;
		int index = -1;
		for(int i=0; i<predictedProbabilities.length; i++)
		{
			if(maxValue<predictedProbabilities[i]) 
			{
				maxValue=predictedProbabilities[i];
				index = i;
			}
		}
		return index;
	}

	@Override
	public void trainClassifier(String filespath) {
		NNDataProvider dataProvider = new NNDataProvider(batchSize, numOfClasses, numOfFeatures, filespath, filespath);
		
		setConfiguration(filespath);
		
		trainClassifier(dataProvider);
	}
	
	private void trainClassifier(DataProvider dataProvider)
	{
		DataSetIterator trainIter  = (DataSetIterator) dataProvider.trainData(null);
		/*normalizer = new NormalizerMinMaxScaler(0, 1);
		normalizer.fitLabel(true);
		normalizer.fit(trainIter);
		trainIter.setPreProcessor(normalizer);*/

		System.out.println("Build model....");
    
        model = new MultiLayerNetwork(conf);
        model.init();
        model.setListeners(new ScoreIterationListener(printIterationsNum));
        model.fit(trainIter,nEpochs);
             
        System.out.println("Finished...");
	}

	@Override
	public void tuningClassifier(String filespath, String outputFilepath) {
        
		//Data
		String filespathKfold = filespath + 0 + "/";
		NNDataProvider dataProvider = new NNDataProvider(batchSize, numOfClasses, numOfFeatures, filespathKfold, filespath);

        //Activation Functions 
       DiscreteParameterSpace<Activation> activationSpace = new DiscreteParameterSpace(new Activation[]{Activation.HARDSIGMOID, Activation.SIGMOID,Activation.TANH,Activation.HARDTANH,Activation.RELU,Activation.RRELU}); 
        
        //Weight Initialization
       ParameterSpace<WeightInit> weigthInitSpace = new DiscreteParameterSpace(new WeightInit[]{WeightInit.NORMAL,WeightInit.XAVIER,WeightInit.RELU,WeightInit.SIGMOID_UNIFORM,WeightInit.DISTRIBUTION});
        
        //Updater
       ParameterSpace<Double> learningRateHyperparam = new ContinuousParameterSpace(0.0001, 0.1);  //Values will be generated uniformly at random between 0.0001 and 0.1 (inclusive)
        
        //Regularization 
        //L2 - 0.0001
        //Dropout 
        //ParameterSpace<Double> dropOut = new ContinuousParameterSpace(0, 0.7);
        
        //Layer Size
       ParameterSpace<Integer> layerSize = new IntegerParameterSpace(128,256);
        
        //LossFunctions
       DiscreteParameterSpace<LossFunctions.LossFunction> lossFunctionSpace = new DiscreteParameterSpace(new LossFunctions.LossFunction[]{LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD, LossFunctions.LossFunction.MCXENT, LossFunctions.LossFunction.L2});
        
        MultiLayerSpace hyperparameterSpace = new MultiLayerSpace.Builder()
        		.seed(seed)
	            .trainingWorkspaceMode(WorkspaceMode.SEPARATE)
	            .activation(activationSpace)
	            .weightInit(weigthInitSpace)
	            .updater(new SgdSpace(learningRateHyperparam))
	            .l2(0.0001)
                .addLayer(new LSTMLayerSpace.Builder()
                    .nIn(numOfFeatures)  
                    .activation(activationSpace)
                    .nOut(layerSize)
                    .build())
                .addLayer(new OutputLayerSpace.Builder()
                	.nIn(layerSize)
	    	    	.nOut(numOfClasses)
                    .activation(Activation.SOFTMAX)
                    .lossFunction(lossFunctionSpace)
                    .build())
                .backprop(true).pretrain(false)
                .build();
        
        
        //Configuration options
        //How are we going to generate candidates? (random search or grid search)
        CandidateGenerator candidateGenerator = new RandomSearchGenerator(hyperparameterSpace, null);   
	   
        //How we are going to save the models that are generated and tested?
       String baseSaveDirectory = "arbiterExample/";
       File f = new File(baseSaveDirectory);
       if (f.exists()) f.delete();
       f.mkdir();
       ResultSaver modelSaver = new FileModelSaver(baseSaveDirectory);
       
       	//What are we actually trying to optimize?
       	EvaluationScoreFunction eval = new EvaluationScoreFunction(Evaluation.Metric.F1);
	  
       
       //When should we stop searching? 
       TerminationCondition[] terminationConditions = {
           new MaxTimeCondition(2, TimeUnit.MINUTES),
           new MaxCandidatesCondition(50)};
	   
       OptimizationConfiguration configuration = new OptimizationConfiguration.Builder()
            .candidateGenerator(candidateGenerator)
            .dataProvider(dataProvider)
            .modelSaver(modelSaver)
            .scoreFunction(eval)
            .terminationConditions(terminationConditions)
            .build();

	    		 		
	   IOptimizationRunner runner = new LocalOptimizationRunner(configuration, new MultiLayerNetworkTaskCreator());

	   //Start the hyperparameter optimization
	   runner.execute();
	
	 //  String s = "Best score: " + runner.bestScore() + "\n" + "Index of model with best score: " + runner.bestScoreCandidateIndex() + "\n" + "Number of configurations evaluated: " + runner.numCandidatesCompleted() + "\n";
	   //System.out.println(s);


		//Get all results, and print out details of the best result:
	    int indexOfBestResult = runner.bestScoreCandidateIndex();
	    List<ResultReference> allResults = runner.getResults();
	
	    OptimizationResult bestResult;
		try {
			bestResult = allResults.get(indexOfBestResult).getResult();
			MultiLayerNetwork bestModel = (MultiLayerNetwork) bestResult.getResultReference().getResultModel();
			//System.out.println("\n\nConfiguration of best model:\n");
		    System.out.println(bestModel.getLayerWiseConfigurations().toJson());
		    ProjectHelper.getInstance().createFile(bestModel.getLayerWiseConfigurations().toJson(),outputFilepath,Constants.FILENAME_TUNING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  	
		
	}
	
	@Override
	public void crossValidation(String filespath) {
		
		for(int n=0; n<Constants.KFLOD; n++)
		{
			String filespathKfold = filespath+n+"/";
			NNDataProvider cvDataProvider = new NNDataProvider(batchSize, numOfClasses, numOfFeatures, filespathKfold, filespath);
			
			setConfiguration(filespath);
			trainClassifier(cvDataProvider);
			testUsingClassifier(cvDataProvider,filespathKfold);
		}
	}

	@Override
	public void setConfiguration(String filespath) {
		try {
			File tuningFile = new File(filespath+Constants.FILENAME_TUNING);
			if(tuningFile.exists())
			{
				String tuningConf = new String(Files.readAllBytes(Paths.get(tuningFile.getAbsolutePath())));
				conf =	MultiLayerConfiguration.fromJson(tuningConf);
			}
			else
			{
				conf= defaultConf;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
