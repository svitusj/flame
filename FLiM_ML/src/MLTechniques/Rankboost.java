package MLTechniques;

import java.util.ArrayList;
import java.util.Arrays;

import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.Constants;
import ciir.umass.edu.eval.Evaluator;
import ciir.umass.edu.features.FeatureManager;
import flame.GO.core.BitSet;

public class Rankboost implements MLTechnique{

	private int rounds = 300;
	private int threshold = 10;
		
	public Rankboost(int rounds, int threshold) {
		super();
		this.rounds = rounds;
		this.threshold = threshold;
	}

	@Override
	public void testUsingClassifier(String filespath) {
		String args2[] = { "-load", filespath + Constants.FILENAME_CLASSIFIER,
						   "-feature",filespath+Constants.FILENAME_FEATURES,
						   "-rank", filespath + Constants.FILENAME_TESTING, 
						   "-score", filespath + Constants.FILENAME_RANKING,
						   "-norm","zscore",
						   "-round", ""+rounds,
							"-tc", ""+threshold,
						   "-silent" };
		Evaluator.main(args2);
	}

	@Override
	public void trainClassifier(String filespath) {
		String args1[] = { "-train", filespath + Constants.FILENAME_TRAINING,
						   "-feature",filespath+Constants.FILENAME_FEATURES,
						   "-test", filespath + Constants.FILENAME_TESTING, 
						   "-ranker", "2", 
						   "-metric2t", "ERR@10",
						   "-round","200",
						   "-norm","zscore",
						   "-round", ""+rounds,
							"-tc", ""+threshold,
						   "-save", filespath + Constants.FILENAME_CLASSIFIER,
						   "-silent" };
		Evaluator.main(args1);	
		
	}

	@Override
	public void tuningClassifier(String filespath, String outputFilepath) {
		ArrayList<Integer> roundsOptions = new ArrayList<Integer>(Arrays.asList(100,200,300,400,500));
		ArrayList<Integer> thersholdsOptions = new ArrayList<Integer>(Arrays.asList(2,4,6,8,10));
		
		float currentFMeasure = 0;
		int bestRound = 0;
		int bestThreshold = 0;
		for (int indexRounds=0;indexRounds<roundsOptions.size();indexRounds++)
		{
			for (int indexThersholds=0; indexThersholds<thersholdsOptions.size(); indexThersholds++)
			{
				System.out.println("Round: "+roundsOptions.get(indexRounds)+"  Thershold: "+thersholdsOptions.get(indexThersholds));
				/*String args1[] = { "-train", filespath + Constants.FILENAME_TRAINING,
						   "-feature",filespath+Constants.FILENAME_FEATURES,
						   "-ranker", "2",
						   "-round", ""+rounds.get(indexRounds),
						   "-norm","zscore",
						   "-tc", ""+thersholds.get(indexThersholds),
						   "-kcv", "4",
						   "-kcvmd", filespath, 
						   "-metric2t", "NDCG@10",
						   "-silent" };
				Evaluator.main(args1);*/
				
				//Cross-validation
				rounds = roundsOptions.get(indexRounds);
				threshold = thersholdsOptions.get(indexThersholds);
				crossValidation(filespath);
				
				System.out.println(currentFMeasure);

				//Retrieve cross-validation results
				ProjectHelper project = ProjectHelper.getInstance();
				float meanFMeasure = project.retrieveScores();
					
				if(meanFMeasure>currentFMeasure)
				{
					currentFMeasure = meanFMeasure;
					bestRound = roundsOptions.get(indexRounds);
					bestThreshold = thersholdsOptions.get(indexThersholds);
				}
			}
		}
		
		rounds = bestRound;
		threshold = bestThreshold;
		String fileContent = "Rounds:"+rounds+"\nThreshold:"+threshold;
		ProjectHelper.getInstance().createFile(fileContent, outputFilepath, Constants.FILENAME_TUNING);
		System.out.println(rounds+"  "+threshold);
	}
	
	@Override
	public void crossValidation(String filespath) {
		for(int n=0; n<Constants.KFLOD; n++)
		{
			String filespathKfold = filespath+n+"/";
			//> java -jar bin/RankLib.jar -train MQ2008/Fold1/train.txt -test MQ2008/Fold1/test.txt -validate MQ2008/Fold1/vali.txt -ranker 6 -metric2t NDCG@10 -metric2T ERR@10 -save mymodel.txt
			//String args1[] = {"-train", project.getLocation().toString()+"/temps/train_kfold_"+i+".txt", "-test",project.getLocation().toString()+"/temps/test_kfold_"+i+".txt","-ranker", "2","-feature",project.getLocation().toString()+"/temps/"+stop+"/features.txt","-metric2t", "ERR@10","-save",project.getLocation().toString()+"/temps/"+stop+"/model_"+i+".txt", "-silent"};
			String args1[] = {"-train", filespathKfold+Constants.FILENAME_TRAINING, 
					"-test",filespathKfold+Constants.FILENAME_TESTING,
					"-ranker", "2",
					"-round", ""+rounds,
					"-norm","zscore",
					"-tc", ""+threshold,
					"-feature",filespath+Constants.FILENAME_FEATURES,
					"-metric2t", "ERR@10",
					"-save",filespathKfold+"model_"+n+".txt", 
					"-silent"};
			Evaluator.main(args1);
			//> java -jar bin/RankLib.jar -load mymodel.txt -rank MQ2008/Fold1/test.txt -score myscorefile.txt
			String args2[] ={"-load", filespathKfold+"model_"+n+".txt", 
					"-round", ""+rounds,
					"-norm","zscore",
					"-tc", ""+threshold,
					"-rank",filespathKfold+Constants.FILENAME_TESTING,
					"-feature",filespath+Constants.FILENAME_FEATURES,
					"-score",filespathKfold+Constants.FILENAME_RANKING,
					"-silent"};
			Evaluator.main(args2);
		}
	}

	@Override
	public void setConfiguration(String filespath) {
		// TODO Auto-generated method stub
		
	}
}
