{
  "backprop" : true,
  "backpropType" : "Standard",
  "cacheMode" : "NONE",
  "confs" : [ {
    "cacheMode" : "NONE",
    "epochCount" : 0,
    "iterationCount" : 0,
    "l1ByParam" : {
      "b" : 0.0,
      "W" : 0.0
    },
    "l2ByParam" : {
      "b" : 0.0,
      "W" : 1.0E-4
    },
    "layer" : {
      "@class" : "org.deeplearning4j.nn.conf.layers.DenseLayer",
      "activationFn" : {
        "@class" : "org.nd4j.linalg.activations.impl.ActivationHardTanH"
      },
      "biasInit" : 0.0,
      "biasUpdater" : null,
      "constraints" : null,
      "dist" : {
        "type" : "org.deeplearning4j.nn.conf.distribution.NormalDistribution",
        "mean" : 0.0,
        "std" : 1.0
      },
      "gradientNormalization" : "None",
      "gradientNormalizationThreshold" : 1.0,
      "hasBias" : true,
      "idropout" : null,
      "iupdater" : {
        "@class" : "org.nd4j.linalg.learning.config.Sgd",
        "learningRate" : 0.07602528767346486
      },
      "l1" : 0.0,
      "l1Bias" : 0.0,
      "l2" : 1.0E-4,
      "l2Bias" : 0.0,
      "layerName" : "layer0",
      "nin" : 7,
      "nout" : 256,
      "weightInit" : "DISTRIBUTION",
      "weightNoise" : null
    },
    "maxNumLineSearchIterations" : 5,
    "miniBatch" : true,
    "minimize" : true,
    "optimizationAlgo" : "STOCHASTIC_GRADIENT_DESCENT",
    "pretrain" : false,
    "seed" : 42,
    "stepFunction" : null,
    "variables" : [ "W", "b" ]
  }, {
    "cacheMode" : "NONE",
    "epochCount" : 0,
    "iterationCount" : 0,
    "l1ByParam" : {
      "b" : 0.0,
      "W" : 0.0
    },
    "l2ByParam" : {
      "b" : 0.0,
      "W" : 1.0E-4
    },
    "layer" : {
      "@class" : "org.deeplearning4j.nn.conf.layers.OutputLayer",
      "activationFn" : {
        "@class" : "org.nd4j.linalg.activations.impl.ActivationSoftmax"
      },
      "biasInit" : 0.0,
      "biasUpdater" : null,
      "constraints" : null,
      "dist" : {
        "type" : "org.deeplearning4j.nn.conf.distribution.NormalDistribution",
        "mean" : 0.0,
        "std" : 1.0
      },
      "gradientNormalization" : "None",
      "gradientNormalizationThreshold" : 1.0,
      "hasBias" : true,
      "idropout" : null,
      "iupdater" : {
        "@class" : "org.nd4j.linalg.learning.config.Sgd",
        "learningRate" : 0.07602528767346486
      },
      "l1" : 0.0,
      "l1Bias" : 0.0,
      "l2" : 1.0E-4,
      "l2Bias" : 0.0,
      "layerName" : "layer1",
      "lossFn" : {
        "@class" : "org.nd4j.linalg.lossfunctions.impl.LossMCXENT",
        "softmaxClipEps" : 1.0E-10,
        "configProperties" : false
      },
      "nin" : 256,
      "nout" : 10,
      "weightInit" : "DISTRIBUTION",
      "weightNoise" : null
    },
    "maxNumLineSearchIterations" : 5,
    "miniBatch" : true,
    "minimize" : true,
    "optimizationAlgo" : "STOCHASTIC_GRADIENT_DESCENT",
    "pretrain" : false,
    "seed" : 42,
    "stepFunction" : null,
    "variables" : [ "W", "b" ]
  } ],
  "epochCount" : 1,
  "inferenceWorkspaceMode" : "ENABLED",
  "inputPreProcessors" : { },
  "iterationCount" : 101,
  "pretrain" : false,
  "tbpttBackLength" : 20,
  "tbpttFwdLength" : 20,
  "trainingWorkspaceMode" : "SEPARATE"
}