package examples;

/**
 * This class contains the already processed strings used to implement the Gold Trucks Example.
 * @author Ra�l Lape�a
 */
public class GoldTrucks 
{
	/* Variables for the documents */
	public static String d1 = "shipment of gold damaged in a fire";
	public static String d2 = "delivery of silver arrived in a silver truck";
	public static String d3 = "shipment of gold arrived in a truck";
	
	/* Variable for the query */
	public static String query = "gold silver truck";
}
