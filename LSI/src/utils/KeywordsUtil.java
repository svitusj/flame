package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class contains the functionality needed to extract keywords from a set of already processed documents and query.
 * @author Ra�l
 */
public class KeywordsUtil 
{
	/**
	 * Method that removes duplicates from the keywords list
	 * @param keywordsList Keywords list from which duplicates must be removed
	 */
	private static void removeDuplicateKeywords(List<String> keywordsList)
	{
		// Create a Set -> Sets don't allow duplicates!
		// Move the words to the set and back to the ArrayList
		Set<String> keywordSet = new HashSet<>();
		keywordSet.addAll(keywordsList);
		keywordsList.clear();
		keywordsList.addAll(keywordSet);
	}
	
	/**
	 * Method that extracts the keywords from the documents and the query and removes duplicates. User can choose whether to order the keywords alphabetically or to keep them unordered.
	 * Extracted keywords are stored in the extractedKeywords variable, which can be accessed through its getter.
	 * @param collection The collection of Strings that compose a problem (documents and query).
	 * @param ordered Variable used to decide whether to order the keywords alphabetically or keep them unordered.
	 */
	public static List<String> extractKeywords(List<String> collection, boolean ordered)
	{	
		// Generte the list of keywords
		List<String> extractedKeywords = new ArrayList<String>();
		
		// For every string in the collection 
		for (String s : collection)
		{
			// Get the individual words and add them to the list
			String[] words = s.split(" ");
			Collections.addAll(extractedKeywords, words);
		}
		
		// Remove duplicate keywords
		removeDuplicateKeywords(extractedKeywords);
		
		// Sort the keywords alphabetically if required
		if(ordered) Collections.sort(extractedKeywords);
		
		// Return the list of keywords
		return extractedKeywords;
	}
}
