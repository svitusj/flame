package main;

import java.util.ArrayList;
import java.util.List;

import examples.GoldTrucks;
import utils.LSIUtil;

/**
 * Main class. Runs the SimpleLSI for the attached examples.
 * @author Ra�l
 */
public class Main
{

	public static void main(String[] args) 
	{
		// Generate the list of documents
		List<String> documents = new ArrayList<String>();
		
		// Fill it with the documents in the example
		documents.add(GoldTrucks.d1);
		documents.add(GoldTrucks.d2);
		documents.add(GoldTrucks.d3);
		
		// Perform the LSI over the example documents and query with a certain dimension
		LSIUtil.PerformLSI(documents, GoldTrucks.query, 1);
		LSIUtil.PerformLSI(documents, GoldTrucks.query, 2);
		LSIUtil.PerformLSI(documents, GoldTrucks.query, 3);
	}

}
