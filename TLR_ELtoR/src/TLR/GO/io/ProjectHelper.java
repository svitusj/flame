package TLR.GO.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import TLR.GO.utils.Constants;
import data.Description;
import data.Fragment;
import data.KnowledgeBase;
import data.TestCase;
import flame.GO.core.Population;

public class ProjectHelper {

	private IProject project;
	private static ProjectHelper instance;

	public static ProjectHelper getInstance() {
		if (instance == null)
			instance = new ProjectHelper();

		return instance;
	}
	
	private ProjectHelper() {
		checkProject();
	}

	private void checkProject() {

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		project = root.getProject(Constants.PROJECT_NAME);
		try {
			if (!project.exists()) {
				project.create(null);
			}
			project.open(null);
			copyInputsToProject();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void copyInputsToProject() {
				
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		String location_inputs = root.getLocation().uptoSegment(root.getLocation().segmentCount()-1) + "/" + Constants.WORSPACE_PLUGIN + "/" + Constants.PROJECT_DATA;
		File srcfolder = new File (location_inputs);
				
		IContainer destFolder = root.getContainerForLocation(project.getLocation());
		
		copyFiles(srcfolder, destFolder);

	}
	
	private void copyFiles(File srcFolder, IContainer destFolder) {
		for (File f : srcFolder.listFiles()) {
			if (f.isDirectory()) {
				IFolder newFolder = destFolder.getFolder(new Path(f.getName()));
				try {
					if (!newFolder.exists()) {
						newFolder.create(true, true, null);
					}
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				copyFiles(f, newFolder);
			} else {
				IFile newFile = destFolder.getFile(new Path(f.getName()));
					if(!newFile.exists()){
						try {
							newFile.create(new FileInputStream(f), true, null);
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (CoreException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			}
		}
	}
	
	public String getProjectLocation()
	{
		return project.getLocation().toString();
	}

	public KnowledgeBase loadKnowledgeBase(String filepath, String filespath_fragments, String filespath_descriptions) {
		
		KnowledgeBase knowledge_base = new KnowledgeBase();
		knowledge_base.setFragments(loadFragments(filespath_fragments));
		knowledge_base.setDescriptions(loadDescriptions(filespath_descriptions));
		loadScores(knowledge_base, filepath);
		
		return knowledge_base;
		
	}

	private HashMap<String, Fragment> loadFragments(String filespath) {
		
		HashMap<String, Fragment> fragments = new HashMap<String, Fragment>();
		try {
			IFolder fragments_folder = project.getFolder(filespath);
			for (IResource res : fragments_folder.members()) {
				if (res instanceof IFile) 
				{
					IFile file = (IFile) res;
					if (file.getFileExtension().equals(Constants.EXTENSION_MODELS)) 
					{
						fragments.put(file.getName(), new Fragment(file.getName(), loadFragment(file)));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fragments;
	}
	
	private EObject loadFragment(IFile f) {
	
		if (f.exists()) {
	
			String filename = f.getFullPath().toString();
			ResourceSet resSet = new ResourceSetImpl();
			URI uri = URI.createFileURI(filename);
			Resource resource = resSet.getResource(uri, true);
			EObject myModel = (EObject) resource.getContents().get(0);
			return myModel;
		}
		return null;
	
	}

	private HashMap<Integer, Description> loadDescriptions(String filespath) {
		
		HashMap<Integer, Description> descriptions = new HashMap<Integer, Description>();
		try {
			IFolder modelsfolder = project.getFolder(filespath);
			for (IResource res : modelsfolder.members()) 
			{
				if (res instanceof IFile) 
				{
					IFile file = (IFile) res;
					Description description = loadDescription(file);
					if (description != null)
					{
						descriptions.put(description.getId(), description); 
					}
				}
			}
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return descriptions;
	}

	private Description loadDescription(IFile file) throws CoreException {
		if (file.getFileExtension().equals(Constants.EXTENSION_TXT)) {
			if (file.exists()) {
				InputStream stream = file.getContents();
				String description = new BufferedReader(new InputStreamReader(stream)).lines()
						.collect(Collectors.joining("\n"));
				String id_description = file.getName().substring(0, file.getName().lastIndexOf('.'));
				try {
					return new Description(Integer.parseInt(id_description), description);
			    } catch (NumberFormatException e) {
			    	return new Description(0, description);
			    }
				
			}
		}
		return null;
	}

	private ArrayList<Element> loadScores(KnowledgeBase knowledge_base, String filepath) {
		ArrayList<Element> elements = new ArrayList<Element>();
		
		try {
			File fXmlFile = new File(project.getLocation().toString()+"/"+filepath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			
			doc.getDocumentElement().normalize();
	
			NodeList nList = doc.getElementsByTagName(Constants.TAG_MODEL);
	
			for (int i = 0; i < nList.getLength(); i++) {
	
				Node nNode = nList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	
					Element eElement = (Element) nNode;
			
					String id_fragment = eElement.getElementsByTagName(Constants.TAG_FILENAME).item(0).getTextContent();
					int id_description = Integer.parseInt(eElement.getElementsByTagName(Constants.TAG_ID_QUERY).item(0).getTextContent());
					float score = Float.parseFloat(eElement.getElementsByTagName(Constants.TAG_SCORE).item(0).getTextContent());
					knowledge_base.setKnowledgeElement(id_description, id_fragment, score);
				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return elements;
	}

	
	

	public void createFile(String file_content, String filename) {

		InputStream input = new ByteArrayInputStream(file_content.getBytes());
		
		IFile ifile = project.getFile(filename);
		
	    try {
	    	if (!ifile.exists()) {
	    		ifile.create(input, true, null);
	    	} else {
	    		ifile.setContents(input, IResource.FORCE, null);
	    	}
	    } catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        try {
	            input.close();
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
	    }		
	}
	
	public void createFile(String name, EObject content) {
		IFile f = project.getFile(name);
		if (!f.exists()) {
			ResourceSet resSet = new ResourceSetImpl();
			Resource resource = resSet.createResource(URI.createPlatformResourceURI(f.getFullPath().toString(), true));
			resource.getContents().add(content);
			try {
				resource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public HashMap<String, TestCase> loadTestCases(String filespath)
	{
		HashMap<String, TestCase> test_cases = new HashMap<String, TestCase>();
		try {
			IFolder modelsfolder = project.getFolder(filespath);
			for (IResource res : modelsfolder.members()) {
				if (res instanceof IFolder) {
					IFolder test_case_folder = (IFolder) res;
					
					String id_model = test_case_folder.getName();
					EObject model = null;
					String description = null;
					
					for (IResource res2 : test_case_folder.members())
					{	
						if (res2 instanceof IFile)
						{
							IFile file = (IFile) res2;
							if (file.getName().equals(Constants.FILENAME_MODEL))
							{
								model = loadFragment(file);
							}
							else if (file.getName().equals(Constants.FILENAME_REQUIREMENT))
							{
								description = loadDescription(file).getDescrition();
							}
						}
					}
					if(model!= null && description!=null)
					{
						test_cases.put(id_model, new TestCase(id_model, model, description));
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return test_cases;
	}

  public void loadResults(Population population) 
  {	
		String line = "";
	    FileReader file_ranking;
		try {
			
			file_ranking = new FileReader(project.getLocation().toString()+"/"+Constants.FILEPATH_RANKING);
			BufferedReader buffered_ranking = new BufferedReader(file_ranking);
		    
			float score = -1;
			int idx = 0;
			while((line = buffered_ranking.readLine())!=null) {
		         String[] fields_ranking = line.split("\t");
		        	         
		         if (fields_ranking.length>1)
		         {
		        		score = Float.parseFloat(fields_ranking[Constants.RANKING_FILE_SCORE_POSITION]);
		        		population.get(idx).setFitness(score);		
		         }
		         idx++;
		    }
		    buffered_ranking.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/*
	public void createFeatureVectorFiles(Map<String, ArrayList<Integer>> files) {
		
		for (Entry<String, ArrayList<Integer>> entry : files.entrySet())
		{
			createFeatureVectorFiles(entry.getKey(), entry.getValue());
		}
		
	}

	public void createFeatureVectorFiles(KnowledgeBase knowledge_base, int kfolds){//, ArrayList<Boolean>features) {
		IFolder folder = project.getFolder("temps");
		try {
			if (!folder.exists())
			{
					folder.create(IResource.NONE, true, null);
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<ArrayList<Element>> knowledgeBase_in_Kfolds = new ArrayList<ArrayList<Element>>();//= knowledge_base.divideKnowledgeBaseIntoKFolds(kfolds);
		
		FeatureVectorGenerator feature_vector = new FeatureVectorGenerator();
		
		ArrayList<Element> training;
		ArrayList<Element> testing;
		for (int i=1; i<= kfolds; i++)
		{
			training = new ArrayList<Element>();
			testing = new ArrayList<Element>();
			
			for (int n=0; n< knowledgeBase_in_Kfolds.size();n++)
			{
				if (i==n+1){
					testing.addAll(knowledgeBase_in_Kfolds.get(n));
				}
				else
				{
					training.addAll(knowledgeBase_in_Kfolds.get(n));
				}
			}
			
		}
	}

	*/
/*
	public HashMap<Integer, String> retrieveScores(int kfold, int id_folder)
	{
		HashMap<Integer, String> best_scores = new HashMap<Integer, String>();
	    float score = 0;
	    
		String line = "";
	    FileReader file;
	    FileReader file_test;
		try {
			file = new FileReader(project.getLocation().toString()+"/temps/"+id_folder+"/score_"+kfold+".txt");
			file_test = new FileReader(project.getLocation().toString()+"/temps/test_kfold_"+kfold+".txt");
			BufferedReader buffered = new BufferedReader(file);
			BufferedReader buffered_test = new BufferedReader(file_test);
		    while((line = buffered.readLine())!=null) {
		         String[] fields = line.split("\t");
		         String[] fields_test = buffered_test.readLine().split(" # ");
		         
		         if (fields.length>1)
		         {
		        	 int id_description = Integer.parseInt(fields[0]);
		        	 //fields_test[1] = id_fragment (string)
		        	 //fields[2] = score (float)
		        	 if(best_scores.containsKey(id_description))
		        	 {
		        		 if(score < Float.parseFloat(fields[2]))
		        		 {
		        			 best_scores.replace(id_description, fields_test[1]);
		        			 score = Float.parseFloat(fields[2]);
		        		 }
		        	 }
		        	 else
		        	 {
		        		 best_scores.put(id_description, fields_test[1]);
		        		 score = Float.parseFloat(fields[2]);
		        	 }
		        	 
		         }
		    }
		    buffered.close();
		    buffered_test.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return best_scores;
	}*/

	
	
}
