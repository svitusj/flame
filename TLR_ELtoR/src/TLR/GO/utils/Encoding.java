package TLR.GO.utils;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import TrainControlDSL.Cabin;
import TrainControlDSL.Condition;
import TrainControlDSL.Equipment;
import TrainControlDSL.EquipmentGroup;
import TrainControlDSL.EvaluableProperty;
import TrainControlDSL.InhibitableElement;
import TrainControlDSL.InvocatedOrder;
import TrainControlDSL.NestedCondition;
import TrainControlDSL.Order;
import TrainControlDSL.OrderInhibitionRule;
import TrainControlDSL.OrderInvocationRule;
import TrainControlDSL.Property;
import TrainControlDSL.PropertySynchronizationRule;
import TrainControlDSL.Rule;
import TrainControlDSL.SingleCondition;
import TrainControlDSL.TrainModule;
import ontology.Ontology;
import ontology.RelationConcepts;

public class Encoding {
	
	public static final Ontology ONTOLOGY = new Ontology();
	
	public static ArrayList<Float> encodeDescriptionFragment(String description, EObject model)
	{
		ArrayList<Float> feature_vector = new ArrayList<Float>();
		ArrayList<String> fragment_concepts = new ArrayList<String>();
		ArrayList<String> fragment_attributes = new ArrayList<String>();
		ArrayList<ArrayList<String>> fragment_relations = new ArrayList<ArrayList<String>>();

		//1: Compare number of concepts
		// 1/(1+|Number concepts in description - Number of concepts in fragment|)
		float num_concepts_description = 0;
		float num_concepts_fragment = 0;
		ArrayList<String> terms = ONTOLOGY.getTerms(description);
		for (int i=0; i<terms.size(); i++)
		{
			if (ONTOLOGY.containsConcept(terms.get(i)))
			{
				num_concepts_description++;
			}
		}
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Equipment)
			{
				if (ONTOLOGY.containsConcept(((Equipment)element).getName()))
				{
					num_concepts_fragment++;
					fragment_concepts.add(ONTOLOGY.getWordRoot(((Equipment)element).getName()));
				}
			}
			else if (element instanceof TrainModule)
			{
				if (ONTOLOGY.containsConcept(((TrainModule)element).getID()))
				{
					num_concepts_fragment++;
					fragment_concepts.add(ONTOLOGY.getWordRoot(((TrainModule)element).getID()));
				}
			}
		}
		feature_vector.add(1/(1+Math.abs(num_concepts_description-num_concepts_fragment)));
		
		//2: Compare number of attributes
		//  1/(1 + |Number of attributes in description - Number of attributes in fragment|)
		float num_attributes_description = 0;
		float num_attributes_fragment = 0;
		terms = ONTOLOGY.getTerms(description);
		for (int i=0; i<terms.size(); i++)
		{
			if (ONTOLOGY.containsAttribute(terms.get(i)))
			{
				num_attributes_description++;
			}
		}
		iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Property)
			{
				if (ONTOLOGY.containsAttribute(((Property)element).getName()))
				{
					num_attributes_fragment++;
					fragment_attributes.add(ONTOLOGY.getWordRoot(((Property)element).getName()));
				}
			}
		}
		feature_vector.add(1/(1+Math.abs(num_attributes_description-num_attributes_fragment)));
		
		//3: Compare number of rules
		//  1/(1 + |Number of rules in description - Number of rules in fragment|)
		// Note: The number of rules in description is by default 1.
		float num_rules_fragment = 0;
		iterator = model.eAllContents();
		while (iterator.hasNext())
		{
			EObject element = iterator.next();
			if(element instanceof Rule)
			{
				num_rules_fragment++;
				fragment_relations.add(getKeywordsOfRule((Rule)element));
			}
		}
		feature_vector.add(1/(1+Math.abs(1-num_rules_fragment)));
		
		//4: Relevance of concepts
		// Summation of 1/(1+|a-b|), for each concept in the ontology a=Number of this concept in the description, and b=Number of this concept in the fragment
		float relevance_of_concepts = 0;
		terms = ONTOLOGY.getTerms(description);
		for (String concept: ONTOLOGY.getConcepts())
		{
			relevance_of_concepts =  relevance_of_concepts + presenceOfConcept(concept, terms, fragment_concepts);
		}
		feature_vector.add(relevance_of_concepts/ONTOLOGY.getNumberConcepts());
		
		//5: Relevance of attributes
		// Summation of 1/(1+|a-b|), for each attribute in the ontology a=Number of this attribute in the description, and b=Number of this attribute in the fragment
		float relevance_of_attributes = 0;
		terms = ONTOLOGY.getTerms(description);
		for (String attribute: ONTOLOGY.getAttributes())
		{
			relevance_of_attributes =  relevance_of_attributes + presenceOfConcept(attribute, terms, fragment_attributes);
		}
		feature_vector.add(relevance_of_attributes/ONTOLOGY.getNumberAttributes());
		
		//6: Relevance of relations
		//Number of relations based on the summation of 1/(1+|a-b|), for each relation in the ontology a=Number of this relation in the description, and b=Number of this relation in the fragment
		float relevance_of_relations = 0;
		terms = ONTOLOGY.getTerms(description);
		for ( Entry<Integer, RelationConcepts> entry :ONTOLOGY.getRelations().entrySet())
		{
			RelationConcepts relation = entry.getValue();
			relevance_of_relations =  relevance_of_relations + presenceOfRelation(relation.getFirstConcept(), relation.getSecondConcept(), terms, fragment_relations);
		}
		feature_vector.add(relevance_of_relations/ONTOLOGY.getNumberRelations());
		
		return feature_vector;
	}
	
	private static Float presenceOfConcept(String concept, ArrayList<String> description_words, ArrayList<String> fragment_concepts) {
		float num_concept_description = 0;
		float num_concept_fragment = 0;
		for (int i=0; i<description_words.size(); i++)
		{
			if (concept.equals(description_words.get(i)))
			{
				num_concept_description++;
			}
		}
		
		for (int i=0;i<fragment_concepts.size(); i++)
		{
			if(concept.equals(fragment_concepts.get(i)))
			{
				num_concept_fragment++;
			}
		}

		return 1/(1+Math.abs(num_concept_description-num_concept_fragment));
		
	}
	
	private static Float presenceOfRelation(String concept1, String concept2, ArrayList<String> description_words, ArrayList<ArrayList<String>> fragment_relations) {
		
		if (description_words.contains(concept1) && description_words.contains(concept2))
		{
			for (int n=0;n<fragment_relations.size(); n++)
			{
				if (fragment_relations.get(n).contains(concept1) && fragment_relations.get(n).contains(concept2))
				{
					return (float) 1;
				}
			}
			return (float) 0;
		}
		else if (!description_words.contains(concept1) && !description_words.contains(concept2))
		{
			for (int n=0;n<fragment_relations.size(); n++)
			{
				if (fragment_relations.get(n).contains(concept1) && fragment_relations.get(n).contains(concept2))
				{
					return (float) 0;
				}
			}
			return (float) 1;
		}
		else
		{
			return (float) 1;
		}
	}
	
	private static ArrayList<String> getKeywordsOfRule(Rule rule)
	{
		ArrayList<String> equimpents_related = new ArrayList<String>();
		
		//TRIGGER
		if(rule.getHasTrigger()!=null) 
		{
			if(rule.getHasTrigger().getProperty()!=null)
			{
				equimpents_related.addAll(getKeywordsOfProperty(rule.getHasTrigger().getProperty()));
			}
		}
			
		//SYNCHRONIZATION RULE
		if (rule instanceof PropertySynchronizationRule)
		{
			
			if(((PropertySynchronizationRule) rule).getProperty()!=null)
			{
				equimpents_related.addAll(getKeywordsOfProperty(((PropertySynchronizationRule)rule).getProperty()));
			}
			
			Condition condition = ((PropertySynchronizationRule) rule).getHasConditions();
			equimpents_related.addAll(getKeywordsOfConditions(condition));
						
		}
		//INVOCATION RULE
		else if (rule instanceof OrderInvocationRule){
			for (InvocatedOrder invocated_order : ((OrderInvocationRule)rule).getHasPhysicalEffects())
			{
				if(invocated_order.getOrder()!=null)
				{
					equimpents_related.addAll(getKeywordsOfOrders(invocated_order.getOrder()));
				}
				
				Condition condition = (invocated_order.getHasConditions());
				equimpents_related.addAll(getKeywordsOfConditions(condition));
			}
		}
		//INHIBITION RULE
		else
		{
			InhibitableElement inhibitable_element = ((OrderInhibitionRule)rule).getInhibitableElement();
			if(inhibitable_element !=null)
			{
				if (inhibitable_element instanceof Equipment)
				{
					equimpents_related.add(((Equipment)inhibitable_element).getName());
				}
				else if (inhibitable_element instanceof Order)
				{
					equimpents_related.addAll(getKeywordsOfOrders((Order)inhibitable_element));
				}
			}
			
			Condition condition = (((OrderInhibitionRule)rule).getHasConditions());
			equimpents_related.addAll(getKeywordsOfConditions(condition));
		}
		
		//Remove duplicated values
		equimpents_related = (ArrayList<String>) equimpents_related.stream().distinct().collect(Collectors.toList());
		
		//Format keywords in lower case and without numbers
		for(int i=0;i<equimpents_related.size();i++)
		{
			equimpents_related.set(i, ONTOLOGY .getWordRoot(equimpents_related.get(i)));
		}
		
		return equimpents_related;

	}
	
	private static ArrayList<String> getKeywordsOfOrders(Order order) 
	{	
		ArrayList<String> equipments_related = new ArrayList<String>();
		
		EObject econtainer = order.eContainer();
		if (econtainer instanceof Equipment)
		{
			equipments_related.add(((Equipment)econtainer).getName());
		}
		else if (econtainer instanceof EquipmentGroup)
		{
			for (Equipment equipment : ((EquipmentGroup)econtainer).getMembers())
			{
				equipments_related.add(equipment.getName());
			}
		}
		
		return equipments_related;
	}

	private static ArrayList<String> getKeywordsOfProperty(EvaluableProperty property)
	{
		ArrayList<String> keywords = new ArrayList<String>();
		
		if(property.eContainer() instanceof Equipment)
		{
			Equipment equipment = (Equipment)property.eContainer();
			keywords.add(equipment.getName());
			
			if(equipment.getInstalled()!=null)
			{
				keywords.add(equipment.getInstalled().getID());
			}
			
		}
		else if (property instanceof Cabin)
		{
			keywords.add(((Cabin)property).getID());
		}
		
		return keywords;
	}
	
	private static ArrayList<String> getKeywordsOfConditions(Condition condition) {
		ArrayList<String> keywords = new ArrayList<String>();
		if(condition != null)
		{
			if (condition instanceof NestedCondition){
				NestedCondition nested_condition = (NestedCondition)condition;
				keywords.addAll(getKeywordsOfConditions(nested_condition.getLeft()));
				keywords.addAll(getKeywordsOfConditions(nested_condition.getRight()));
				if(nested_condition.getProperty()!=null)
				{
					keywords.addAll(getKeywordsOfProperty(nested_condition.getProperty()));
				}
			}
			else
			{
				SingleCondition single_condition = (SingleCondition)condition;
				if (single_condition.getCondition() != null)
				{
					keywords.addAll(getKeywordsOfConditions(single_condition.getCondition()));
				}
				if (single_condition.getProperty()!= null)
				{
					keywords.addAll(getKeywordsOfProperty(single_condition.getProperty()));
				}
			}
		}
		return keywords;
	}

}
