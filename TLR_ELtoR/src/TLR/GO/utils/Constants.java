package TLR.GO.utils;

public class Constants {

	//PLUGIN AND PROJECT
	public static final String WORSPACE_PLUGIN = "workspace_TLR_ELtoR/TLR_ELtoR";
	public static final String PROJECT_NAME = "TRAINCONTROL";
	public static final String PROJECT_DATA = "data";
	
	//EXTENSIONS
	public static final String EXTENSION_MODELS = "traincontroldsl";	
	public static final String EXTENSION_TXT = "txt";
	public static final String EXTENSION_XML = "xml";
	
	//KNOWLEDGE BASE
	public static final String FOLDER_KNOWLEDGE_BASE = "knowledge_base/";
	public static final String FOLDERPATH_KNOWLEDGE_BASE_FRAGMENTS = FOLDER_KNOWLEDGE_BASE+"models/";
	public static final String FOLDERPATH_KNOWLEDGE_BASE_DESCRIPTIONS = FOLDER_KNOWLEDGE_BASE+"descriptions/";	
	public static final String FILEPATH_KNOWLEDGE_BASE = FOLDER_KNOWLEDGE_BASE + "knowledge_base." + EXTENSION_XML;

		//Read Knowledge Base File
		public static final String TAG_MODEL = "model";
		public static final String TAG_FILENAME = "filename";
		public static final String TAG_ID_QUERY = "query_id";
		public static final String TAG_SCORE = "score";
	
	//DATASET
	public static final String FOLDERPATH_DATASET = "dataset/";
	public static final String FILENAME_MODEL = "model" + "."+EXTENSION_MODELS;
	public static final String FILENAME_REQUIREMENT = "requirement"+"."+EXTENSION_TXT;
	public static final String NAME_FRAGMENT = "fragment_";
	
	//CLASSIFIER
	public static final String FOLDERPATH_CLASSIFIER = "/classifier/";
	public static final String FILEPATH_TRAINING = FOLDERPATH_CLASSIFIER+"training."+EXTENSION_TXT;
	public static final String FILEPATH_FEATURES = FOLDERPATH_CLASSIFIER+"features."+EXTENSION_TXT;
	public static final String FILEPATH_TESTING = FOLDERPATH_CLASSIFIER+"testing."+EXTENSION_TXT;
	public static final String FILEPATH_CLASSIFIER = FOLDERPATH_CLASSIFIER+"classifier."+EXTENSION_TXT;
	public static final String FILEPATH_RANKING = FOLDERPATH_CLASSIFIER + "ranking"+"."+EXTENSION_TXT;
	
	public static final float MIN_SCORE = 0;
		
		//Read Ranking File
		public static final int RANKING_FILE_DESCRIPTION_POSITION = 0;
		public static final int RANKING_FILE_SCORE_POSITION = 2;
	
	//SET UP EVOLUTIONARY ALGORITHM
	public static final int INDIVIDUALS_IN_POPULATION = 120;
	public static final int INDIVIDUALS_SELECTED = 30;
	public static final double PERCENTAGE_ELEMENTS_INITIAL_INDIVIDUALS = 0.1;
	
	
	
	//Test Save Results
	public static final String FOLDER_RESULTS = "results_TLR_ELtoR/";
	public static final String FOLDER_REPORTS = "reports_TLR_ELtoR/";
}
