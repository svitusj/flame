package TLR.views;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import TLR.GO.utils.Constants;
import eval.core.Evaluator;


public class TestView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "TLRELtoR.views.TLRELtoR";


	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		
		Composite panel = new Composite(parent, SWT.NONE);
		RowLayout layout = new RowLayout();
		panel.setLayout(layout);
		
		//Train Classifier.
		Button b1 = new Button(panel, SWT.NONE);
		b1.setText("Train Classifier");
		b1.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestHelper.trainClassifier(Constants.FILEPATH_KNOWLEDGE_BASE, Constants.FOLDERPATH_KNOWLEDGE_BASE_FRAGMENTS, Constants.FOLDERPATH_KNOWLEDGE_BASE_DESCRIPTIONS);
			}
		});
		
		//Test Dataset.
		Button b2 = new Button(panel, SWT.NONE);
		b2.setText("Test Dataset");
		b2.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestHelper.testDataset(Constants.FOLDERPATH_DATASET);
			}
		});
		
		Button b3 = new Button(panel, SWT.NONE);
		b3.setText("Generate Reports");
		b3.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				Evaluator evaluator = new Evaluator(Constants.FOLDER_RESULTS, Constants.FOLDER_REPORTS);
				evaluator.evaluateResults();
			}
		});
		
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		
	}
}
