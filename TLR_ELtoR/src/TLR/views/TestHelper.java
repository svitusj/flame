package TLR.views;

import ELtoR.TLRELtoR;
import TLR.GO.io.ProjectHelper;

public class TestHelper {

		public static void trainClassifier(String filepath_kb, String filespath_fragments_kb, String filespath_descriptions_kb) 
		{
			ProjectHelper project_helper = ProjectHelper.getInstance();
			TLRELtoR tlr_eltor = new TLRELtoR(project_helper);
			tlr_eltor.trainClassifier(filepath_kb, filespath_fragments_kb, filespath_descriptions_kb);			
		}

		public static void testDataset(String filespath) {
			ProjectHelper project_helper = ProjectHelper.getInstance();
			TLRELtoR tlr_eltor = new TLRELtoR(project_helper);
			tlr_eltor.testDataset(filespath);
		}

}


