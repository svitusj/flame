package ELtoR;

import java.util.HashMap;
import java.util.Map.Entry;

import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.Constants;
import ciir.umass.edu.eval.Evaluator;
import data.KnowledgeBase;
import data.TestCase;
import flame.GO.codec.CODECUtils;
import flame.GO.core.Individual;
import flame.GO.core.Population;

public class TLRELtoR {

	private ProjectHelper project = null;
	
	
	public TLRELtoR(ProjectHelper project) {
		super();
		this.project = project;
	}

	public void trainClassifier(String filepath_kb, String filespath_fragments_kb, String filespath_descriptions_kb) 
	{
				
		//Load the Knowledge Base
		KnowledgeBase knowledge_base = project.loadKnowledgeBase(filepath_kb, filespath_fragments_kb, filespath_descriptions_kb);
		
		//Encode Feature Vectors
		String feature_vectors = knowledge_base.encodeToFeatureVectors();
		
		// Save Feature Vectors
		String filename_training = Constants.FILEPATH_TRAINING;
		project.createFile(feature_vectors, filename_training);
		
		//Train classifier
		trainClassifierRankLib();

		
	}
	
	private void trainClassifierRankLib() {
		
		String project_location = project.getProjectLocation().toString();
		String args1[] = { "-train", project_location + Constants.FILEPATH_TRAINING, 
						   "-test", project_location + Constants.FILEPATH_TESTING, 
						   "-ranker", "2",
						   "-feature", project_location + Constants.FILEPATH_FEATURES, 
						   "-metric2t", "ERR@10",
						   "-save", project_location + Constants.FILEPATH_CLASSIFIER,
						   "-silent" };
		Evaluator.main(args1);		
	}
	
	public void testDataset(String filespath) 
	{
		//Load Test Cases from Dataset
		HashMap<String, TestCase> test_cases = project.loadTestCases(filespath);
		
		//Launch Test Cases
		for (Entry<String, TestCase> entry : test_cases.entrySet())
		{
			launchTestCase(entry.getKey(), entry.getValue());
		}
		
	}
	
	private void launchTestCase(String id_test_case, TestCase test_case) {
		
		//Evolutionary Algorithm
		EvolutionaryAlgorithm evol_alg = new EvolutionaryAlgorithm();
		
		//Initial Population
		Population population = evol_alg.createInitialPopulation(test_case.getModel());

		int idx_loop = 0;
		float fitness = 0;
		while (idx_loop<200)
		{
			//Fitness Initial Population
			population = evol_alg.fitnessFunction(test_case, population);
			
			//Genetic Operations
			population = evol_alg.geneticOperations(population);

			//Stop Condition
			if (fitness != population.get(0).getFitness())
			{
				fitness = population.get(0).getFitness();
				idx_loop=-1;
			}
			idx_loop++;
		}
		
		//Save results
		saveFragmentAt1(test_case.getId(), population);
	}
	
	private void saveFragmentAt1(String id_model, Population population) 
	{
		//Select Fragment at 1
		Individual individual = null;
		for (int idx_population=0; idx_population<population.size(); idx_population++)
		{
			if(individual==null)
			{
				individual = population.get(idx_population);
			}
			else
			{
				if(individual.getFitness()<population.get(idx_population).getFitness())
				{
					individual = population.get(idx_population);
				}
			}
		}
		
		//Save Selected Fragment
		String filepath = Constants.FOLDER_RESULTS+id_model+"."+Constants.EXTENSION_MODELS;
		project.createFile(filepath, CODECUtils.decode(individual));
	}
}
