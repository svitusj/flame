package ELtoR;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import TLR.GO.io.ProjectHelper;
import TLR.GO.utils.Constants;
import TLR.GO.utils.FeatureVectorGenerator;
import ciir.umass.edu.eval.Evaluator;
import data.Description;
import data.Fragment;
import data.TestCase;
import flame.GO.codec.CODECMapping;
import flame.GO.codec.CODECUtils;
import flame.GO.core.Individual;
import flame.GO.core.Population;
import flame.GO.crossover.SimpleCrossover;
import flame.GO.mutation.SimpleMutation;

public class EvolutionaryAlgorithm {
	
	public Population createInitialPopulation(EObject model)
	{
		Population initial_population = new Population();
		
		int num_elements = getNumberOfElements(model);
		double num_elements_in_fragments =  (num_elements * Constants.PERCENTAGE_ELEMENTS_INITIAL_INDIVIDUALS);
		
		for (int i = 0; i<Constants.INDIVIDUALS_IN_POPULATION; i++)
		{
			Individual individual = CODECUtils.encode(copyEObject(model));
			boolean[] elements = individual.getGenes().elements;
			while(individual.getGenes().sizeInelements()>num_elements_in_fragments)
			{
				int index =  (int) (Math.random()*(elements.length-1));
				elements[index] = false;

			}
		
			initial_population.set(individual);
		}

		return initial_population;
	}

	private int getNumberOfElements(EObject eobject) {
		int num_elements = 0;
		TreeIterator<EObject> iterator = eobject.eAllContents();
		while(iterator.hasNext())
		{
			iterator.next();
			num_elements++;
		}
		return num_elements;
	}
	
	private static EObject copyEObject(EObject oldObject)
	{
        EObject result = EcoreUtil.copy(oldObject);
        return result;
	}
	
	public Population geneticOperations(Population population) {
		
		//Selection
		ArrayList<Individual> individuals_selected = higherFitnessSelection(population, Constants.INDIVIDUALS_SELECTED);
		population = new Population();
		
		for (Individual individual : individuals_selected)
		{
			population.set(individual);
		}
		
		//Mutation
		for (int loops=0;loops<2;loops++)
		{
			for (int i=0; i<individuals_selected.size(); i++)
			{
				Individual individual1 = copyIndividual(individuals_selected.get(i));
				population.set(SimpleMutation.mutate(individual1));
				i++;
			}
		}
		
		//Crossover
		for (int loops=0;loops<2;loops++)
		{
			for (int i=0; (i+1)<individuals_selected.size(); i++)
			{
				Individual individual1 = copyIndividual(individuals_selected.get(i));
				Individual individual2 = copyIndividual(individuals_selected.get(i+1));
				population.set(SimpleCrossover.crossoverRandomMask(individual1, individual2));
				i++;
			}
		}

		return population;
	}

	private ArrayList<Individual> higherFitnessSelection(Population population, int individualsSelected) {
		ArrayList<Individual> individuals_in_population = new ArrayList<Individual>();
		for (int index = 0; index<population.size(); index++)
		{
			individuals_in_population.add(population.get(index));
		}
		
		Collections.sort(individuals_in_population, new Comparator<Individual>() {
			@Override
			public int compare(Individual individual1, Individual individual2) {
				// TODO Auto-generated method stub
				return Float.compare(individual1.getFitness(),individual2.getFitness());
			}
	    });
		
		ArrayList<Individual> selected = new ArrayList<Individual>();
		for (int i=1;i<=individualsSelected;i++)
		{
			selected.add(individuals_in_population.get(individuals_in_population.size()-i));
		}
		
		return selected;

	}

	public Population fitnessFunction(TestCase test_case, Population population) 
	{
		ProjectHelper project = ProjectHelper.getInstance();
		FeatureVectorGenerator feature_vector_generator = new FeatureVectorGenerator();

		//Population to Feature Vectors
		String feature_vectors = "";
		for (int i=0;i<population.size(); i++)
		{
			Individual individual = copyIndividual(population.get(i));
			Fragment fragment = new Fragment(Constants.NAME_FRAGMENT+i,CODECUtils.decode(individual));
			feature_vectors =  feature_vectors + feature_vector_generator.getFeatureVector(Constants.MIN_SCORE, new Description(Integer.parseInt(test_case.getId()), test_case.getDescription()), fragment);
		}
		
		//Save Feature Vectors
		String filename_training = Constants.FILEPATH_TESTING;
		project.createFile(feature_vectors, filename_training);
		
		//Test Feature Vectors
		testClassifierRankLib();
		
		//Retrieve Ranking
		project.loadResults(population);
		
		return population;
	}
	
	private Individual copyIndividual(Individual individual) {
		CODECMapping codec = new CODECMapping(copyEObject(individual.getCodec().getRoot()));
		Individual new_individual = new Individual(individual.getGenes(),codec);
		return new_individual;
	}

	private void testClassifierRankLib() 
	{
		String project_location = ProjectHelper.getInstance().getProjectLocation();
		String args2[] = { "-load", project_location + Constants.FILEPATH_CLASSIFIER,
						   "-rank", project_location + "/" + Constants.FILEPATH_TESTING, 
						   "-feature", project_location + Constants.FILEPATH_FEATURES, 
						   "-score", project_location + "/" + Constants.FILEPATH_RANKING,
						   "-silent" };
		Evaluator.main(args2);
	}


}
