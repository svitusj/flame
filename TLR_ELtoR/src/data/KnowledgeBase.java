package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.eclipse.emf.ecore.EObject;

import TLR.GO.utils.FeatureVectorGenerator;


public class KnowledgeBase {
	
	private ArrayList<Triplet> knowledge_base;
	private HashMap<Integer, Description> descriptions;
	private HashMap<String, Fragment> fragments;
	
	
	public KnowledgeBase(){
		knowledge_base = new ArrayList<Triplet>();
		descriptions = new HashMap<Integer, Description>();
		fragments = new HashMap<String, Fragment>();
	}
	
	public void setFragment (String id_fragment, EObject fragment)
	{
		fragments.put(id_fragment, new Fragment(id_fragment, fragment));
	}
	
	public void setDescription (int id_description, String description)
	{
		descriptions.put(id_description, new Description(id_description, description));
	}
	
	public void setKnowledgeElement(int id_description, String id_fragment, float score)
	{
		if(descriptions.containsKey(new Integer(id_description)))
		{
			if(fragments.containsKey(id_fragment))
			{
				knowledge_base.add(new Triplet(id_description, id_fragment, score));
			}
		}
	}
	
	public Description getDescription(int id_description){
		return descriptions.get(id_description);
	}
	
	public Fragment getFragment(String id_fragment){
		return fragments.get(id_fragment);
	}
	
	public Triplet getKnowledgeBaseElement(int id_knoledge_base_element){
		return knowledge_base.get(id_knoledge_base_element);
	}
	
	public int getSizeKnowledgeBase()
	{
		return knowledge_base.size();
	}
	
	private void sortKnowledgeBase()
	{
		Collections.sort(knowledge_base, new Comparator<Triplet>(){

			@Override
			public int compare(Triplet o1, Triplet o2) {
				// TODO Auto-generated method stub
				return o1.compareTo(o2);
			}	
		});

	}

	public void setFragments(HashMap<String, Fragment> fragments) 
	{
		this.fragments = fragments;
	}

	public void setDescriptions(HashMap<Integer, Description> descriptions) 
	{
		this.descriptions = descriptions;
	}
	
	public String encodeToFeatureVectors()
	{
		String feature_vectors = "";
		sortKnowledgeBase();
		FeatureVectorGenerator feature_vector_generator = new FeatureVectorGenerator();
		
		for (Triplet element : knowledge_base)
		{
			feature_vectors = feature_vectors + feature_vector_generator.getFeatureVector(element.getScore(),descriptions.get(element.getIdDescription()),fragments.get(element.getIdFragment()));
		}
		
		return feature_vectors;
	}
}
