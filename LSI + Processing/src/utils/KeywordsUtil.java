package utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import main.Main;
import opennlp.tools.lemmatizer.SimpleLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.WhitespaceTokenizer;

/**
 * This class contains the functionality needed to extract keywords from a set of already processed documents and query.
 * @author Ra�l
 */
public class KeywordsUtil 
{
	
	/* Variables for domain terms and stopwords lists + POS Model document route */
	// Sidenote: domain terms and stopwords are tailored for the provided example.
	private final static List<String> domainTerms = Arrays.asList("gold","truck");
	private final static List<String> stopwords = Arrays.asList("in","of","a");
	private final static String posModelRoute = "/resources/en-pos-maxent.bin";
	private final static String dictRoute = "/resources/en-lemmatizer.dict";
	

	/* Preprocessing methods */
	
	/**
	 * Method that detects domain terms in a string. 
	 * Puts the detected terms in the keywords list and subtracts them from the string.
	 * @param requirement The requirement from which the terms must be extracted
	 * @param extractedKeywords The list of extracted keywords
	 * @return The requirement without the domain terms
	 */
	private static String extractDomainTerms(String s, List<String> extractedKeywords)
	{
		// For every term in the list of domain terms
		for(String term : domainTerms)
		{
			// If the string contains the term
			if (s.contains(term))
			{
				// The term is added to the keywords
				extractedKeywords.add(term);				
				// And then removed from the string
				s = s.replace(term, "");
			}
		}
		// Return the modified string
		return s;
	}
	
	/**
	 * Method used to extract the lemmas of the nouns of a string via POS Tagging and lemmatizing. 
	 * The method extracts the lemmas of the nouns of the string.
	 * @param s The string to be processed.
	 * @return A list of words (lemmas of the nouns of the introduced string) 
	 */
	private static List<String> extractNounLemmas(String s)
    {
        try 
        {
        	/* START POS TAGGING */
            
        	// Create the POS Model from resource stream
            InputStream is = Main.class.getResourceAsStream(posModelRoute);
            final POSModel model = new POSModel(is);
          
            // Create the POS tagger from the model
            POSTaggerME tagger = new POSTaggerME(model);
            
            // Close the resource stream
            is.close();
            
            // Tag the string
            String[] tokens = WhitespaceTokenizer.INSTANCE.tokenize(s);
            String[] tags = tagger.tag(tokens);
         
            /* END POS TAGGING */
            
            /* START LEMMATIZING */
            
            // Create a list of lemmas for the lemmas of the words
            List<String> lemmas = new ArrayList<String>();
            
            // For all the words
            for(int i = 0; i < tokens.length; i++)
            {    	
            	// If the POS tag associated to the word represents a noun
            	if(tags[i].contains("NN"))
            	{
            		// Extract the lemma and add it to the list of lemmas
            		lemmas.add(lemmatize(tokens[i], tags[i]));
            	}
            }
            
            /* END LEMMATIZING */
            
            // Return the final list of words (lemmas of the nouns of the string)
            return lemmas;
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            return null;
        }
    }	

	/**
	 * Method that removes stopwords from a list of words.
	 * @param words The list of words from which stopwords must be removed.
	 */
	private static void filterStopwords(List<String> words)
	{
		// Remove every stopword in the list of stopwords
		words.removeAll(stopwords);
	}
	
	/* User-friendly util methods */
	
	/**
	 * Method that extracts the keywords from the documents and the query and removes duplicates. 
	 * User can choose whether to order the keywords alphabetically or to keep them unordered.
	 * @param collection The collection of Strings that compose a problem (documents and query).
	 * @param ordered Variable used to decide whether to order the keywords alphabetically or keep them unordered.
	 * @return A list of keywords.
	 */
	public static List<String> extractKeywords(List<String> collection, boolean ordered)
	{	
		// Generte the list of keywords
		List<String> extractedKeywords = new ArrayList<String>();
		
		// For every string in the collection 
		for (String s : collection)
		{
			// Extract the domain terms from the string
			s = extractDomainTerms(s, extractedKeywords);	
			// Obtain the lemmas from the nouns in the string
			List<String> lemmas = extractNounLemmas(s);
			// Filter the list of noun lemmas with the stopwords
			filterStopwords(lemmas);
			// Add the filtered lemmas to the extracted keywords list
			extractedKeywords.addAll(lemmas);
		}
		
		// Remove duplicate keywords
		removeDuplicateKeywords(extractedKeywords);
		
		// Sort the keywords alphabetically if required
		if(ordered) Collections.sort(extractedKeywords);
		
		// Return the list of keywords
		return extractedKeywords;
	}
	
	/**
	 * Method used to process the language of a string via POS Tagging and lemmatizing. 
	 * The method extracts the lemmas of all the words of the string.
	 * @param s The string to be processed.
	 * @return A list of words (lemmas of the words of the introduced string) 
	 */
	public static List<String> processLanguage(String s)
    {
        try 
        {
        	/* START POS TAGGING */
            
        	// Create the POS Model from resource stream
            InputStream is = Main.class.getResourceAsStream(posModelRoute);
            final POSModel model = new POSModel(is);
          
            // Create the POS tagger from the model
            POSTaggerME tagger = new POSTaggerME(model);
            
            // Close the resource stream
            is.close();
            
            // Tag the string
            String[] tokens = WhitespaceTokenizer.INSTANCE.tokenize(s);
            String[] tags = tagger.tag(tokens);
         
            /* END POS TAGGING */
            
            /* START LEMMATIZING */
            
            // Create a list of lemmas for the lemmas of the words
            List<String> lemmas = new ArrayList<String>();
            
            // For all the words
            for(int i = 0; i < tokens.length; i++)
            {    	
           		// Extract the lemma and add it to the list of lemmas
           		lemmas.add(lemmatize(tokens[i], tags[i]));
            }
            
            /* END LEMMATIZING */
            
            // Filter the stopwords from the list
            filterStopwords(lemmas);
            
            // Return the final list of words (lemmas of all the words of the string)
            return lemmas;
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            return null;
        }
    }
	
	/* Auxiliary methods */
	
	/**
	 * Method that removes duplicates from the keywords list
	 * @param keywordsList Keywords list from which duplicates must be removed
	 */
	private static void removeDuplicateKeywords(List<String> keywordsList)
	{
		// Create a Set -> Sets don't allow duplicates!
		// Move the words to the set and back to the ArrayList
		Set<String> keywordSet = new HashSet<>();
		keywordSet.addAll(keywordsList);
		keywordsList.clear();
		keywordsList.addAll(keywordSet);
	}
	
	/**
	 * Method used to lemmatize words into their semantical root.
	 * @param word The word that must be lemmatized.
	 * @param postag The postag associated to the word.
	 * @return The lemma of the word.
	 */
	private static String lemmatize(String word, String postag) 
	{
		try
		{
			// Get the dictionary as stream
			InputStream is = Main.class.getResourceAsStream(dictRoute);
			// Create the lemmatizer
			SimpleLemmatizer lemmatizer = new SimpleLemmatizer(is);
			// Close the stream
			is.close();

			// Return the lemma of the word according to its postag
			return lemmatizer.lemmatize(word, postag);
		
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	

}
