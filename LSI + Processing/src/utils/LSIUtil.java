package utils;

import java.util.ArrayList;
import java.util.List;
import org.ejml.alg.dense.mult.VectorVectorMult;
import org.ejml.simple.SimpleMatrix;
import org.ejml.simple.SimpleSVD;

/**
 * 
 * @author Ra�l
 */
public class LSIUtil 
{
	/* Variables for the LSI Term-by-Document Co-Occurrence Matrices and Query */
	private static SimpleMatrix termByDocumentCoOccurrenceMatrix;
	private static SimpleMatrix queryMatrix;
	
	/* LSI Methods -> Build matrix, build query, calculate LSI similitude values, produce ranking */
	
	/**
	 * Method that builds the Term-by-Document Co-Occurrence Matrix counting occurrences of the terms in the processed documents.
	 * @param terms Keywords of the problem
	 * @param documents List of String documents
	 */
	private static void buildTermByDocumentCoOccurrenceMatrix(List<String> terms, List<String> documents)
	{
		/* What needs to be done is count the term occurrences in each document */
		/* For each term, count the occurrences in a document, then go to the next document */
		// Start at row 0 (first term)
		int rowControl = 0;

		// The matrix has to have dimensions [number of keywords][number of documents]
		double[][] termByDocumentCoOccurrenceMatrixData = new double[terms.size()][documents.size()];
		
		// Process the documents to convert them into their lemmas
		// Generate the list of processed documents
		List<String> processedDocuments = new ArrayList<String>();
		// For every document
		for (String document : documents)
		{
			// Extract the lemmas of the nouns of the document
			List<String> documentLemmas = KeywordsUtil.processLanguage(document);
			
			// Rejoin the lemmas into a single string
			String processedDocument = String.join(" ", documentLemmas);
			
			// Add the processed document to the list of processed documents
			processedDocuments.add(processedDocument);
		}
		
		// For each term in the list of terms
		for(String term : terms)
		{
			// Restart the column count at the beginning (we are at the beginning of a term calculations, we have to go through all the documents)
			int colControl = 0;
			
			// For each document in the list of processed documents
			for(String processedDocument : processedDocuments)
			{				
				// Count the number of occurrences of the term in the processed document
				int termOccurrences = count(term, processedDocument);
				
				// Set the value on the matrix
				termByDocumentCoOccurrenceMatrixData[rowControl][colControl] = termOccurrences;
				
				// Increase the column (we go to the next document)
				colControl++;
			}
			
			// Once the term occurrences have been calculated for all the documents, go to the next row (next term)
			rowControl++;
		}
		
		// When all values have been calculated, then fill the matrix data
		termByDocumentCoOccurrenceMatrix = new SimpleMatrix(termByDocumentCoOccurrenceMatrixData);
	}
	
	/**
	 * Method that builds the Query matrix counting occurrences of the terms in the processed query.
	 * @param terms Keywords of the problem
	 * @param query The query
	 */
	private static void buildQueryMatrix(List<String> terms, String query)
	{
		/* What needs to be done is count term occurrences in the query for each term */

		// Start at row 0 (first term)
		int rowControl = 0;
		
		// The query is just one column
		double[][] queryMatrixData = new double[terms.size()][1];
		
		// Get the lemmas of the nouns in the query
		List<String> queryLemmas = KeywordsUtil.processLanguage(query);
		
		// Rejoin the lemmas into a single string
		String processedQuery = String.join(" ", queryLemmas);
		
		// For each term in the list of terms
		for(String term : terms)
		{
	
			// Count the number of occurrences of the term in the query
			int termOccurrences = count(term, processedQuery);

			// Set the value on the matrix
			queryMatrixData[rowControl][0] = termOccurrences;

			// Once the term occurrences have been calculated the query, go to the next row (next term)
			rowControl++;
		}
		
		// When all values have been calculated, then fill the matrix data
		queryMatrix = new SimpleMatrix(queryMatrixData);
	}

	/**
	 * Method that calculates the LSI similitude values between the documents and the query.
	 * @param documents List of String documents used in the problem, to be able to display their similitude values to the query
	 * @param dimension Dimension to which the matrices of the LSI must be reduced
	 */
	@SuppressWarnings("rawtypes")
	private static void calculateLSISimilitudeValues(List<String> documents, int dimension)
	{
		// Get the SVD decomposition of the Term-by-Document Co-Occurrence Matrix
		SimpleSVD matrixSVD = termByDocumentCoOccurrenceMatrix.svd(true);
				
		// Get the three matrices that compose the SVD
		SimpleMatrix u = matrixSVD.getU();
		SimpleMatrix w = matrixSVD.getW();
		SimpleMatrix v = matrixSVD.getV();
		
		/* Reduce the matrices to work with a certain number of dimensions */
		SimpleMatrix reducedU = new SimpleMatrix(u.numRows(),dimension);
		SimpleMatrix reducedW = new SimpleMatrix(dimension,dimension);
		SimpleMatrix reducedV = new SimpleMatrix(v.numRows(),dimension);
		
		/* Fill the reduced matrices */
		// Fill reducedU
		for(int row = 0; row < u.numRows(); row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedU.set(row, col, u.get(row,col));
		}
		// Fill reducedW
		for(int row = 0; row < dimension; row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedW.set(row, col, w.get(row,col));
		}
		// Fill reducedV
		for(int row = 0; row < v.numRows(); row++)
		{
			for(int col = 0; col < dimension; col++)
				reducedV.set(row, col, v.get(row,col));
		}
					
		// The vectors of the documents are stored in the V matrix rows
		// Create a list of vectors and add the V matrix rows recursively
		List<SimpleMatrix> documentVectors = new ArrayList<SimpleMatrix>();
		for(int i = 0; i < reducedV.numRows(); i++)
		{
			documentVectors.add(reducedV.extractVector(true, i));
		}
	
		// Obtain the vector associated to the query
		SimpleMatrix transposedQuery = queryMatrix.transpose();
		SimpleMatrix invertedW = reducedW.invert();
		SimpleMatrix queryVector = transposedQuery.mult(reducedU).mult(invertedW);
		
		// Calculate the cosine similitude for each of the documents
		int documentIndex = 0;
		for(SimpleMatrix vector : documentVectors)
		{
			// Calculate the dot product between the document vector and the query vector, and the norms of both vectors
			double dotProd = VectorVectorMult.innerProd(queryVector.getMatrix(), vector.getMatrix());
			double queryNorm = queryVector.normF();
			double vectorNorm = vector.normF();
			
			// Perform the similitude operation
			double similitude = dotProd / (queryNorm * vectorNorm);
			
			// Display the document similitude to the query
			System.out.println("Document: d" + (documentIndex+1) + ", Similitude to the query: " + similitude);
						
			documentIndex++;
		}		
	}
	
	/**
	 * Static method that can be called to retrieve the similitude values of LSI for a collection of documents + query with a certain dimension reduction.
	 * @param documents The documents of the problem
	 * @param query The query of the problem
	 * @param dimension The dimension to which the matrices of the LSI must be reduced
	 */
	public static void PerformLSI(List<String> documents, String query, int dimension)
	{
		// Generate the example collection (documents + query)
		List<String> collection = new ArrayList<String>();
		collection.addAll(documents);
		collection.add(query);
		
		// Extract the keywords from the collection, removing duplicates
		List<String> keywords = KeywordsUtil.extractKeywords(collection, true);
		
		// Build the Term-by-Document Co-Ocurrence Matrix
		buildTermByDocumentCoOccurrenceMatrix(keywords, documents);
		
		// Build the Query Matrix
		buildQueryMatrix(keywords, query);
		
		// Calculate the similitude values between documents and query
		calculateLSISimilitudeValues(documents, dimension);
	}
	
	
	/* Auxiliary methods */
	
	/**
	 * Method that counts the occurrences of a term in a particular String
	 * @param term The term
	 * @param s The string from which terms must be counted
	 * @return The number of occurrences of the term in the string
	 */
	private static int count(String term, String s)
	{
		// Set occurrences to 0
		int occurrences = 0;
		
		// Tokenize the incoming string (splitting the words through spaces)
		String[] tokens = s.split(" ");
		
		// Check if each token is the term we are checking
		// In case it is, increase the occurrences by 1
		for(String token : tokens) 
			if (term.equals(token)) occurrences++;
				
		// Return occurrences
		return occurrences;
	}
}
